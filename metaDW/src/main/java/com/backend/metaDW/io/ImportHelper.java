package com.backend.metaDW.io;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.backend.metaDW.model.dao.ICategoryDAO;
import com.backend.metaDW.model.dao.IScopeDAO;
import com.backend.metaDW.model.dao.ISearchDAO;
import com.backend.metaDW.model.dao.ITermDAO;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.CreationTerm;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.TermType;
import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;
import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;
import com.backend.metaDW.model.vo.term.hierarchy.SimpleObjectivesDimension;
import com.opencsv.CSVReader;

@Service
public class ImportHelper {

	@Autowired
	private ITermDAO termDAO;
	@Autowired
	private ICategoryDAO categoryDAO;
	@Autowired
	private IScopeDAO scopeDAO;
	@Autowired
	private ISearchDAO searchDAO;

	private Integer idUser;
	private TermType termType;

	private static final int CONCEPT = 1;
	private static final int DIMENSION = 2;
	private static final int INDICATOR = 3;
	private static final int OBJECTIVE = 4;
	private static final int INFORM = 5;
	private static final int DATA_SOURCE = 6;
	private static final int SCOPE = 7;
	private static final int CATEGORY = 8;

	private static final String INIT_VERSION = "1.0";
	private static final String SEPARATOR = ",";

	private File file;

	@Transactional
	public boolean importDataFromCSV(MultipartFile multipart, Integer type, String idUser, String path)
			throws IllegalStateException, IOException {

		CSVReader reader = null;
		try {
			reader = new CSVReader(getFileReader(multipart, path));
			List<String[]> fileAsList = reader.readAll();
			fileAsList.remove(0); // se elimina la cabecera
			this.idUser = Integer.parseInt(idUser);
			boolean result = false;

			if (!type.equals(SCOPE) && !type.equals(CATEGORY))
				this.setTermType(type);

			switch (type) {
			case CONCEPT:
			case DATA_SOURCE:
				result = this.extractTerms(fileAsList);
				break;
			case DIMENSION:
				result = this.extractDimensions(fileAsList);
				break;
			case INDICATOR:
				result = this.extractIndicators(fileAsList);
				break;
			case OBJECTIVE:
				result = this.extractObjectives(fileAsList);
				break;
			case INFORM:
				result = this.extractInforms(fileAsList);
				break;
			case SCOPE:
				result = this.extractScopes(fileAsList);
				break;
			case CATEGORY:
				result = this.extractCategories(fileAsList);
				break;
			default:
				reader.close();
				throw new IllegalStateException();
			}

			reader.close();
			file.delete();
			return result;

		} catch (Exception e) {

			if (reader != null)
				reader.close();
			if (file != null)
				file.delete();
			throw e;
		}

	}

	private FileReader getFileReader(MultipartFile multipart, String path) throws IllegalStateException, IOException {

		file = new File(path + "/" + multipart.getOriginalFilename());
		multipart.transferTo(file);
		return new FileReader(file);
	}

	/**
	 * Cargar térmitos tipo concepto y fuente de datos. Cabeceras:
	 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories
	 * 
	 * @param list
	 * @return
	 */
	private boolean extractTerms(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();
			CreationTerm creationTerm = this.extractTerm(item);
			creationTerm.setIdScope(this.getScopeFromTerm(item[7]));
			creationTerm.setCategories(this.getCategoriesFromTerm(item[8]));
			System.out.println(creationTerm.toString());
			this.termDAO.createTermTransaction(creationTerm);
		}
		return true;
	}

	/**
	 * 
	 * Cargar térmitos tipo dimensión. Cabeceras:
	 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,originName,originCreation,originUpdate,period,applicativeName,applicativeDenomination,hierarchy
	 * 
	 * @param list
	 * @return
	 */
	private boolean extractDimensions(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();
			CreationTerm creationTerm = this.extractTerm(item);
			creationTerm.setIdScope(this.getScopeFromTerm(item[7]));
			creationTerm.setCategories(this.getCategoriesFromTerm(item[8]));
			creationTerm = this.extractCommonDimensionIndicatorObjective(creationTerm, item);
			creationTerm.setHierarchy(item[15]);
			System.out.println(creationTerm.toString());
			this.termDAO.createTermTransaction(creationTerm);
		}
		return true;
	}

	/**
	 * Cargar términos de tipo indicador. Cabeceras:
	 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,originName,originCreation,originUpdate,period,applicativeName,applicativeDenomination,functionalInterpretation,formula,granularity,dimensions
	 * 
	 * @param list
	 * @return
	 */
	private boolean extractIndicators(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();
			CreationTerm creationTerm = this.extractTerm(item);
			creationTerm.setIdScope(this.getScopeFromTerm(item[7]));
			creationTerm.setCategories(this.getCategoriesFromTerm(item[8]));
			creationTerm = this.extractCommonDimensionIndicatorObjective(creationTerm, item);
			creationTerm.setFunctionalInterpretation(item[15]);
			creationTerm.setFormula(item[16]);
			creationTerm.setGranularity(item[17]);
			creationTerm.setIndicatorsDimensions(extractIndicatorsDimensions(item[18]));
			System.out.println(creationTerm.toString());
			this.termDAO.createTermTransaction(creationTerm);
		}
		return true;
	}

	/**
	 * Cargar términos de tipo objetivos. Cabeceras:
	 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,originName,originCreation,originUpdate,period,applicativeName,applicativeDenomination,functionalInterpretation,formula,granularity,dimensions,indicators
	 * 
	 * @param list
	 * @return
	 */
	private boolean extractObjectives(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();
			CreationTerm creationTerm = this.extractTerm(item);
			creationTerm.setIdScope(this.getScopeFromTerm(item[7]));
			creationTerm.setCategories(this.getCategoriesFromTerm(item[8]));
			creationTerm = this.extractCommonDimensionIndicatorObjective(creationTerm, item);
			creationTerm.setFunctionalInterpretation(item[15]);
			creationTerm.setFormula(item[16]);
			creationTerm.setGranularity(item[17]);
			creationTerm.setObjectivesDimensions(extractObjectivesDimensions(item[18]));
			creationTerm.setAsociatedIndicators(extractAsociatedIndicators(item[19]));
			System.out.println(creationTerm.toString());
			this.termDAO.createTermTransaction(creationTerm);
		}
		return true;
	}

	/**
	 * Cargar términos de tipo informe. Cabeceras:
	 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,link,indicators
	 * 
	 * @param list
	 * @return
	 */
	private boolean extractInforms(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();
			CreationTerm creationTerm = this.extractTerm(item);
			creationTerm.setIdScope(this.getScopeFromTerm(item[7]));
			creationTerm.setCategories(this.getCategoriesFromTerm(item[8]));
			creationTerm.setLink(item[9]);
			creationTerm.setInformIndicators(extractInformIndicators(item[10]));
			System.out.println(creationTerm.toString());
			this.termDAO.createTermTransaction(creationTerm);
		}
		return true;
	}

	private boolean extractScopes(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();

			Scope scope = new Scope();
			scope.setName(item[0]);
			scope.setDefinition(item[1]);
			scope.setVersion(INIT_VERSION);
			scope.setOwner(this.idUser);
			scope.setState(1);

			this.scopeDAO.saveScope(scope);
		}
		return true;
	}

	@Transactional
	private boolean extractCategories(List<String[]> list) {

		Iterator<String[]> iterator = list.iterator();
		String[] item;

		while (iterator.hasNext()) {
			item = iterator.next();

			CreationCategory category = new CreationCategory();
			category.setName(item[0]);
			category.setDefinition(item[1]);
			category.setVersion(INIT_VERSION);
			category.setOwner(this.idUser);
			category.setState(1);
			category.setType(-1);

			if (item[2] != null && !item[2].isEmpty()) {
				Integer subcategory = findSubcategory(item[2]);
				if (subcategory != null)
					category.setType(0);
				category.setIdParent(subcategory);
			}

			this.categoryDAO.createCategoryTransaction(category);
		}
		return true;
	}

	@Transactional(readOnly = true)
	private Integer findSubcategory(String input) {
		Page<Category> foundCategories = this.searchDAO.findCategoriesByName(input, new PageRequest(0, 10));
		if (foundCategories != null && foundCategories.getNumberOfElements() == 1) {
			return foundCategories.getContent().get(0).getIdCategory();

		}
		return null;
	}

	private Term getTerm(String[] list) {
		Term term = new Term();
		term.setName(list[0]);
		term.setShortName(list[1]);
		term.setDefinition(list[2]);
		term.setExtendedDefinition(list[3]);
		term.setState(1);
		term.setVersion(INIT_VERSION);
		term.setOwner(this.idUser);
		term.setType(this.termType);
		return term;
	}

	@Transactional(readOnly = true)
	private Integer getScopeFromTerm(String input) {
		if (!input.isEmpty()) {
			Page<Scope> foundScopes = this.searchDAO.findScopesByName(input, new PageRequest(0, 10));
			if (foundScopes.getNumberOfElements() == 1)
				return foundScopes.getContent().get(0).getIdScope();
		}
		return null;
	}

	@Transactional(readOnly = true)
	private List<Integer> getCategoriesFromTerm(String input) {

		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);
		List<Integer> categories = new ArrayList<>();
		if (!input.isEmpty()) {
			while (tokenizer.hasMoreTokens()) {
				Page<Category> foundCategories = this.searchDAO.findCategoriesByName(tokenizer.nextToken(),
						new PageRequest(0, 10));
				if (foundCategories.getNumberOfElements() == 1) {
					categories.add(foundCategories.getContent().get(0).getIdCategory());
				}
			}
		}
		return categories;
	}

	private void setTermType(Integer type) {

		Iterable<TermType> termTypes = this.termDAO.findAllTermTypes();

		for (TermType termType : termTypes) {
			if (termType.getType().equals(type)) {
				this.termType = termType;
			}
		}
	}

	private List<Denomination> loadDenominations(String input) {
		List<Denomination> denominations = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);
		while (tokenizer.hasMoreTokens()) {
			Denomination denomination = new Denomination();
			denomination.setDenomination(tokenizer.nextToken());
			denominations.add(denomination);
		}
		return denominations;
	}

	private List<String> loadStringElements(String input) {
		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);
		List<String> list = new ArrayList<>();
		while (tokenizer.hasMoreTokens()) {
			list.add(tokenizer.nextToken());
		}
		return list;
	}

	private CreationTerm extractTerm(String[] item) {
		CreationTerm creationTerm = new CreationTerm();
		creationTerm.setMainTerm(this.getTerm(item));
		creationTerm.setDenominations(this.loadDenominations(item[4]));
		creationTerm.setExamples(loadStringElements(item[5]));
		creationTerm.setExclusions(loadStringElements(item[6]));
		return creationTerm;
	}

	private CreationTerm extractCommonDimensionIndicatorObjective(CreationTerm creationTerm, String[] item) {

		String originName = item[9];
		if (isNullOrEmpty(originName)) { // información del origen

			OperationalOrigin operationalOrigin = findOperationalOriginByName(originName);
			if (operationalOrigin != null) {
				creationTerm.setOrigin(operationalOrigin.getIdOrigin());
				creationTerm.setOriginCreation(Timestamp.valueOf(item[10]));
				creationTerm.setOriginUpdate(Timestamp.valueOf(item[11]));
				creationTerm.setPeriod(Integer.parseInt(item[12]));
			}
		}

		String applicativeName = item[13];
		if (isNullOrEmpty(applicativeName)) { // información del aplicativo

			AnalyticalApplicative analyticalApplicative = findAnalyticalApplicativeByName(applicativeName);
			if (analyticalApplicative != null) {
				creationTerm.setApplicative(analyticalApplicative.getIdApplicative());
				creationTerm.setApplicativeDenomination(item[14]);
			}
		}

		return creationTerm;
	}

	private OperationalOrigin findOperationalOriginByName(String originName) {

		Page<OperationalOrigin> foundOrigins = this.searchDAO.findOperationalOriginsByName(originName,
				new PageRequest(0, 10));

		if (foundOrigins.getNumberOfElements() == 1) {
			return (OperationalOrigin) foundOrigins.getContent().get(0);
		}
		return null;
	}

	private AnalyticalApplicative findAnalyticalApplicativeByName(String applicativeName) {
		Page<AnalyticalApplicative> foundApplicatives = this.searchDAO.findAnalyticalApplicativeByName(applicativeName,
				new PageRequest(0, 10));

		if (foundApplicatives.getNumberOfElements() == 1) {
			return (AnalyticalApplicative) foundApplicatives.getContent().get(0);
		}
		return null;
	}

	private List<Integer> extractIndicatorsDimensions(String input) {
		List<Integer> indicatorsDimensions = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);
		while (tokenizer.hasMoreTokens()) {
			Page<Term> foundTerms = this.searchDAO.findTermDimensionsByNameAndType(tokenizer.nextToken(),
					new PageRequest(0, 10));
			if (foundTerms.getNumberOfElements() == 1)
				indicatorsDimensions.add(foundTerms.getContent().get(0).getIdTerm());
		}
		return indicatorsDimensions;
	}

	private List<SimpleObjectivesDimension> extractObjectivesDimensions(String input) {
		List<SimpleObjectivesDimension> objectivesDimensions = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);

		while (tokenizer.hasMoreTokens()) {
			Page<Term> foundTerms = this.searchDAO.findTermDimensionsByNameAndType(tokenizer.nextToken(),
					new PageRequest(0, 10));
			if (foundTerms.getNumberOfElements() == 1)
				objectivesDimensions
						.add(new SimpleObjectivesDimension(null, foundTerms.getContent().get(0).getIdTerm(), false));
		}

		return objectivesDimensions;
	}

	private List<Integer> extractAsociatedIndicators(String input) {
		List<Integer> asociatedIdicators = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);

		while (tokenizer.hasMoreTokens()) {
			Page<Term> foundTerms = this.searchDAO.findTermIndicatorsByName(tokenizer.nextToken(),
					new PageRequest(0, 10));
			if (foundTerms.getNumberOfElements() == 1)
				asociatedIdicators.add(foundTerms.getContent().get(0).getIdTerm());
		}

		return asociatedIdicators;
	}

	private List<Integer> extractInformIndicators(String input) {
		List<Integer> asociatedIdicators = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(input, SEPARATOR);

		while (tokenizer.hasMoreTokens()) {
			Page<Term> foundTerms = this.searchDAO.findTermIndicatorsByName(tokenizer.nextToken(),
					new PageRequest(0, 10));
			if (foundTerms.getNumberOfElements() == 1)
				asociatedIdicators.add(foundTerms.getContent().get(0).getIdTerm());
		}

		return asociatedIdicators;
	}

	private static boolean isNullOrEmpty(String item) {
		if (item != null && item.isEmpty())
			return false;
		return true;
	}
}
