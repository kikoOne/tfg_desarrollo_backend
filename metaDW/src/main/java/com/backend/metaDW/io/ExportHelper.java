package com.backend.metaDW.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.ICategoryDAO;
import com.backend.metaDW.model.dao.IScopeDAO;
import com.backend.metaDW.model.dao.ITermDAO;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.TermsByCategory;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.scope.TermsByScope;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.TermExample;
import com.backend.metaDW.model.vo.term.TermExclusion;
import com.backend.metaDW.model.vo.term.hierarchy.AsociatedIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.FullDimension;
import com.backend.metaDW.model.vo.term.hierarchy.FullIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.IndicatorsDimension;
import com.backend.metaDW.model.vo.term.hierarchy.Inform;
import com.backend.metaDW.model.vo.term.hierarchy.InformIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.ObjectivesDimension;
import com.opencsv.CSVWriter;

@Service
public class ExportHelper {

	@Autowired
	private ITermDAO termDAO;
	@Autowired
	private ICategoryDAO categoryDAO;
	@Autowired
	private IScopeDAO scopeDAO;

	private CSVWriter writer;

	private static final int CONCEPT = 1;
	private static final String CONCEPT_ = "CONCEPT";
	private static final int DIMENSION = 2;
	private static final String DIMENSION_ = "DIMENSION";
	private static final int INDICATOR = 3;
	private static final String INDICATOR_ = "INDICATOR";
	private static final int OBJECTIVE = 4;
	private static final String OBJECTIVE_ = "OBJECTIVE";
	private static final int INFORM = 5;
	private static final String INFORM_ = "INFORM";
	private static final int DATA_SOURCE = 6;
	private static final String DATA_SOURCE_ = "DATA_SOURCE";
	private static final int SCOPE = 7;
	private static final String SCOPE_ = "SCOPE";
	private static final int CATEGORY = 8;
	private static final String CATEGORY_ = "CATEGORY";

	private static final char SEPARATOR = ';';
	private static final String CSV_EXTENSION = ".csv";

	@Transactional
	public File exportDataFromCSV(Integer type) throws IOException {

		String outputFile;
		File file;

		switch (type) {
		case CONCEPT:
			outputFile = CONCEPT_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadConcepts();
			break;
		case DIMENSION:
			outputFile = DIMENSION_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadDimensions();
			break;
		case INDICATOR:
			outputFile = INDICATOR_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadIndicators();
			break;
		case OBJECTIVE:
			outputFile = OBJECTIVE_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadObjectives();
			break;
		case INFORM:
			outputFile = INFORM_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadInforms();
			break;
		case DATA_SOURCE:
			outputFile = DATA_SOURCE_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadDataSources();
			break;
		case SCOPE:
			outputFile = SCOPE_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadScopes();
			break;
		case CATEGORY:
			outputFile = CATEGORY_ + CSV_EXTENSION;
			file = new File(outputFile);
			writer = new CSVWriter(new FileWriter(file), SEPARATOR);
			this.loadCategories();
			break;
		default:
			throw new IllegalStateException();
		}
		this.writer.close();

		return file;
	}

	@Transactional(readOnly = true)
	private void loadScopes() {
		/**
		 * name description
		 */
		writer.writeNext(new String[2]);

		List<Scope> scopes = this.scopeDAO.findAllScopes();
		for (Scope scope : scopes) {
			List<String> scopeAsList = new ArrayList<>();
			scopeAsList.add(scope.getName());
			scopeAsList.add(scope.getDefinition());
			writer.writeNext(scopeAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private void loadCategories() {
		/**
		 * name description, parent
		 */
		writer.writeNext(new String[3]);

		List<Category> categories = this.categoryDAO.findAllCategories();
		for (Category category : categories) {
			List<String> scopeAsList = new ArrayList<>();
			scopeAsList.add(category.getName());
			scopeAsList.add(category.getDefinition());
			int type = category.getType();
			if (type != -1) {
				Category parent = this.categoryDAO.findParentCategory(category.getIdCategory());
				if (parent != null)
					scopeAsList.add(parent.getName());
			} else {
				scopeAsList.add("");
			}
			writer.writeNext(scopeAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private void loadConcepts() {

		/**
		 * Headers
		 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories
		 */
		writer.writeNext(new String[8]);

		List<Term> terms = this.termDAO.findAllTermsByType(CONCEPT);
		for (Term term : terms) {
			List<String> termAsList = new ArrayList<>();
			termAsList = loadTerm(termAsList, term);
			writer.writeNext(termAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private void loadDataSources() {

		/**
		 * Headers
		 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories
		 */
		writer.writeNext(new String[8]);

		List<Term> terms = this.termDAO.findAllTermsByType(DATA_SOURCE);
		for (Term term : terms) {
			List<String> termAsList = new ArrayList<>();
			termAsList = loadTerm(termAsList, term);
			writer.writeNext(termAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private void loadDimensions() {

		/**
		 * Headers
		 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,originName,originCreation,originUpdate,period,applicativeName,applicativeDenomination,hierarchy
		 */
		writer.writeNext(new String[16]);

		List<Term> terms = this.termDAO.findAllTermsByType(DIMENSION);
		for (Term term : terms) {
			List<String> termAsList = new ArrayList<>();
			termAsList = loadTerm(termAsList, term);
			termAsList = loadMainDimension(termAsList, term.getIdTerm());
			writer.writeNext(termAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private List<String> loadMainDimension(List<String> termAsList, Integer idTerm) {

		FullDimension dimension = this.termDAO.getDimensionFromTerm(idTerm);
		if (dimension.getOrigin() != null) {
			termAsList.add(dimension.getOrigin().getDescription());
			termAsList.add(new Date(dimension.getOriginCreation().getTime()).toString());
			termAsList.add(new Date(dimension.getOriginUpdate().getTime()).toString());
			termAsList.add(((Integer) dimension.getPeriod()).toString());
		}
		if (dimension.getApplicative() != null) {
			termAsList.add(dimension.getApplicative().getName());
			termAsList.add(dimension.getApplicative_denomination());
		}
		termAsList.add(dimension.getHierarchy());

		return termAsList;
	}

	@Transactional(readOnly = true)
	private void loadIndicators() {

		/**
		 * Headers
		 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,originName,originCreation,originUpdate,period,applicativeName,applicativeDenomination,functionalInterpretation,formula,granularity,dimensions
		 */
		writer.writeNext(new String[19]);

		List<Term> terms = this.termDAO.findAllTermsByType(INDICATOR);
		for (Term term : terms) {
			List<String> termAsList = new ArrayList<>();
			termAsList = loadTerm(termAsList, term);
			termAsList = loadMainIndicatorsObjectives(termAsList, term.getIdTerm());
			termAsList.add(this.loadIndicatorsDimensionsFromTerm(term.getIdTerm()));
			writer.writeNext(termAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private List<String> loadMainIndicatorsObjectives(List<String> termAsList, Integer idTerm) {

		FullIndicator indicator = this.termDAO.getIndicatorFromTerm(idTerm);
		if (indicator != null) {
			if (indicator.getOrigin() != null) {
				termAsList.add(indicator.getOrigin().getDescription());
				termAsList.add(new Date(indicator.getOriginCreation().getTime()).toString());
				termAsList.add(new Date(indicator.getOriginUpdate().getTime()).toString());
				termAsList.add(((Integer) indicator.getPeriod()).toString());
			}
			if (indicator.getApplicative() != null) {
				termAsList.add(indicator.getApplicative().getName());
				termAsList.add(indicator.getApplicative_denomination());
			}
			termAsList.add(indicator.getFunctionalInterpretation());
			termAsList.add(indicator.getFormula());
			termAsList.add(indicator.getGranularity());
		}
		return termAsList;
	}

	@Transactional(readOnly = true)
	private String loadIndicatorsDimensionsFromTerm(Integer idTerm) {

		List<IndicatorsDimension> dimensions = this.termDAO.getAllDimensionIndicators(idTerm);
		List<String> list = new ArrayList<>();
		for (IndicatorsDimension dimension : dimensions) {
			list.add(dimension.getDimension().getName());
		}

		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private void loadObjectives() {

		/**
		 * Headers
		 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,originName,originCreation,originUpdate,period,applicativeName,applicativeDenomination,functionalInterpretation,formula,granularity,dimensions,indicators
		 */
		writer.writeNext(new String[20]);

		List<Term> terms = this.termDAO.findAllTermsByType(OBJECTIVE);
		for (Term term : terms) {
			List<String> termAsList = new ArrayList<>();
			termAsList = loadTerm(termAsList, term);
			termAsList = loadMainIndicatorsObjectives(termAsList, term.getIdTerm());
			termAsList.add(this.loadObjectivesDimensionsFromTerm(term.getIdTerm()));
			termAsList.add(this.loadAsociatedIndicatorsFromTerm(term.getIdTerm()));
			writer.writeNext(termAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private String loadObjectivesDimensionsFromTerm(Integer idTerm) {

		List<ObjectivesDimension> dimensions = this.termDAO.getAllObjectiveDimensions(idTerm);
		List<String> list = new ArrayList<>();
		if (dimensions != null) {
			for (ObjectivesDimension dimension : dimensions) {
				list.add(dimension.getDimension().getName());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private String loadAsociatedIndicatorsFromTerm(Integer idTerm) {

		List<AsociatedIndicator> indicators = this.termDAO.getAllAsociatedIndicatorsFromTerm(idTerm);
		List<String> list = new ArrayList<>();
		if (indicators != null) {
			for (AsociatedIndicator indicator : indicators) {
				list.add(indicator.getIndicator().getName());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private void loadInforms() {

		/**
		 * Headers
		 * name,shortName,definition,extendeDefinition,denominations,examples,exclusions,scope,categories,link,indicators
		 */
		writer.writeNext(new String[11]);

		List<Term> terms = this.termDAO.findAllTermsByType(INFORM);
		for (Term term : terms) {
			List<String> termAsList = new ArrayList<>();
			termAsList = loadTerm(termAsList, term);
			termAsList = loadMainInform(termAsList, term.getIdTerm());
			termAsList.add(this.loadInformIndicators(term.getIdTerm()));
			writer.writeNext(termAsList.toArray(new String[0]));
		}
	}

	@Transactional(readOnly = true)
	private List<String> loadMainInform(List<String> termAsList, Integer idTerm) {

		Inform inform = this.termDAO.getInformFromTerm(idTerm);
		termAsList.add(inform.getLink());

		return termAsList;
	}

	@Transactional(readOnly = true)
	private String loadInformIndicators(Integer idTerm) {

		List<InformIndicator> indicators = this.termDAO.getAllInformIndicators(idTerm);
		List<String> list = new ArrayList<>();
		if (indicators != null) {
			for (InformIndicator indicator : indicators) {
				list.add(indicator.getIndicator().getName());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private List<String> loadTerm(List<String> termAsList, Term term) {
		int idTerm = term.getIdTerm();
		termAsList.add(term.getName());
		termAsList.add(term.getShortName());
		termAsList.add(term.getDefinition());
		termAsList.add(term.getExtendedDefinition());
		termAsList.add(this.loadDenominations(idTerm));
		termAsList.add(this.loadExamples(idTerm));
		termAsList.add(this.loadExclusions(idTerm));
		termAsList.add(this.loadScope(idTerm));
		termAsList.add(this.loadCategories(idTerm));
		return termAsList;
	}

	@Transactional(readOnly = true)
	private String loadCategories(Integer idTerm) {
		List<TermsByCategory> categories = this.termDAO.getCategoriesFromTerm(idTerm);
		List<String> list = new ArrayList<>();
		if (categories != null) {
			for (TermsByCategory category : categories) {
				list.add(category.getCategory().getName());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private String loadScope(Integer idTerm) {
		TermsByScope scope = this.termDAO.getScopeFromTerm(idTerm);
		if (scope != null)
			return scope.getScope().getName();
		else
			return null;
	}

	@Transactional(readOnly = true)
	private String loadExclusions(Integer idTerm) {
		List<TermExclusion> exclusions = this.termDAO.getAllExclusionsFromTerm(idTerm);
		List<String> list = new ArrayList<>();
		if (exclusions != null) {
			for (TermExclusion exclusion : exclusions) {
				list.add(exclusion.getExclusion());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private String loadExamples(Integer idTerm) {
		List<TermExample> examples = this.termDAO.getAllExamplesFromTerm(idTerm);
		List<String> list = new ArrayList<>();
		if (examples != null) {
			for (TermExample example : examples) {
				list.add(example.getExample());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	@Transactional(readOnly = true)
	private String loadDenominations(Integer idTerm) {
		List<Denomination> denominations = this.termDAO.getAllDenominationsFromTerm(idTerm);
		List<String> list = new ArrayList<>();
		if (denominations != null) {
			for (Denomination denomination : denominations) {
				list.add(denomination.getDenomination());
			}
		}
		return this.arrayToCommaSeparated(list);
	}

	private String arrayToCommaSeparated(List<String> list) {
		String output = new String();
		if (list != null) {
			if (list.size() > 0) {
				output = output + list.get(0);
				list.remove(0);
			}
			for (String item : list) {
				output = output + SEPARATOR + item;
			}
		}
		return output;
	}
}
