package com.backend.metaDW.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.backend.metaDW.model.dao.ISearchDAO;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.FullTerm;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;
import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;

@RestController
@RequestMapping("/1.0/metadw/search")
public class SearchService {

	@Autowired
	private ISearchDAO searchDAO;

	/**
	 * Recuperar los ámbitos paginados por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/scopes", method = RequestMethod.GET)
	public ResponseEntity<?> findScopesByName(Pageable page, @RequestParam String query) {
		try {
			Page<Scope> result = this.searchDAO.findScopesByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las categorías paginadas por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<?> findCategoriesByName(Pageable page, @RequestParam String query) {
		try {
			Page<Category> result = this.searchDAO.findCategoriesByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los origenes operacionales paginados cond búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/operational_origins", method = RequestMethod.GET)
	public ResponseEntity<?> findOperationalOriginsByName(Pageable page, @RequestParam String query) {
		try {
			Page<OperationalOrigin> result = this.searchDAO.findOperationalOriginsByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los aplicativos analíticos paginados con búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/analytical_applicatives", method = RequestMethod.GET)
	public ResponseEntity<?> findAnalyticalApplicativesByName(Pageable page, @RequestParam String query) {
		try {
			Page<AnalyticalApplicative> result = this.searchDAO.findAnalyticalApplicativeByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las dimensiones paginadas con búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/dimensions", method = RequestMethod.GET)
	public ResponseEntity<?> findTermDimensionsByName(Pageable page, @RequestParam String query) {
		try {
			Page<Term> result = this.searchDAO.findTermDimensionsByNameAndType(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los objetivos paginados con búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/objectives", method = RequestMethod.GET)
	public ResponseEntity<?> findTermObjectivesByName(Pageable page, @RequestParam String query) {
		try {
			Page<Term> result = this.searchDAO.findTermObjectivesByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los indicadores asociados paginados con búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/indicators", method = RequestMethod.GET)
	public ResponseEntity<?> findTermIndicatorsByName(Pageable page, @RequestParam String query) {
		try {
			Page<Term> result = this.searchDAO.findTermIndicatorsByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar términos paginados con búsqueda por nombre y tipo
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ResponseEntity<?> findTermsByNameAndType(Pageable page, @RequestParam String query,
			@RequestParam List<Integer> types) {
		try {
			Page<FullTerm> result;
			if (query != null && types != null) {
				result = this.searchDAO.findTermsByNameAndType(query, types, page);
			} else if (query == null) {
				result = this.searchDAO.findTermsByType(types, page);
			} else if (types == null || types.size() == 0) {
				result = this.searchDAO.findTermsByName(query, page);
			} else {
				throw new Exception();
			}
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar términos paginados con búsqueda por nombre y tipo de una
	 * categoría
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories/{idCategory}/terms", method = RequestMethod.GET)
	public ResponseEntity<?> findTermsByNameAndTypeFromCategory(Pageable page, @PathVariable Integer idCategory,
			@RequestParam String query, @RequestParam List<Integer> types) {
		try {
			Page<?> result;
			if (query != null && types != null) {
				result = this.searchDAO.findTermsByNameAndTypeFromCategory(query, types, idCategory, page);
			} else if (query == null) {
				result = this.searchDAO.findTermsByTypeFromCategory(types, idCategory, page);
			} else if (types == null) {
				result = this.searchDAO.findTermsByNameFromCategory(query, idCategory, page);
			} else {
				throw new Exception();
			}
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar categorías paginadas con búsqueda por nombre y pertenecientes a
	 * una categoría
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories/{idCategory}/subcategories", method = RequestMethod.GET)
	public ResponseEntity<?> findCategoriesByNameFromCategory(Pageable page, @PathVariable Integer idCategory,
			@RequestParam String query) {
		try {
			Page<?> result = this.searchDAO.findCategoriesByNameFromCategory(query, idCategory, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar categorías paginadas con búsqueda por nombre y tipo
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories/type/{type}", method = RequestMethod.GET)
	public ResponseEntity<?> findCategoriesByNameAndType(Pageable page, @RequestParam String query, @PathVariable Integer type) {
		try {
			Page<?> result = this.searchDAO.findCategoriesByNameAndType(query, type, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar categorías paginadas con búsqueda por nombre y tipos
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories/types", method = RequestMethod.GET)
	public ResponseEntity<?> findCategoriesByNameAndTypes(Pageable page, @RequestParam String query,
			@RequestParam List<Integer> types) {
		try {
			Page<?> result = this.searchDAO.findCategoriesByNameAndTypes(query, types, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar usuarios paginados con búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/users/desactivated", method = RequestMethod.GET)
	public ResponseEntity<?> findDesactivatedUsersByName(Pageable page, @RequestParam String query) {
		try {
			Page<?> result = this.searchDAO.findDesactivatedUsersByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar usuarios paginados con búsqueda por nombre
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<?>  findUsersByName(Pageable page, @RequestParam String query) {
		try {
			Page<?> result = this.searchDAO.findActivatedUsersByName(query, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los términos paginados, por nombre y tipo, de un ámbito
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/scopes/{idScope}/terms", method = RequestMethod.GET)
	public ResponseEntity<?> findTermsByNameAndTypeFromScope(Pageable page, @RequestParam String query,
			@PathVariable Integer idScope, @RequestParam List<Integer> types) {
		try {
			Page<?> result = this.searchDAO.findTermsByNameAndTypeFromScope(query, types, idScope, page);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
