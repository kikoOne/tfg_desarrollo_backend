package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.model.dao.IHistoryDAO;
import com.backend.metaDW.model.vo.history.HistoryCategory;
import com.backend.metaDW.model.vo.history.HistoryDimension;
import com.backend.metaDW.model.vo.history.HistoryIndicator;
import com.backend.metaDW.model.vo.history.HistoryInform;
import com.backend.metaDW.model.vo.history.HistoryObjective;
import com.backend.metaDW.model.vo.history.HistoryScope;
import com.backend.metaDW.model.vo.history.HistoryTerm;

@RestController
@RequestMapping("/1.0/metadw/history")
public class HistoryService {

	@Autowired
	private IHistoryDAO historyDAO;

	/**
	 * Recuperar las versiones de un ámbito
	 * 
	 * @param idScope
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/scopes/{idScope}", method = RequestMethod.GET)
	public ResponseEntity<?> getScopeHistory(@PathVariable Integer idScope, Pageable page) {
		try {

			Page<?> result = this.historyDAO.getHistoryFromScope(idScope, page);
			if (result == null)
				throw new NotFoundException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar una versión de un ámbito
	 * 
	 * @param idScope
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/scopes/{idScope}/versions/{idVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> getScopeVersion(@PathVariable Integer idScope, @PathVariable Integer idVersion,
			Pageable page) {
		try {
			HistoryScope historyScope = this.historyDAO.getScopeHistoryVersion(idScope, idVersion);
			if (historyScope == null)
				throw new NotFoundException();

			return new ResponseEntity<>(historyScope, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las versiones de una categoría
	 * 
	 * @param idCategory
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories/{idCategory}", method = RequestMethod.GET)
	public ResponseEntity<?> getCategoryHistory(@PathVariable Integer idCategory, Pageable page) {
		try {
			Page<?> result = this.historyDAO.getHistoryFromCategory(idCategory, page);
			if (result == null)
				throw new NotFoundException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * Recuperar una versión de una categoría
	 * 
	 * @param idScope
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/categories/{idCategory}/versions/{idVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> getCategoryVersion(@PathVariable Integer idCategory, @PathVariable Integer idVersion,
			Pageable page) {
		try {
			HistoryCategory historyCategory = this.historyDAO.getCategoryHistoryVersion(idCategory, idVersion);
			if (historyCategory == null)
				throw new NotFoundException();

			return new ResponseEntity<>(historyCategory, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las versiones de un término
	 * 
	 * @param idTerm
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms/{idTerm}", method = RequestMethod.GET)
	public ResponseEntity<?> getTermHistory(@PathVariable Integer idTerm, Pageable page) {
		try {

			Page<HistoryTerm> versions = this.historyDAO.getHistoryFromTerm(idTerm, page);
			if (versions == null)
				throw new InternalServerException();

			return new ResponseEntity<>(versions, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar una versión de un término
	 * 
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms/{idTerm}/versions/{idVersion}", method = RequestMethod.GET)
	public ResponseEntity<?> getTermHistoryVersion(@PathVariable Integer idTerm, @PathVariable Integer idVersion,
			Pageable page) {
		try {

			HistoryTerm version = this.historyDAO.getTermHistoryVersion(idTerm, idVersion);
			if (version == null)
				throw new NotFoundException();

			return new ResponseEntity<>(version, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar una versión de una dimensión
	 * 
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms/{idVersion}/dimension", method = RequestMethod.GET)
	public ResponseEntity<?> getDimensionHistoryVersion(@PathVariable Integer idVersion, Pageable page) {
		try {

			HistoryDimension version = this.historyDAO.getDimensionHistoryVersion(idVersion);
			if (version == null)
				throw new NotFoundException();

			return new ResponseEntity<>(version, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar una versión de un indicador
	 * 
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms/{idVersion}/indicator", method = RequestMethod.GET)
	public ResponseEntity<?> getIndicatorHistoryVersion(@PathVariable Integer idVersion, Pageable page) {
		try {

			HistoryIndicator version = this.historyDAO.getIndicatorHistoryVersion(idVersion);
			if (version == null)
				throw new NotFoundException();

			return new ResponseEntity<>(version, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	

	/**
	 * Recuperar una versión de un objetivo
	 * 
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms/{idVersion}/objective", method = RequestMethod.GET)
	public ResponseEntity<?> getObjectiveHistoryVersion(@PathVariable Integer idVersion, Pageable page) {
		try {

			HistoryObjective version = this.historyDAO.getObjectiveHistoryVersion(idVersion);
			if (version == null)
				throw new NotFoundException();

			return new ResponseEntity<>(version, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Recuperar una versión de un informe
	 * 
	 * @param idVersion
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/terms/{idVersion}/inform", method = RequestMethod.GET)
	public ResponseEntity<?> getInformHistoryVersion(@PathVariable Integer idVersion, Pageable page) {
		try {

			HistoryInform version = this.historyDAO.getInformHistoryVersion(idVersion);
			if (version == null)
				throw new NotFoundException();

			return new ResponseEntity<>(version, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
