package com.backend.metaDW.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.IUserDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.user.FullExternalUser;
import com.backend.metaDW.model.vo.user.TermFollowed;
import com.backend.metaDW.model.vo.user.User;
import com.backend.metaDW.security.jwt.JwtTokenHelper;
import com.backend.metaDW.security.jwt.JWTToken;

@RestController
@RequestMapping("/1.0/metadw/users")
public class UserService {

	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String ADMIN_USERS = "ROLE_ADMIN_USERS";
	private static final String TERM = "ROLE_TERM";
	private static final String SCOPE = "ROLE_SCOPE";
	private static final String CATEGORY = "ROLE_CATEGORY";
	private static final String CREATION = "_CREATION";

	/**
	 * Crear un nuevo usuario
	 * 
	 * @param user
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createUser(@RequestBody User user) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}

			user.setPassword((new BCryptPasswordEncoder()).encode(user.getPassword()));
			userDAO.saveUser(user);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar usuarios
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getUsers(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}
			return new ResponseEntity<>(this.userDAO.findAllActivatedUsers(page), HttpStatus.OK);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/desactivated", method = RequestMethod.GET)
	public ResponseEntity<?> getDesactivatedUsers(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}
			return new ResponseEntity<>(this.userDAO.findAllDesactivatedUsers(page), HttpStatus.OK);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar un usuario
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(value = "/{idUser}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable Integer idUser) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				System.out.println("token null");

			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}
			return new ResponseEntity<>(this.userDAO.findUserById(idUser), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Modificar los datos de un usuario
	 * 
	 * @param idUser
	 * @param newUser
	 * @return
	 */
	@RequestMapping(value = "/{idUser}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateUser(@PathVariable Integer idUser, @RequestBody FullExternalUser newUser) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}

			if (!this.userDAO.updateUserData(newUser)) {
				throw new InternalServerException();
			}

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Cambiar contraseña
	 * 
	 * @param idUser
	 * @param newUser
	 * @return
	 */
	@RequestMapping(value = "/{idUser}/newPassword", method = RequestMethod.PUT)
	public ResponseEntity<?> changePassword(@PathVariable Integer idUser, @RequestParam String password,
			@RequestParam String newPassword, FullExternalUser newUser) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!token.getJwt().getTokenId().equals(idUser.toString())) {
				throw new UnauthorizedResponseException();
			}

			if (!this.userDAO.updatePassword(idUser, password, newPassword)) {
				throw new InternalServerException();
			}

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar un usuario
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(value = "/{idUser}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteUser(@PathVariable Integer idUser) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}
			if (!this.userDAO.desactivateUser(idUser)) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los términos creados por un usuario
	 * 
	 * @param page
	 * @param query
	 * @param types
	 * @return
	 */
	@RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ResponseEntity<?> findTermsFromOWner(Pageable page, @RequestParam String query,
			@RequestParam List<Integer> types) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + CREATION, token))
				throw new UnauthorizedResponseException();

			Integer idUser = Integer.parseInt(token.getJwt().getTokenId());
			return new ResponseEntity<>(this.userDAO.findTermsFromOwnerByNameAndType(idUser, query, types, page),
					HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los ámbitos creados por un usuario
	 *
	 * @param page
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/scopes", method = RequestMethod.GET)
	public ResponseEntity<?> findScopesFromOWner(Pageable page, @RequestParam String query) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(SCOPE + CREATION, token))
				throw new UnauthorizedResponseException();

			Integer idUser = Integer.parseInt(token.getJwt().getTokenId());
			return new ResponseEntity<>(this.userDAO.findScopesFromOwnerByName(idUser, query, page), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las categorías creadas por un usuario
	 * 
	 * @param page
	 * @param query
	 * @return
	 */
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<?> findCategoriesFromOWner(Pageable page, @RequestParam String query) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + CREATION, token))
				throw new UnauthorizedResponseException();

			Integer idUser = Integer.parseInt(token.getJwt().getTokenId());
			return new ResponseEntity<>(this.userDAO.findCategoriesFromOwnerByName(idUser, query, page), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Activar un usuario
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(value = "/{idUser}/activate", method = RequestMethod.PUT)
	public ResponseEntity<?> activateUser(@PathVariable Integer idUser) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_USERS, token)) {
				throw new UnauthorizedResponseException();
			}

			Integer result = this.userDAO.activateUser(idUser);
			if (result == null)
				throw new InternalServerException();

			return new ResponseEntity<>(new ResponseWrapper(result.toString()), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Añadir un término a los seguidos
	 * 
	 * @param idUser
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idUser}/bookmarks/terms/{idTerm}", method = RequestMethod.POST)
	public ResponseEntity<?> addTermBookmark(@PathVariable Integer idUser, @PathVariable Integer idTerm) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token != null && !idUser.equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			if (!this.userDAO.addTermBookmark(idUser, idTerm))
				throw new InternalServerException();

			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{idUser}/bookmarks/terms/{idTerm}", method = RequestMethod.GET)
	public ResponseEntity<?> getTermBookmark(@PathVariable Integer idUser, @PathVariable Integer idTerm) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token != null && !idUser.equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			TermFollowed termFollowed = this.userDAO.getFollowedTerm(idUser, idTerm);
			if (termFollowed == null)
				throw new NotFoundException();

			return new ResponseEntity<>(termFollowed, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los términos marcados por un usuario y paginados
	 * 
	 * @param idUser
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idUser}/bookmarks/terms", method = RequestMethod.GET)
	public ResponseEntity<?> getTermBookmarks(@PathVariable Integer idUser, Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token != null && !idUser.equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			Page<TermFollowed> result = this.userDAO.getFollowedTerms(idUser, page);
			if (result == null)
				throw new InternalServerException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar todos los términos marcados por un usuario
	 * 
	 * @param idUser
	 * @return
	 */
	@RequestMapping(value = "/{idUser}/bookmarks/terms/all", method = RequestMethod.GET)
	public ResponseEntity<?> getAllTermBookmarks(@PathVariable Integer idUser) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token != null && !idUser.equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();
			List<TermFollowed> result = this.userDAO.getAllFollowedTerms(idUser);
			if (result == null)
				throw new InternalServerException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar el marcador de un usuario
	 * 
	 * @param idUser
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idUser}/bookmarks/terms/{idTerm}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeTermBookmark(@PathVariable Integer idUser, @PathVariable Integer idTerm) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token != null && !idUser.equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			this.userDAO.removeFollowedTerm(idUser, idTerm);

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
