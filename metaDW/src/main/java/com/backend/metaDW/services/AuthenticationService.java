package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.ISecurityDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.user.security.FullInternalUser;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtAuthenticationRequest;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw")
public class AuthenticationService {

	@Autowired
	private ISecurityDAO securityDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	/**
	 * Crea un token R a partir de los parámetros de autenticación
	 * 
	 * @param jwtAuthenticationRequest
	 * @return
	 */
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest jwtAuthenticationRequest) {
		try {
			// Recuperar el usuario con sus permisos
			FullInternalUser retrievedUser = this.securityDAO.authenticateTransaction(
					jwtAuthenticationRequest.getUsername(), jwtAuthenticationRequest.getPassword());
			if (retrievedUser == null || retrievedUser.getState() == 0) {
				throw new UnauthorizedResponseException();
			}

			// Crear el token
			String tokenR = this.jwtTokenHelper.generateTokenR(retrievedUser.getIdUser(),
					this.jwtTokenHelper.parseRolesToString(retrievedUser.getRoles()),
					jwtAuthenticationRequest.getDevice());
			if (tokenR == null) {
				throw new InternalServerException();
			}

			return new ResponseEntity<>(new ResponseWrapper(tokenR), HttpStatus.CREATED);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		} catch (InternalServerException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Crea un token A a partir de un token R
	 * 
	 * @param tokenR
	 * @param auth
	 * @return
	 */
	@RequestMapping(value = "/refresh", method = RequestMethod.POST)
	public ResponseEntity<?> refreshAuthenticationToken(@RequestBody String tokenR) {
		try {

			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();

			String tokenA = this.jwtTokenHelper.generateTokenA(token.getJwt());
			if (tokenA == null)
				throw new InternalServerException();

			return new ResponseEntity<>(new ResponseWrapper(tokenA), HttpStatus.OK);
		} catch (SecurityException e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		} catch (InternalServerException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/auth", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteAuthToken(@RequestBody String tokenR) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();

			if(!this.jwtTokenHelper.removeTokenR(tokenR))
				throw new UnauthorizedResponseException();
			
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		} catch (InternalServerException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/roles", method = RequestMethod.GET)
	public ResponseEntity<?> getRoles() {
		try {
			return new ResponseEntity<>(this.securityDAO.getAllRoles(), HttpStatus.OK);
		} catch (InternalServerException e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
