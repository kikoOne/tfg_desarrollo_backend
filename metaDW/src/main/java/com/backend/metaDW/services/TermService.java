package com.backend.metaDW.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.ITermDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.category.TermsByCategory;
import com.backend.metaDW.model.vo.scope.TermsByScope;
import com.backend.metaDW.model.vo.term.CreationTerm;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.FullTerm;
import com.backend.metaDW.model.vo.term.Incidence;
import com.backend.metaDW.model.vo.term.TermExclusion;
import com.backend.metaDW.model.vo.term.TermType;
import com.backend.metaDW.model.vo.term.hierarchy.AsociatedIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.FullDimension;
import com.backend.metaDW.model.vo.term.hierarchy.FullIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.FullObjective;
import com.backend.metaDW.model.vo.term.hierarchy.IndicatorsDimension;
import com.backend.metaDW.model.vo.term.hierarchy.Inform;
import com.backend.metaDW.model.vo.term.hierarchy.InformIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.ObjectivesDimension;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/terms")
public class TermService {

	@Autowired
	private ITermDAO termDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String TERM = "ROLE_TERM";
	private static final String CREATION = "_CREATION";
	private static final String UPDATE = "_UPDATE";
	private static final String DELETE = "_DELETE";

	/**
	 * Crear nuevo término
	 * 
	 * @param term
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createTerm(@RequestBody CreationTerm term) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + CREATION, token)) {
				throw new UnauthorizedResponseException();
			}
			Integer idTerm = this.termDAO.createTermTransaction(term);
			if (idTerm == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(new ResponseWrapper(idTerm.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar un término
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}", method = RequestMethod.GET)
	public ResponseEntity<?> getTermById(@PathVariable Integer idTerm) {
		try {

			FullTerm fullTerm = this.termDAO.findTermById(idTerm);
			if (fullTerm == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<>(fullTerm, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getTerms(Pageable page) {
		try {

			Page<FullTerm> terms = this.termDAO.getTerms(page);
			if (terms == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(terms, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar ejemplos de un término
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/examples", method = RequestMethod.GET)
	public ResponseEntity<?> getExamplesFromTerm(@PathVariable Integer idTerm, Pageable page) {
		try {

			Page<?> termExamples = this.termDAO.getExamplesFromTerm(idTerm, page);
			if (termExamples == null)
				throw new InternalServerException();
			return new ResponseEntity<>(termExamples, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar exclusiones de un término
	 * 
	 * @param idTerm
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/exclusions", method = RequestMethod.GET)
	public ResponseEntity<?> getExclusionsFromTerm(@PathVariable Integer idTerm, Pageable page) {
		try {

			Page<TermExclusion> termExclusions = this.termDAO.getExclusionsFromTerm(idTerm, page);
			if (termExclusions == null)
				throw new InternalServerException();
			return new ResponseEntity<>(termExclusions, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar denominaciones de un término
	 * 
	 * @param idTerm
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/denominations", method = RequestMethod.GET)
	public ResponseEntity<?> getDenominationsFromTerm(@PathVariable Integer idTerm, Pageable page) {
		try {

			Page<Denomination> denominations = this.termDAO.getDenominationsFromTerm(idTerm, page);
			if (denominations == null)
				throw new InternalServerException();
			return new ResponseEntity<>(denominations, HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Modificar un término
	 * 
	 * @param idTerm
	 * @param term
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateTerm(@PathVariable Integer idTerm, @RequestBody CreationTerm term) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + UPDATE, token)) {
				throw new UnauthorizedResponseException();
			}
			this.termDAO.updateTerm(term);
			return new ResponseEntity<>(new ResponseWrapper(term.getMainTerm().getIdTerm().toString()), HttpStatus.OK);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause().getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar un término
	 * 
	 * @param idTerm
	 * @param term
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteTerm(@PathVariable Integer idTerm) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + DELETE, token)) {
				throw new UnauthorizedResponseException();
			}
			this.termDAO.removeTerm(idTerm);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los tipos de términos
	 * 
	 * @return
	 */
	@RequestMapping(value = "/types", method = RequestMethod.GET)
	public ResponseEntity<?> getTermTypes() {
		try {

			List<TermType> termTypes = (List<TermType>) this.termDAO.findAllTermTypes();
			if (termTypes == null)
				throw new NotFoundException();
			return new ResponseEntity<>(termTypes, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar el ámbito de un término
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/scope", method = RequestMethod.GET)
	public ResponseEntity<?> getTermScope(@PathVariable Integer idTerm) {
		try {
			TermsByScope tbs = this.termDAO.getScopeFromTerm(idTerm);
			if (tbs == null)
				throw new NotFoundException();
			return new ResponseEntity<>(tbs, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las categorías a las que pertenece un término
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/categories", method = RequestMethod.GET)
	public ResponseEntity<?> getTermCategories(@PathVariable Integer idTerm) {
		try {

			List<TermsByCategory> tbc = this.termDAO.getCategoriesFromTerm(idTerm);
			if (tbc == null)
				throw new NotFoundException();
			return new ResponseEntity<>(tbc, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las características de término de tipo dimensión
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/dimension", method = RequestMethod.GET)
	public ResponseEntity<?> getTermDimension(@PathVariable Integer idTerm) {
		try {

			FullDimension fullDimension = this.termDAO.getDimensionFromTerm(idTerm);
			if (fullDimension == null)
				throw new NotFoundException();
			return new ResponseEntity<>(fullDimension, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las características de término de tipo indicador
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/indicator", method = RequestMethod.GET)
	public ResponseEntity<?> getTermIndicator(@PathVariable Integer idTerm) {
		try {

			FullIndicator fullIndicator = this.termDAO.getIndicatorFromTerm(idTerm);
			if (fullIndicator == null)
				throw new NotFoundException();
			return new ResponseEntity<>(fullIndicator, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las características de término de tipo objetivo
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/objective", method = RequestMethod.GET)
	public ResponseEntity<?> getTermObjective(@PathVariable Integer idTerm) {
		try {

			FullObjective fullObjective = this.termDAO.getObjectiveFromTerm(idTerm);
			if (fullObjective == null)
				throw new NotFoundException();
			return new ResponseEntity<>(fullObjective, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las características de término de tipo informe
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/inform", method = RequestMethod.GET)
	public ResponseEntity<?> getTermInform(@PathVariable Integer idTerm) {
		try {

			Inform inform = this.termDAO.getInformFromTerm(idTerm);
			if (inform == null)
				throw new NotFoundException();
			return new ResponseEntity<>(inform, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las incidencias asociadas a un término
	 * 
	 * @param idTerm
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idTerm}/incidences", method = RequestMethod.GET)
	public ResponseEntity<?> getTermIncidences(@PathVariable Integer idTerm, Pageable page) {
		try {

			Page<Incidence> termIncidences = this.termDAO.getIncidencesFromTerm(idTerm, page);
			if (termIncidences == null)
				throw new NotFoundException();
			return new ResponseEntity<>(termIncidences, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los indicadores asociados a un término
	 * 
	 * @param idObjective
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idObjective}/objectives/asociated", method = RequestMethod.GET)
	public ResponseEntity<?> getAsociatedIndicators(@PathVariable Integer idObjective, Pageable page) {
		try {

			Page<AsociatedIndicator> ai = this.termDAO.getAsociatedIndicatorsFromTerm(idObjective, page);
			if (ai == null)
				throw new NotFoundException();
			return new ResponseEntity<>(ai, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los indicadores asociados a un informe
	 * 
	 * @param idInform
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idInform}/informs/indicators", method = RequestMethod.GET)
	public ResponseEntity<?> getInformIndicators(@PathVariable Integer idInform, Pageable page) {
		try {

			Page<InformIndicator> ii = this.termDAO.getInformIndicators(idInform, page);
			if (ii == null)
				throw new NotFoundException();
			return new ResponseEntity<>(ii, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los indicadores asociados a una dimensión
	 * 
	 * @param idIndicator
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idIndicator}/dimensions/indicators", method = RequestMethod.GET)
	public ResponseEntity<?> getDimensionIndicators(@PathVariable Integer idIndicator, Pageable page) {
		try {

			Page<IndicatorsDimension> id = this.termDAO.getDimensionIndicators(idIndicator, page);
			if (id == null)
				throw new NotFoundException();
			return new ResponseEntity<>(id, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los indicadores asociados a un objetivo
	 * 
	 * @param idObjective
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idObjective}/objectives/dimensions", method = RequestMethod.GET)
	public ResponseEntity<?> getObjectiveDimensions(@PathVariable Integer idObjective, Pageable page) {
		try {

			Page<ObjectivesDimension> od = this.termDAO.getObjectiveDimensions(idObjective, page);
			if (od == null)
				throw new NotFoundException();
			return new ResponseEntity<>(od, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
