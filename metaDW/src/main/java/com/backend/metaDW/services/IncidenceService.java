package com.backend.metaDW.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.IIncidenceDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.term.FullIncidence;
import com.backend.metaDW.model.vo.term.Incidence;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/incidences")
public class IncidenceService {

	@Autowired
	private IIncidenceDAO incidenceDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String ADMIN_INCIDENCE = "ROLE_ADMIN_INCIDENCE";

	/**
	 * Añadir una nueva indicencia
	 * 
	 * @param incidence
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createIncidence(@RequestBody Incidence incidence) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_INCIDENCE, token))
				throw new UnauthorizedResponseException();

			Integer id = this.incidenceDAO.saveIncidence(incidence);
			if (id == null)
				throw new InternalServerException();
			return new ResponseEntity<>(new ResponseWrapper(id.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recupearar todas las incidencias paginadas
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getIncidences(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_INCIDENCE, token))
				throw new UnauthorizedResponseException();

			Page<Incidence> incidences = this.incidenceDAO.getIncidences(page);
			if (incidences == null)
				throw new NotFoundException();

			return new ResponseEntity<>(incidences, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las últimas incidencias recogidas en el sistema
	 * @return
	 */
	@RequestMapping(value = "/last", method = RequestMethod.GET)
	public ResponseEntity<?> getLastIncidences() {
		try {
			Pageable page = new PageRequest(0, 100, new Sort(new Order(Direction.DESC, "creation")));
			Page<FullIncidence> incidences = this.incidenceDAO.getFullIncidences(page);
			if (incidences == null)
				throw new NotFoundException();

			return new ResponseEntity<>(incidences, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recupearar todos las incidencias completas y paginadas
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/full", method = RequestMethod.GET)
	public ResponseEntity<?> getFullIncidences(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_INCIDENCE, token))
				throw new UnauthorizedResponseException();

			Page<FullIncidence> incidences = this.incidenceDAO.getFullIncidences(page);
			if (incidences == null)
				throw new NotFoundException();

			return new ResponseEntity<>(incidences, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las incidencias asociadas a un término
	 * 
	 * @param idTerm
	 * @return
	 */
	@RequestMapping(value = "/terms/{idTerm}", method = RequestMethod.GET)
	public ResponseEntity<?> getIncidencesFromTerm(@PathVariable Integer idTerm) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_INCIDENCE, token))
				throw new UnauthorizedResponseException();

			List<Incidence> incidences = this.incidenceDAO.getIncidencesFromTerm(idTerm);
			if (incidences == null)
				throw new NotFoundException();

			return new ResponseEntity<>(incidences, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Actualizar inicidencia
	 * 
	 * @param idIncidence
	 * @param newIncidence
	 * @return
	 */
	@RequestMapping(value = "/{idIncidence}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateIncidence(@PathVariable Integer idIncidence, @RequestBody Incidence newIncidence) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_INCIDENCE, token))
				throw new UnauthorizedResponseException();

			Integer id = this.incidenceDAO.updateIncidence(newIncidence);
			if (id == null)
				throw new NotFoundException();

			return new ResponseEntity<>(new ResponseWrapper(id.toString()), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar inicidencia
	 * 
	 * @param idIncidence
	 * @return
	 */
	@RequestMapping(value = "/{idIncidence}/terms/{idTerm}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeIncidence(@PathVariable Integer idIncidence, @PathVariable Integer idTerm) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_INCIDENCE, token))
				throw new UnauthorizedResponseException();

			this.incidenceDAO.removeIncidenceFromTerm(idIncidence, idTerm);

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
