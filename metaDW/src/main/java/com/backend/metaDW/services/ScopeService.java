package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.IScopeDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.scope.FullScope;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/scopes")
public class ScopeService {

	@Autowired
	private IScopeDAO scopeDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String SCOPE = "ROLE_SCOPE";
	private static final String CREATION = "_CREATION";
	private static final String UPDATE = "_UPDATE";
	private static final String DELETE = "_DELETE";

	/**
	 * Crear un ámbito
	 * 
	 * @param scope
	 * @param auth
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createScope(@RequestBody Scope scope) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();			
			if (!this.jwtTokenHelper.hasRole(SCOPE + CREATION, token)) {
				throw new UnauthorizedResponseException();
			}

			Integer idScope = this.scopeDAO.saveScope(scope);

			if (idScope == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(new ResponseWrapper(idScope.toString()) , HttpStatus.CREATED);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los ámbitos paginados
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getScopes(Pageable page) {
		try {
			return new ResponseEntity<>(this.scopeDAO.findAllScopes(page), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar un ámbito
	 * 
	 * @param idScope
	 * @return
	 */
	@RequestMapping(value = "/{idScope}", method = RequestMethod.GET)
	public ResponseEntity<?> getScope(@PathVariable Integer idScope) {
		try {
			FullScope scope = this.scopeDAO.findScopeById(idScope);
			if (scope == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<>(scope, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar términos de un ámbito
	 * 
	 * @param idScope
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idScope}/terms", method = RequestMethod.GET)
	public ResponseEntity<?> getTermsByScope(@PathVariable Integer idScope, Pageable page) {
		try {

			Page<?> terms = this.scopeDAO.findTermsFromScope(idScope, page);
			if (terms == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<>(terms, HttpStatus.OK);
		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Actualizar un ámbito
	 * 
	 * @param idScope
	 * @param newScope
	 * @param auth
	 * @return
	 */
	@RequestMapping(value = "/{idScope}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateScope(@PathVariable Integer idScope, @RequestBody Scope newScope) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();	
			if (!this.jwtTokenHelper.hasRole(SCOPE + UPDATE, token)) {
				throw new UnauthorizedResponseException();
			}

			if (!this.scopeDAO.updateScopeData(newScope)) {
				throw new InternalServerException();
			}

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar un ámbito
	 * 
	 * @param idScope
	 * @param auth
	 * @return
	 */
	@RequestMapping(value = "/{idScope}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteScope(@PathVariable Integer idScope) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();	
			if (!this.jwtTokenHelper.hasRole(SCOPE + DELETE, token)) {
				throw new UnauthorizedResponseException();
			}
			this.scopeDAO.removeScope(idScope);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
