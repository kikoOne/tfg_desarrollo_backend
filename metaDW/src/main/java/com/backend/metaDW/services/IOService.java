package com.backend.metaDW.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.io.ExportHelper;
import com.backend.metaDW.io.ImportHelper;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/io")
public class IOService {

	@Autowired
	private ImportHelper importHelper;
	@Autowired
	private ExportHelper exportHelper;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String ADMIN_ROLE = "ROLE_SCOPE_CREATION";

	public static String ROOT = "upload-dir";

	@RequestMapping(value = "/imports/{type}", method = RequestMethod.POST)
	public ResponseEntity<?> importDataFromCSV(MultipartHttpServletRequest request, @PathVariable Integer type) {
		try {

			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!jwtTokenHelper.hasRole(ADMIN_ROLE, token))
				throw new UnauthorizedResponseException();

			Iterator<String> itr = request.getFileNames();
			MultipartFile file = request.getFile(itr.next());
			if (file.isEmpty())
				throw new IOException();
			boolean result = importHelper.importDataFromCSV(file, type, token.getJwt().getTokenId(),
					request.getServletContext().getRealPath("/"));

			if (!result)
				throw new InternalServerException();
			return new ResponseEntity<>(new ResponseWrapper("Successful import"), HttpStatus.CREATED);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (IOException | IllegalStateException e) {
			return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/exports/{type}", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> exportDataFromCSV(@PathVariable Integer type, Authentication auth) {
		try {
			// System.out.println("in");
			File file = this.exportHelper.exportDataFromCSV(type);
			// System.out.println("ok");

			HttpHeaders respHeaders = new HttpHeaders();
			respHeaders.setContentType(new MediaType("text", "csv"));
			respHeaders.setContentLength(file.length());
			respHeaders.setContentDispositionFormData("attachment", file.getName());

			InputStreamResource isr = new InputStreamResource(new FileInputStream(file));

			return new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace().toString());
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause().toString());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
