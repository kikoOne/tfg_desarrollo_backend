package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.IAnalyticalApplicativeDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/analytical_applicatives")
public class AnalyticalApplicativeService {

	@Autowired
	private IAnalyticalApplicativeDAO analyticalApplicativeDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String ADMIN_ANALYTICALAPPLICATIVE = "ROLE_ADMIN_ANALYTICALAPPLICATIVE";

	/**
	 * Crear nuevo aplicativo analítico
	 * 
	 * @param analyticalApplicative
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createAnalyticalApplicative(@RequestBody AnalyticalApplicative analyticalApplicative) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_ANALYTICALAPPLICATIVE, token))
				throw new UnauthorizedResponseException();

			Integer id = this.analyticalApplicativeDAO.saveAnalyticalApplicative(analyticalApplicative);
			if (id == null)
				throw new InternalServerException();
			return new ResponseEntity<>(new ResponseWrapper(id.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los aplicativos analíticos paginados
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getAnalyticalApplicatives(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_ANALYTICALAPPLICATIVE, token))
				throw new UnauthorizedResponseException();

			Page<?> analyticalApplicatives = this.analyticalApplicativeDAO.getAnalyticalApplicatives(page);
			if (analyticalApplicatives == null)
				throw new NotFoundException();

			return new ResponseEntity<>(analyticalApplicatives, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recupear un aplicativo analítico por id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAnalyticalApplicative(@PathVariable Integer id) {
		try {
			// JWTToken token = (JWTToken)
			// SecurityContextHolder.getContext().getAuthentication();
			// if (!this.jwtTokenHelper.hasRole(ADMIN_ANALYTICALAPPLICATIVE,
			// token))
			// throw new UnauthorizedResponseException();

			AnalyticalApplicative analyticalApplicative = this.analyticalApplicativeDAO.getAnalyticalApplicative(id);
			if (analyticalApplicative == null)
				throw new NotFoundException();

			return new ResponseEntity<>(analyticalApplicative, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateAnalyticalApplicative(@RequestBody AnalyticalApplicative analyticalApplicative) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_ANALYTICALAPPLICATIVE, token))
				throw new UnauthorizedResponseException();

			Integer id = this.analyticalApplicativeDAO.updateAnalyticalApplicative(analyticalApplicative);
			if (id == null)
				throw new NotFoundException();

			return new ResponseEntity<>(new ResponseWrapper(id.toString()), HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar aplicativo analítico por id
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeAnalyticalApplicatives(@PathVariable Integer id) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_ANALYTICALAPPLICATIVE, token))
				throw new UnauthorizedResponseException();

			this.analyticalApplicativeDAO.removeAnalyticalApplicative(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);

		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
