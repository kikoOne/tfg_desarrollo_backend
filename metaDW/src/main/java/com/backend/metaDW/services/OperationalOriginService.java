package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.IOperationalOriginDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/operational_origins")
public class OperationalOriginService {

	@Autowired
	private IOperationalOriginDAO operationalOriginDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String ADMIN_OPERATIONALORIGIN = "ROLE_ADMIN_OPERATIONALORIGIN";

	/**
	 * 
	 * @param operationalOrigin
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createOperationalOrigin(@RequestBody OperationalOrigin operationalOrigin) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			System.out.println("1");
			if (!this.jwtTokenHelper.hasRole(ADMIN_OPERATIONALORIGIN, token))
				throw new UnauthorizedResponseException();

			System.out.println("2");
			Integer id = this.operationalOriginDAO.saveOperationalOrigin(operationalOrigin);
			System.out.println("user" + id);
			if (id == null)
				throw new InternalServerException();

			return new ResponseEntity<>(new ResponseWrapper(id.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getOperationalOrigins(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_OPERATIONALORIGIN, token))
				throw new UnauthorizedResponseException();

			Page<?> operationalOrigins = this.operationalOriginDAO.getOperationalOrigins(page);
			if (operationalOrigins == null)
				throw new InternalServerException();

			return new ResponseEntity<>(operationalOrigins, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getOperationalOrigin(@PathVariable Integer id) {
		try {
			// JWTToken token = (JWTToken)
			// SecurityContextHolder.getContext().getAuthentication();
			// if (!this.jwtTokenHelper.hasRole(ADMIN_OPERATIONALORIGIN, token))
			// throw new UnauthorizedResponseException();

			OperationalOrigin operationalOrigin = this.operationalOriginDAO.getOperationalOrigin(id);
			if (operationalOrigin == null)
				throw new NotFoundException();

			return new ResponseEntity<>(operationalOrigin, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param id
	 * @param operationalOrigin
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateOperationalOrigin(@PathVariable Integer id,
			@RequestBody OperationalOrigin operationalOrigin) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_OPERATIONALORIGIN, token))
				throw new UnauthorizedResponseException();

			Integer result = this.operationalOriginDAO.updateOperationalOrigin(operationalOrigin);
			if (result == null)
				throw new NotFoundException();

			return new ResponseEntity<>(operationalOrigin, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param id
	 * @param operationalOrigin
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeOperationalOrigin(@PathVariable Integer id) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(ADMIN_OPERATIONALORIGIN, token))
				throw new UnauthorizedResponseException();

			this.operationalOriginDAO.removeOperationalOrigin(id);

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
