package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.IDraftDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.drafts.DraftCategory;
import com.backend.metaDW.model.vo.drafts.DraftDimension;
import com.backend.metaDW.model.vo.drafts.DraftIndicator;
import com.backend.metaDW.model.vo.drafts.DraftInform;
import com.backend.metaDW.model.vo.drafts.DraftObjective;
import com.backend.metaDW.model.vo.drafts.DraftScope;
import com.backend.metaDW.model.vo.drafts.DraftTerm;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.CreationTerm;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/drafts")
public class DraftService {

	@Autowired
	private IDraftDAO draftDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String SCOPE = "ROLE_SCOPE";
	private static final String CATEGORY = "ROLE_CATEGORY";
	private static final String TERM = "ROLE_TERM";
	private static final String CREATION = "_CREATION";

	@RequestMapping(value = "/scopes", method = RequestMethod.POST)
	public ResponseEntity<?> addDraftScope(@RequestBody Scope scope) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(SCOPE + CREATION, token)
					|| !token.getJwt().getTokenId().equals(scope.getOwner().toString())) {
				throw new UnauthorizedResponseException();
			}
			Integer idScope = this.draftDAO.addDraftScope(scope);
			if (idScope == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(new ResponseWrapper(idScope.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/scopes", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftScopesFromUser(Pageable page) {
		try {			
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			
			Page<DraftScope> result = this.draftDAO
					.getDraftScopesFromUser(Integer.parseInt(token.getJwt().getTokenId()), page);
			if (result == null)
				throw new InternalServerException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/scopes/{idDraft}", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftScope(@PathVariable Integer idDraft, Pageable page) {
		try {
			System.out.println("1");
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			System.out.println("2");
			
			DraftScope result = this.draftDAO.getDraftScope(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/scopes/{idDraft}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateDraftScope(@PathVariable Integer idDraft, @RequestBody Scope scope) {

		try {

			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(SCOPE + CREATION, token)
					|| !token.getJwt().getTokenId().equals(scope.getOwner().toString()))
				throw new UnauthorizedResponseException();

			Integer idScope = this.draftDAO.updateDraftScope(idDraft, scope);
			if (idScope == null)
				throw new InternalServerException();

			return new ResponseEntity<>(new ResponseWrapper(idScope.toString()), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/scopes/{idDraft}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeDraftScope(@PathVariable Integer idDraft) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(SCOPE + CREATION, token))
				throw new UnauthorizedResponseException();

			this.draftDAO.removeDraftScope(idDraft, Integer.parseInt(token.getJwt().getTokenId()));

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/categories", method = RequestMethod.POST)
	public ResponseEntity<?> addDraftCategory(@RequestBody CreationCategory category) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + CREATION, token)
					|| !token.getJwt().getTokenId().equals(category.getOwner().toString())) {
				throw new UnauthorizedResponseException();
			}
			Integer idCategory = this.draftDAO.addDraftCategory(category);
			if (idCategory == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(new ResponseWrapper(idCategory.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftCategoriesFromUser(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			Page<DraftCategory> result = this.draftDAO
					.getDraftCategoriesFromUser(Integer.parseInt(token.getJwt().getTokenId()), page);
			if (result == null)
				throw new InternalServerException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/categories/{idDraft}", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftCategory(@PathVariable Integer idDraft) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			DraftCategory result = this.draftDAO.getDraftCategory(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/categories/{idDraft}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateDraftCategory(@PathVariable Integer idDraft,
			@RequestBody CreationCategory category) {

		try {

			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + CREATION, token)
					|| !token.getJwt().getTokenId().equals(category.getOwner().toString()))
				throw new UnauthorizedResponseException();

			Integer idCategory = this.draftDAO.updateDraftCategory(idDraft, category);
			if (idCategory == null)
				throw new InternalServerException();

			return new ResponseEntity<>(new ResponseWrapper(idCategory.toString()), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/categories/{idDraft}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeDraftCategory(@PathVariable Integer idDraft) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + CREATION, token))
				throw new UnauthorizedResponseException();

			this.draftDAO.removeDraftCategory(idDraft, Integer.parseInt(token.getJwt().getTokenId()));

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/terms", method = RequestMethod.POST)
	public ResponseEntity<?> addDraftTerm(@RequestBody CreationTerm term) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + CREATION, token)
					|| !token.getJwt().getTokenId().equals(term.getMainTerm().getOwner().toString())) {
				throw new UnauthorizedResponseException();
			}
			Integer idTerm = this.draftDAO.addDraftTerm(term);
			if (idTerm == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(new ResponseWrapper(idTerm.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/terms", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftTermsFromUser(Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			Page<DraftTerm> result = this.draftDAO.getDraftTermsFromUser(Integer.parseInt(token.getJwt().getTokenId()),
					page);
			if (result == null)
				throw new InternalServerException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/terms/{idDraft}", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftTerm(@PathVariable Integer idDraft, Pageable page) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			DraftTerm result = this.draftDAO.getDraftTerm(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();

			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/terms/{idDraft}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateDraftTerm(@PathVariable Integer idDraft, @RequestBody CreationTerm term) {

		try {

			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + CREATION, token)
					|| !token.getJwt().getTokenId().equals(term.getMainTerm().getOwner().toString()))
				throw new UnauthorizedResponseException();

			Integer idCategory = this.draftDAO.updateDraftTerm(idDraft, term);
			if (idCategory == null)
				throw new InternalServerException();

			return new ResponseEntity<>(new ResponseWrapper(idCategory.toString()), HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/terms/{idDraft}", method = RequestMethod.DELETE)
	public ResponseEntity<?> removeDraftTerm(@PathVariable Integer idDraft) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(TERM + CREATION, token))
				throw new UnauthorizedResponseException();

			this.draftDAO.removeDraftTerm(idDraft, Integer.parseInt(token.getJwt().getTokenId()));

			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/terms/{idDraft}/dimensions", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftTermDimension(@PathVariable Integer idDraft) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			DraftTerm result = this.draftDAO.getDraftTerm(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();
			DraftDimension dimension = this.draftDAO.getDraftDimension(idDraft);

			return new ResponseEntity<>(dimension, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/terms/{idDraft}/indicators", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftTermIndicator(@PathVariable Integer idDraft) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			DraftTerm result = this.draftDAO.getDraftTerm(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();
			DraftIndicator indicator = this.draftDAO.getDraftIndicator(idDraft);

			return new ResponseEntity<>(indicator, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/terms/{idDraft}/objectives", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftTermObjective(@PathVariable Integer idDraft) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			DraftTerm result = this.draftDAO.getDraftTerm(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();
			DraftObjective objective = this.draftDAO.getDraftObjective(idDraft);

			return new ResponseEntity<>(objective, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/terms/{idDraft}/informs", method = RequestMethod.GET)
	public ResponseEntity<?> getDraftTermInform(@PathVariable Integer idDraft) {
		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (token == null)
				throw new UnauthorizedResponseException();
			DraftTerm result = this.draftDAO.getDraftTerm(idDraft);
			if (result == null)
				throw new InternalServerException();
			if (!result.getOwner().equals(Integer.parseInt(token.getJwt().getTokenId())))
				throw new UnauthorizedResponseException();
			DraftInform inform = this.draftDAO.getDraftInform(idDraft);

			return new ResponseEntity<>(inform, HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
