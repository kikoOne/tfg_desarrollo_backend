package com.backend.metaDW.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.ICategoryDAO;
import com.backend.metaDW.model.vo.ResponseWrapper;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.category.FullCategory;
import com.backend.metaDW.security.jwt.JWTToken;
import com.backend.metaDW.security.jwt.JwtTokenHelper;

@RestController
@RequestMapping("/1.0/metadw/categories")
public class CategoryService {

	@Autowired
	private ICategoryDAO categoryDAO;
	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	private static final String CATEGORY = "ROLE_CATEGORY";
	private static final String CREATION = "_CREATION";
	private static final String UPDATE = "_UPDATE";
	private static final String DELETE = "_DELETE";

	/**
	 * Crear una categoria
	 * 
	 * @param category
	 * @param auth
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> createCategory(@RequestBody CreationCategory category) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + CREATION, token)) {
				throw new UnauthorizedResponseException();
			}
			Integer idCategory = this.categoryDAO.createCategoryTransaction(category);
			if (idCategory == null) {
				throw new InternalServerException();
			}
			return new ResponseEntity<>(new ResponseWrapper(idCategory.toString()), HttpStatus.CREATED);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	/**
	 * Recuperar una categoría
	 * 
	 * @param idCategory
	 * @return
	 */
	@RequestMapping(value = "/{idCategory}", method = RequestMethod.GET)
	public ResponseEntity<?> getCategory(@PathVariable Integer idCategory) {
		try {

			FullCategory category = this.categoryDAO.findCategoryById(idCategory);
			if (category == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<>(category, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * 
	 * @param idCategory
	 * @return
	 */
	@RequestMapping(value = "/{idCategory}/parent", method = RequestMethod.GET)
	public ResponseEntity<?> getParentCategory(@PathVariable Integer idCategory) {
		try {

			Category category = this.categoryDAO.findParentCategory(idCategory);
			if (category == null) {
				throw new NotFoundException();
			}
			return new ResponseEntity<>(category, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las categorías raíz
	 * 
	 * @param page
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getRootCategories(Pageable page) {
		try {
			return new ResponseEntity<>(this.categoryDAO.findCategoriesByType(-1, page), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar las subcategorías de una categoría
	 * 
	 * @param idCategory
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idCategory}/subcategories", method = RequestMethod.GET)
	public ResponseEntity<?> getCategoriesFromCategory(@PathVariable Integer idCategory, Pageable page) {
		try {
			Page<?> subcategories = this.categoryDAO.findSubcategoriesFromCategory(idCategory, page);

			if (subcategories == null)
				throw new NotFoundException();
			return new ResponseEntity<>(subcategories, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Recuperar los términos de una categoría
	 * 
	 * @param idCategory
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/{idCategory}/terms", method = RequestMethod.GET)
	public ResponseEntity<?> getTermsFromCategory(@PathVariable Integer idCategory, Pageable page) {
		try {
			Page<?> terms = this.categoryDAO.findTermsFromCategory(idCategory, page);

			if (terms == null)
				throw new NotFoundException();
			return new ResponseEntity<>(terms, HttpStatus.OK);

		} catch (NotFoundException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Actualizar categoría
	 * 
	 * @param idCategory
	 * @param category
	 * @return
	 */
	@RequestMapping(value = "/{idCategory}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateCategory(@PathVariable String idCategory, @RequestBody CreationCategory category) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + UPDATE, token)) {
				throw new UnauthorizedResponseException();
			}

			this.categoryDAO.updateCategory(category);

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Eliminar categoría
	 * 
	 * @param idCategory
	 * @return
	 */
	@RequestMapping(value = "/{idCategory}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCategory(@PathVariable Integer idCategory) {

		try {
			JWTToken token = (JWTToken) SecurityContextHolder.getContext().getAuthentication();
			if (!this.jwtTokenHelper.hasRole(CATEGORY + DELETE, token)) {
				throw new UnauthorizedResponseException();
			}
			this.categoryDAO.removeCategory(idCategory);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (UnauthorizedResponseException e) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
