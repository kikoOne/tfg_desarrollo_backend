package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftDimension;

public interface DraftDimensionRepository extends PagingAndSortingRepository<DraftDimension, Integer>{

}
