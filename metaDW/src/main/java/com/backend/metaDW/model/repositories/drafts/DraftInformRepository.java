package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftInform;

public interface DraftInformRepository extends PagingAndSortingRepository<DraftInform, Integer>{

}
