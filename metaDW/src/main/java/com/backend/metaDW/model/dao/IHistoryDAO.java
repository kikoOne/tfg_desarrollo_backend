package com.backend.metaDW.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.history.HistoryCategory;
import com.backend.metaDW.model.vo.history.HistoryDimension;
import com.backend.metaDW.model.vo.history.HistoryIndicator;
import com.backend.metaDW.model.vo.history.HistoryInform;
import com.backend.metaDW.model.vo.history.HistoryObjective;
import com.backend.metaDW.model.vo.history.HistoryScope;
import com.backend.metaDW.model.vo.history.HistoryTerm;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.Term;

public interface IHistoryDAO {

	public Integer addScopeHistoryVersion(Scope scope);

	public Page<HistoryScope> getHistoryFromScope(Integer idScope, Pageable page);

	public HistoryScope getScopeHistoryVersion(Integer idScope, Integer idVersion);

	public Integer addCategoryHistoryVersion(Category category);

	public Page<HistoryCategory> getHistoryFromCategory(Integer idCategory, Pageable page);

	public HistoryCategory getCategoryHistoryVersion(Integer idCategory, Integer idVersion);

	public Integer addTermHistoryVersion(Term term);

	public Page<HistoryTerm> getHistoryFromTerm(Integer idTerm, Pageable page);

	public HistoryTerm getTermHistoryVersion(Integer idTerm, Integer idVersion);

	public HistoryDimension getDimensionHistoryVersion(Integer idVersion);

	public HistoryIndicator getIndicatorHistoryVersion(Integer idVersion);

	public HistoryObjective getObjectiveHistoryVersion(Integer idVersion);
	
	public HistoryInform getInformHistoryVersion(Integer idVersion);
}
