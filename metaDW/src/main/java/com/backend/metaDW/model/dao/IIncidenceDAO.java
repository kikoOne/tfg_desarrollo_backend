package com.backend.metaDW.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.term.FullIncidence;
import com.backend.metaDW.model.vo.term.Incidence;

public interface IIncidenceDAO {

	public Integer saveIncidence(Incidence incidence);
	
	public Page<Incidence> getIncidences(Pageable page);
	
	public List<Incidence> getAllIncidences();
	
	public  Page<FullIncidence> getFullIncidences(Pageable page);
	
	public List<Incidence> getIncidencesFromTerm(Integer idTerm);
	
	public Integer updateIncidence(Incidence newIncidence);
	
	public void removeIncidenceFromTerm(Integer idIncidence, Integer idTerm);
}
