package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftIndicator;

public interface DraftIndicatorRepository extends PagingAndSortingRepository<DraftIndicator, Integer>{

}
