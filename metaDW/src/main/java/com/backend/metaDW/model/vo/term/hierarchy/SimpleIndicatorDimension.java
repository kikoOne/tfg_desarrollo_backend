package com.backend.metaDW.model.vo.term.hierarchy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(SimpleIndicatorsDimensionPK.class)
@Table(name = "indicatorsdimensions", schema = "public")
public class SimpleIndicatorDimension {

	@Id
	@Column(name = "indicator")
	private Integer indicator;
	
	@Id
	@Column(name = "dimension")
	private Integer dimension;
	
	/**
	 * 
	 */
	public SimpleIndicatorDimension() {
	}

	/**
	 * @param indicator
	 * @param dimension
	 */
	public SimpleIndicatorDimension(Integer indicator, Integer dimension) {
		this.indicator = indicator;
		this.dimension = dimension;
	}

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + ((indicator == null) ? 0 : indicator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleIndicatorDimension other = (SimpleIndicatorDimension) obj;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (indicator == null) {
			if (other.indicator != null)
				return false;
		} else if (!indicator.equals(other.indicator))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleIndicatorDimension [indicator=" + indicator + ", dimension=" + dimension + "]";
	}
}

@SuppressWarnings("serial")
class SimpleIndicatorsDimensionPK implements Serializable {

	private Integer indicator;
	private Integer dimension;

	/**
	 * 
	 */
	public SimpleIndicatorsDimensionPK() {
	}
	
	/**
	 * @param indicator
	 * @param dimension
	 */
	public SimpleIndicatorsDimensionPK(Integer indicator, Integer dimension) {
		this.indicator = indicator;
		this.dimension = dimension;
	}

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

}
