package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftTerm;

public interface DraftTermRepository extends PagingAndSortingRepository<DraftTerm, Integer>{

	public Page<DraftTerm> findAllByOwner(Integer owner, Pageable page);
}
