package com.backend.metaDW.model.repositories.categories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.category.Category;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer>{
	
	Page<?> findByType(int type, Pageable page);
	
	Page<Category> findByNameContainingIgnoreCase(String name, Pageable page);
	
	Page<Category> findByNameContainingIgnoreCaseAndOwner(String name, Integer owner, Pageable page);
	
	Page<Category> findByTypeInAndNameContainingIgnoreCase(List<Integer> type, String name, Pageable page);
}
