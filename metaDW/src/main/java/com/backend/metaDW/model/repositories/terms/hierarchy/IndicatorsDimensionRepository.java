package com.backend.metaDW.model.repositories.terms.hierarchy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.IndicatorsDimension;

public interface IndicatorsDimensionRepository extends PagingAndSortingRepository<IndicatorsDimension, Integer>{

	public Page<IndicatorsDimension> findByIndicator(Integer idIndicator, Pageable page);
	
	public List<IndicatorsDimension> findAllByIndicator(Integer idIndicator);
}
