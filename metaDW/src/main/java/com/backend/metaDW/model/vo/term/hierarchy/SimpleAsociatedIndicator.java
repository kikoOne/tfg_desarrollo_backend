package com.backend.metaDW.model.vo.term.hierarchy;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(SimpleAsociatedIndicatorPK.class)
@Table(name = "asociatedindicators", schema = "public")
public class SimpleAsociatedIndicator {

	@Id
	@Column(name = "objective")
	private Integer objective;

	@Id
	@Column(name = "indicator")
	private Integer indicator;
	
	/**
	 * void constructor
	 */
	public SimpleAsociatedIndicator() {
	}
	
	/**
	 * @param objective
	 * @param indicator
	 */
	public SimpleAsociatedIndicator(Integer objective, Integer indicator) {
		this.objective = objective;
		this.indicator = indicator;
	}
	public Integer getObjective() {
		return objective;
	}
	public void setObjective(Integer objective) {
		this.objective = objective;
	}
	public Integer getIndicator() {
		return indicator;
	}
	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indicator == null) ? 0 : indicator.hashCode());
		result = prime * result + ((objective == null) ? 0 : objective.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleAsociatedIndicator other = (SimpleAsociatedIndicator) obj;
		if (indicator == null) {
			if (other.indicator != null)
				return false;
		} else if (!indicator.equals(other.indicator))
			return false;
		if (objective == null) {
			if (other.objective != null)
				return false;
		} else if (!objective.equals(other.objective))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AsociatedIndicator [objective=" + objective + ", indicator=" + indicator + "]";
	}
}

@SuppressWarnings("serial")
class SimpleAsociatedIndicatorPK implements Serializable{

	private Integer objective;
	private Integer indicator;
	
	/**
	 * 
	 */
	public SimpleAsociatedIndicatorPK() {
	}
	/**
	 * @param objective
	 * @param indicator
	 */
	public SimpleAsociatedIndicatorPK(Integer objective, Integer indicator) {
		this.objective = objective;
		this.indicator = indicator;
	}
	public Integer getObjective() {
		return objective;
	}
	public void setObjective(Integer objective) {
		this.objective = objective;
	}
	public Integer getIndicator() {
		return indicator;
	}
	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}
}
