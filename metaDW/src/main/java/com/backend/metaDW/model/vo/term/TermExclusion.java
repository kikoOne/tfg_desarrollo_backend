package com.backend.metaDW.model.vo.term;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@IdClass(TermExclusionPK.class)
@Table(name = "termexclusions", schema = "public")
public class TermExclusion {

	@Id
	@SequenceGenerator(name = "termexclusions_id_exclusion_seq", sequenceName = "termexclusions_id_exclusion_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "termexclusions_id_exclusion_seq")
	@Column(name = "id_exclusion", updatable = false)
	private Integer idExclusion;

	@Id
	@Column(name = "id_term")
	private Integer idTerm;

	@Column(name = "exclusion")
	private String exclusion;

	/**
	 * void
	 */
	public TermExclusion() {
	}

	/**
	 * @param idExclusion
	 * @param idTerm
	 */
	public TermExclusion(Integer idExclusion, Integer idTerm) {
		this.idExclusion = idExclusion;
		this.idTerm = idTerm;
	}

	/**
	 * @param idTerm
	 * @param exclusion
	 * @param idExclusion
	 */
	public TermExclusion(Integer idTerm,  Integer idExclusion, String exclusion) {
		this.idTerm = idTerm;
		this.idExclusion = idExclusion;
		this.exclusion = exclusion;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer id_term) {
		this.idTerm = id_term;
	}

	public String getExclusion() {
		return exclusion;
	}

	public void setExclusion(String exclusion) {
		this.exclusion = exclusion;
	}

	public Integer getIdExclusion() {
		return idExclusion;
	}

	public void setIdExclusion(Integer idExclusion) {
		this.idExclusion = idExclusion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExclusion == null) ? 0 : idExclusion.hashCode());
		result = prime * result + ((exclusion == null) ? 0 : exclusion.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermExclusion other = (TermExclusion) obj;
		if (idExclusion == null) {
			if (other.idExclusion != null)
				return false;
		} else if (!idExclusion.equals(other.idExclusion))
			return false;
		if (exclusion == null) {
			if (other.exclusion != null)
				return false;
		} else if (!exclusion.equals(other.exclusion))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermExample [id_term=" + idTerm + ", denomination=" + idExclusion + ", id_denomination=" + exclusion
				+ "]";
	}
}

@SuppressWarnings("serial")
class TermExclusionPK implements Serializable {

	private Integer idExclusion;
	private Integer idTerm;

	public Integer getIdExample() {
		return idExclusion;
	}

	public void setIdExample(Integer idExample) {
		this.idExclusion = idExample;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}
}
