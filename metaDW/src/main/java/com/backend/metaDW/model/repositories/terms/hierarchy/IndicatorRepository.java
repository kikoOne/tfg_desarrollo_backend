package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.Indicator;

public interface IndicatorRepository extends PagingAndSortingRepository<Indicator, Integer>{

}
