package com.backend.metaDW.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.user.FullExternalUser;
import com.backend.metaDW.model.vo.user.TermFollowed;
import com.backend.metaDW.model.vo.user.User;
import com.backend.metaDW.model.vo.user.security.Role;

public interface IUserDAO {

	public void saveUser(User user);
	
	public Role saveRole(Role role);
	
	public List<Role> getAllRoles();

	public Page<FullExternalUser> findAllUsers(Pageable page);
	
	public Page<FullExternalUser> findAllActivatedUsers(Pageable page);
	
	public Page<FullExternalUser> findAllDesactivatedUsers(Pageable page);

	public FullExternalUser findUserById(Integer idUser);
	
	public User findUserByUserName(String userName);

	public boolean updateUserData(FullExternalUser user);
	
	public boolean updatePassword(Integer idUser, String password, String newPassword);

	public boolean desactivateUser(Integer idUser);

	public Page<Term> findTermsFromOwnerByNameAndType(Integer owner, String query, List<Integer> types, Pageable page);
	
	public Page<Scope> findScopesFromOwnerByName(Integer owner, String query, Pageable page);
	
	public Page<Category> findCategoriesFromOwnerByName(Integer owner, String query, Pageable page);
	
	public Integer activateUser(Integer idUser);
	
	public boolean addTermBookmark(Integer idUser, Integer idTerm);
	
	public TermFollowed getFollowedTerm(Integer idUser, Integer idTerm);
	
	public Page<TermFollowed> getFollowedTerms(Integer idUser, Pageable page);
	
	public List<TermFollowed> getAllFollowedTerms(Integer idUser);
	
	public void removeFollowedTerm(Integer idUser, Integer idTerm);
}
