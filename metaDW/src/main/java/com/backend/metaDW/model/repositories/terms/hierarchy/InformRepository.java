package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.Inform;

public interface InformRepository extends PagingAndSortingRepository<Inform, Integer> {

	public Inform findOneByIdTerm(Integer idTerm);
}
