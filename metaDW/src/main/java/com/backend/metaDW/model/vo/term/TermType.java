package com.backend.metaDW.model.vo.term;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "termtypes", schema = "public")
public class TermType {
	
	@Id
	@SequenceGenerator(name = "termtypes_type_seq", sequenceName = "termtypes_type_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "termtypes_type_seq")
	@Column(name = "type", updatable = false)
	private Integer type;
	
	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;
	
	public TermType(){
		
	}
	
	public TermType(Integer type){
		this.type = type;
	}

	/**
	 * @param name
	 */
	public TermType(String name) {
		this.name = name;
	}

	public TermType(Integer type, String name, String description) {
		this.type = type;
		this.name = name;
		this.description = description;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermType other = (TermType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermType [type=" + type + ", name=" + name + ", description=" + description + "]";
	}
}
