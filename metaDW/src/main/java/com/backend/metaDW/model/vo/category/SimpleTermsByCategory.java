package com.backend.metaDW.model.vo.category;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(SimpleTermsByCategoryPK.class)
@Table(name = "termsbycategory", schema = "public")
public class SimpleTermsByCategory {

	@Id
	@Column(name = "id_term")
	private Integer idTerm; 
	
	@Id
	@Column(name = "id_category")
	private Integer idCategory;

	/**
	 * 
	 */
	public SimpleTermsByCategory() {
	}

	/**
	 * @param idTerm
	 * @param idCategory
	 */
	public SimpleTermsByCategory(Integer idTerm, Integer idCategory) {
		this.idTerm = idTerm;
		this.idCategory = idCategory;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	public Integer getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idCategory == null) ? 0 : idCategory.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleTermsByCategory other = (SimpleTermsByCategory) obj;
		if (idCategory == null) {
			if (other.idCategory != null)
				return false;
		} else if (!idCategory.equals(other.idCategory))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleTermsByCategory [idTerm=" + idTerm + ", idCategory=" + idCategory + "]";
	}
}

@SuppressWarnings("serial")
class SimpleTermsByCategoryPK implements Serializable {

	private Integer idCategory;
	private Integer idTerm;

	public Integer getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}
}
