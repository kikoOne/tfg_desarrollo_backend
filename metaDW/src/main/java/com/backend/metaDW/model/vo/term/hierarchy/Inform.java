package com.backend.metaDW.model.vo.term.hierarchy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "informs", schema = "public")
public class Inform {

	@Id
	@Column(name = "id_term")
	private Integer idTerm;
	
	@Column(name = "link")
	private String link;

	/**
	 * void
	 */
	public Inform() {
	}

	/**
	 * @param idTerm
	 */
	public Inform(Integer idTerm) {
		this.idTerm = idTerm;
	}

	/**
	 * @param idTerm
	 * @param link
	 */
	public Inform(Integer idTerm, String link) {
		this.idTerm = idTerm;
		this.link = link;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inform other = (Inform) obj;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Inform [idTerm=" + idTerm + ", link=" + link + "]";
	}
}
