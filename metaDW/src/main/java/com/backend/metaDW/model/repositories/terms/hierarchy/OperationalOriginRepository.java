package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;

public interface OperationalOriginRepository extends PagingAndSortingRepository<OperationalOrigin, Integer> {

	Page<OperationalOrigin> findByNameContainingIgnoreCase(String name, Pageable page);
}
