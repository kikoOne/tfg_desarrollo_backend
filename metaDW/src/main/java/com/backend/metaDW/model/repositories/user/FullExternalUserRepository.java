package com.backend.metaDW.model.repositories.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.backend.metaDW.model.vo.user.FullExternalUser;

public interface FullExternalUserRepository extends PagingAndSortingRepository<FullExternalUser, Integer> {

	public Page<FullExternalUser> findByNameContainingIgnoreCase(String name, Pageable page);

	public FullExternalUser findByUserName(String userName);

	public Page<FullExternalUser> findByState(Integer state, Pageable page);

	@Query("select u from FullExternalUser u WHERE u.state = :state AND (LOWER(u.name) LIKE CONCAT('%',LOWER(:name),'%') OR LOWER(u.surnames) LIKE CONCAT('%',LOWER(:surnames),'%') OR LOWER(u.userName) LIKE CONCAT('%',LOWER(:userName),'%'))")
	public Page<FullExternalUser> findByNameContainingIgnoreCaseOrSurnamesContainingIgnoreCaseOrUserNameContainingIgnoreCaseAndState(
			@Param("name") String name, @Param("surnames") String surnames, @Param("userName") String userName, @Param("state") Integer state, Pageable page);
}
