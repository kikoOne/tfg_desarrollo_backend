package com.backend.metaDW.model.repositories.categories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.Subcategory;

public interface SubcategoryRepository extends PagingAndSortingRepository<Subcategory, Integer>{
	
	public Page<?> findByIdParent(Integer idParent, Pageable page);
	
	@Query("SELECT s.child FROM Subcategory s WHERE s.idParent =:idParent")
	public Page<?> findSubcategoriesFromParent(@Param ("idParent") Integer idParent, Pageable page);
	
	@Query("SELECT s.child FROM Subcategory s WHERE s.idParent =:idParent")
	public List<?> findAllSubcategoriesFromParent(@Param ("idParent") Integer idParent);
	
	@Query("SELECT s.child FROM Subcategory s WHERE s.idParent =:idParent AND LOWER(s.child.name) LIKE CONCAT('%',LOWER(:name),'%')")
	public Page<?> findSubcategoriesByNameFromParent(@Param("name") String name, @Param ("idParent") Integer idParent, Pageable page);
	
	public Page<Category> findByIdParent(String idParent, Pageable page);
}
