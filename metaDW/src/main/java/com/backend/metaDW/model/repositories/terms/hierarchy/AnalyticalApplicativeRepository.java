package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;

public interface AnalyticalApplicativeRepository extends PagingAndSortingRepository<AnalyticalApplicative, Integer> {

	public Page<AnalyticalApplicative> findByNameContainingIgnoreCase(String name, Pageable page);
}
