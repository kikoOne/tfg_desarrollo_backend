package com.backend.metaDW.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.category.TermsByCategory;
import com.backend.metaDW.model.vo.scope.TermsByScope;
import com.backend.metaDW.model.vo.term.CreationTerm;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.FullTerm;
import com.backend.metaDW.model.vo.term.Incidence;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.TermExample;
import com.backend.metaDW.model.vo.term.TermExclusion;
import com.backend.metaDW.model.vo.term.TermType;
import com.backend.metaDW.model.vo.term.hierarchy.AsociatedIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.FullDimension;
import com.backend.metaDW.model.vo.term.hierarchy.FullIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.FullObjective;
import com.backend.metaDW.model.vo.term.hierarchy.IndicatorsDimension;
import com.backend.metaDW.model.vo.term.hierarchy.Inform;
import com.backend.metaDW.model.vo.term.hierarchy.InformIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.ObjectivesDimension;

public interface ITermDAO {

	public Integer createTermTransaction(CreationTerm ct);

	public void createTermTransaction(List<CreationTerm> ct);

	public FullTerm findTermById(Integer idTerm);

	public Page<?> getExamplesFromTerm(Integer idTerm, Pageable page);

	public List<TermExample> getAllExamplesFromTerm(Integer idTerm);

	public Page<TermExclusion> getExclusionsFromTerm(Integer idTerm, Pageable page);

	public List<TermExclusion> getAllExclusionsFromTerm(Integer idTerm);

	public Page<Denomination> getDenominationsFromTerm(Integer idTerm, Pageable page);

	public List<Denomination> getAllDenominationsFromTerm(Integer idTerm);

	public boolean updateTerm(CreationTerm term);

	public void removeTerm(Integer idTerm);

	public Iterable<TermType> findAllTermTypes();
	
	public TermType saveTermType(TermType type);

	public TermsByScope getScopeFromTerm(Integer idTerm);

	public List<TermsByCategory> getCategoriesFromTerm(Integer idTerm);

	public FullDimension getDimensionFromTerm(Integer idTerm);

	public FullIndicator getIndicatorFromTerm(Integer idTerm);

	public FullObjective getObjectiveFromTerm(Integer idTerm);

	public Inform getInformFromTerm(Integer idTerm);

	public Page<Incidence> getIncidencesFromTerm(Integer idTerm, Pageable page);

	public Page<AsociatedIndicator> getAsociatedIndicatorsFromTerm(Integer idObjective, Pageable page);
	
	public List<AsociatedIndicator> getAllAsociatedIndicatorsFromTerm(Integer idObjective);

	public Page<InformIndicator> getInformIndicators(Integer idInform, Pageable page);
	
	public List<InformIndicator> getAllInformIndicators(Integer idInform);

	public Page<IndicatorsDimension> getDimensionIndicators(Integer idIndicator, Pageable page);

	public List<IndicatorsDimension> getAllDimensionIndicators(Integer idIndicator);

	public Page<ObjectivesDimension> getObjectiveDimensions(Integer idObjective, Pageable page);

	public List<ObjectivesDimension> getAllObjectiveDimensions(Integer idObjective);

	public List<Term> findAllTermsByType(Integer type);
	
	public Page<FullTerm> getTerms(Pageable page);
}
