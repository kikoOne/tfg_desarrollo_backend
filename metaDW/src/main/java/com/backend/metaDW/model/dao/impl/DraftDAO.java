package com.backend.metaDW.model.dao.impl;

import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.model.dao.IDraftDAO;
import com.backend.metaDW.model.repositories.drafts.DraftCategoryRepository;
import com.backend.metaDW.model.repositories.drafts.DraftDimensionRepository;
import com.backend.metaDW.model.repositories.drafts.DraftIndicatorRepository;
import com.backend.metaDW.model.repositories.drafts.DraftInformRepository;
import com.backend.metaDW.model.repositories.drafts.DraftObjectiveRepository;
import com.backend.metaDW.model.repositories.drafts.DraftScopeRepository;
import com.backend.metaDW.model.repositories.drafts.DraftTermRepository;
import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.drafts.DraftCategory;
import com.backend.metaDW.model.vo.drafts.DraftDimension;
import com.backend.metaDW.model.vo.drafts.DraftIndicator;
import com.backend.metaDW.model.vo.drafts.DraftInform;
import com.backend.metaDW.model.vo.drafts.DraftObjective;
import com.backend.metaDW.model.vo.drafts.DraftScope;
import com.backend.metaDW.model.vo.drafts.DraftTerm;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.CreationTerm;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.hierarchy.SimpleObjectivesDimension;

@Service
public class DraftDAO implements IDraftDAO {

	private DraftScopeRepository draftScopeRepository;
	private DraftCategoryRepository draftCategoryRepository;
	private DraftTermRepository draftTermRepository;
	private DraftDimensionRepository draftDimensionRepository;
	private DraftIndicatorRepository draftIndicatorRepository;
	private DraftObjectiveRepository draftObjectiveRepository;
	private DraftInformRepository draftInformRepository;

	/**
	 * @param draftScopeRepository
	 * @param draftCategoryRepository
	 * @param draftTermRepository
	 * @param draftDimensionRepository
	 * @param draftIndicatorRepository
	 * @param draftObjectiveRepository
	 * @param draftInformRepository
	 */
	@Autowired
	public DraftDAO(DraftScopeRepository draftScopeRepository, DraftCategoryRepository draftCategoryRepository,
			DraftTermRepository draftTermRepository, DraftDimensionRepository draftDimensionRepository,
			DraftIndicatorRepository draftIndicatorRepository, DraftObjectiveRepository draftObjectiveRepository,
			DraftInformRepository draftInformRepository) {
		this.draftScopeRepository = draftScopeRepository;
		this.draftCategoryRepository = draftCategoryRepository;
		this.draftTermRepository = draftTermRepository;
		this.draftDimensionRepository = draftDimensionRepository;
		this.draftIndicatorRepository = draftIndicatorRepository;
		this.draftObjectiveRepository = draftObjectiveRepository;
		this.draftInformRepository = draftInformRepository;
	}

	@Override
	@Transactional
	public Integer addDraftScope(Scope scope) {

		DraftScope draftScope = new DraftScope(null, scope.getIdScope(), scope.getName(), scope.getDefinition(),
				scope.getVersion(), scope.getOwner(), null);

		DraftScope result = this.draftScopeRepository.save(draftScope);
		if (result == null)
			return null;
		return result.getId();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<DraftScope> getDraftScopesFromUser(Integer idUser, Pageable page) {
		return this.draftScopeRepository.findAllByOwner(idUser, page);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftScope getDraftScope(Integer idDraft) {
		return this.draftScopeRepository.findOne(idDraft);
	}

	@Override
	@Transactional
	public Integer addDraftCategory(CreationCategory category) {

		DraftCategory draftCategory = new DraftCategory(null, category.getIdCategory(), category.getName(),
				category.getDefinition(), category.getVersion(), category.getOwner(), null, category.getType(),
				category.getIdParent());

		DraftCategory result = this.draftCategoryRepository.save(draftCategory);
		if (result == null)
			return null;
		return result.getId();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<DraftCategory> getDraftCategoriesFromUser(Integer idUser, Pageable page) {
		return this.draftCategoryRepository.findAllByOwner(idUser, page);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftCategory getDraftCategory(Integer idDraft) {
		return this.draftCategoryRepository.findOne(idDraft);
	}

	@Override
	@Transactional
	public Integer addDraftTerm(CreationTerm term) {

		DraftTerm draftTerm = new DraftTerm();
		draftTerm.setIdTerm(term.getMainTerm().getIdTerm());
		draftTerm.setName(term.getMainTerm().getName());
		draftTerm.setShortName(term.getMainTerm().getShortName());
		draftTerm.setDefinition(term.getMainTerm().getDefinition());
		draftTerm.setExtendedDefinition(term.getMainTerm().getExtendedDefinition());
		draftTerm.setVersion(term.getMainTerm().getVersion());
		draftTerm.setOwner(term.getMainTerm().getOwner());
		draftTerm.setType(term.getMainTerm().getType());
		draftTerm.setScope(term.getIdScope());

		if (term.getCategories() != null && term.getCategories().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (Integer cat : term.getCategories()) {
				joiner.add(cat.toString());
			}
			draftTerm.setCategories(joiner.toString());
		}

		if (term.getDenominations() != null && term.getDenominations().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (Denomination denom : term.getDenominations()) {
				joiner.add(denom.getDenomination());
			}
			draftTerm.setDenominations(joiner.toString());
		}

		if (term.getExamples() != null && term.getExamples().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (String example : term.getExamples()) {
				joiner.add(example);
			}
			draftTerm.setExamples(joiner.toString());
		}

		if (term.getExclusions() != null && term.getExclusions().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (String exclusion : term.getExclusions()) {
				joiner.add(exclusion);
			}
			draftTerm.setExclusions(joiner.toString());
		}

		DraftTerm res = this.draftTermRepository.save(draftTerm);
		if (res == null)
			return null;
		Integer id = res.getId();

		Integer termType = term.getMainTerm().getType().getType();
		switch (termType) {
		case 2: // DIMENSIÓN
			DraftDimension draftDimension = new DraftDimension(id);
			if (term.getOrigin() != null) {
				draftDimension.setOrigin(term.getOrigin());
				draftDimension.setOriginCreation(term.getOriginCreation());
				draftDimension.setOriginUpdate(term.getOriginUpdate());
				draftDimension.setPeriod(term.getPeriod());
			}

			if (term.getApplicative() != null) {
				draftDimension.setApplicative(term.getApplicative());
				draftDimension.setApplicative_denomination(term.getApplicativeDenomination());
			}
			draftDimension.setHierarchy(term.getHierarchy());
			DraftDimension dimRes = this.draftDimensionRepository.save(draftDimension);
			if (dimRes != null)
				return dimRes.getId();
			break;
		case 3: // INDICADOR
			DraftIndicator draftIndicator = new DraftIndicator(id);
			if (term.getOrigin() != null) {
				draftIndicator.setOrigin(term.getOrigin());
				draftIndicator.setOriginCreation(term.getOriginCreation());
				draftIndicator.setOriginUpdate(term.getOriginUpdate());
				draftIndicator.setPeriod(term.getPeriod());
			}

			if (term.getApplicative() != null) {
				draftIndicator.setApplicative(term.getApplicative());
				draftIndicator.setApplicative_denomination(term.getApplicativeDenomination());
			}

			draftIndicator.setFunctionalInterpretation(term.getFunctionalInterpretation());
			draftIndicator.setFormula(term.getFormula());
			draftIndicator.setGranularity(term.getGranularity());

			if (term.getIndicatorsDimensions() != null && term.getIndicatorsDimensions().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (Integer dimension : term.getIndicatorsDimensions()) {
					joiner.add(dimension.toString());
				}
				draftIndicator.setDimensions(joiner.toString());
			}

			DraftIndicator indRes = this.draftIndicatorRepository.save(draftIndicator);
			if (indRes != null)
				return indRes.getId();
			break;
		case 4: // OBJETIVO
			DraftObjective draftObjective = new DraftObjective(id);
			if (term.getOrigin() != null) {
				draftObjective.setOrigin(term.getOrigin());
				draftObjective.setOriginCreation(term.getOriginCreation());
				draftObjective.setOriginUpdate(term.getOriginUpdate());
				draftObjective.setPeriod(term.getPeriod());
			}

			if (term.getApplicative() != null) {
				draftObjective.setApplicative(term.getApplicative());
				draftObjective.setApplicative_denomination(term.getApplicativeDenomination());
			}

			draftObjective.setFunctionalInterpretation(term.getFunctionalInterpretation());
			draftObjective.setFormula(term.getFormula());
			draftObjective.setGranularity(term.getGranularity());

			if (term.getObjectivesDimensions() != null && term.getObjectivesDimensions().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (SimpleObjectivesDimension dimension : term.getObjectivesDimensions()) {
					joiner.add(dimension.getDimension().toString());
				}
				draftObjective.setDimensions(joiner.toString());
			}

			if (term.getAsociatedIndicators() != null && term.getAsociatedIndicators().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (Integer indicator : term.getAsociatedIndicators()) {
					joiner.add(indicator.toString());
				}
				draftObjective.setIndicators(joiner.toString());
			}

			DraftObjective objRes = this.draftObjectiveRepository.save(draftObjective);
			if (objRes != null)
				return objRes.getId();
			break;
		case 5: // INFORME
			DraftInform draftInform = new DraftInform(id);
			draftInform.setLink(term.getLink());
			if (term.getInformIndicators() != null && term.getInformIndicators().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (Integer indicator : term.getInformIndicators()) {
					joiner.add(indicator.toString());
				}
				draftInform.setIndicators(joiner.toString());
			}

			DraftInform infRes = this.draftInformRepository.save(draftInform);
			if (infRes != null)
				return infRes.getId();
			break;
		case 1:
		case 6:
			return id;
		default:
			throw new InternalServerException();
		}

		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<DraftTerm> getDraftTermsFromUser(Integer idUser, Pageable page) {
		return this.draftTermRepository.findAllByOwner(idUser, page);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftTerm getDraftTerm(Integer idDraft) {
		return this.draftTermRepository.findOne(idDraft);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftDimension getDraftDimension(Integer idDraft) {
		return this.draftDimensionRepository.findOne(idDraft);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftIndicator getDraftIndicator(Integer idDraft) {
		return this.draftIndicatorRepository.findOne(idDraft);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftObjective getDraftObjective(Integer idDraft) {
		return this.draftObjectiveRepository.findOne(idDraft);
	}

	@Override
	@Transactional(readOnly = true)
	public DraftInform getDraftInform(Integer idDraft) {
		return this.draftInformRepository.findOne(idDraft);
	}

	@Override
	public Integer updateDraftScope(Integer idDraft, Scope scope) {

		DraftScope foundScope = this.draftScopeRepository.findOne(idDraft);
		if (foundScope == null)
			return null;
		DraftScope res = this.draftScopeRepository.save(new DraftScope(idDraft, scope.getIdScope(), scope.getName(),
				scope.getDefinition(), scope.getVersion(), scope.getOwner(), scope.getCreation()));
		if (res == null)
			return null;
		return res.getId();
	}

	@Override
	public Integer updateDraftCategory(Integer idDraft, CreationCategory category) {

		DraftCategory foundCategory = this.draftCategoryRepository.findOne(idDraft);
		if (foundCategory == null)
			return null;
		DraftCategory res = this.draftCategoryRepository.save(new DraftCategory(idDraft, category.getIdCategory(),
				category.getName(), category.getDefinition(), category.getVersion(), category.getOwner(),
				foundCategory.getCreation(), category.getType(), category.getIdParent()));
		if (res == null)
			return null;
		return res.getId();
	}

	@Override
	public Integer updateDraftTerm(Integer idDraft, CreationTerm term) {

		DraftTerm foundTerm = this.draftTermRepository.findOne(idDraft);
		if (foundTerm == null)
			return null;

		DraftTerm draftTerm = new DraftTerm(idDraft, term.getMainTerm().getIdTerm(), term.getMainTerm().getName(),
				term.getMainTerm().getShortName(), term.getMainTerm().getDefinition(),
				term.getMainTerm().getExtendedDefinition(), term.getMainTerm().getVersion(),
				term.getMainTerm().getOwner(), foundTerm.getCreation(), term.getMainTerm().getType(),
				term.getIdScope());

		if (term.getCategories() != null && term.getCategories().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (Integer cat : term.getCategories()) {
				joiner.add(cat.toString());
			}
			draftTerm.setCategories(joiner.toString());
		}

		if (term.getDenominations() != null && term.getDenominations().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (Denomination denom : term.getDenominations()) {
				joiner.add(denom.getDenomination());
			}
			draftTerm.setDenominations(joiner.toString());
		}

		if (term.getExamples() != null && term.getExamples().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (String example : term.getExamples()) {
				joiner.add(example);
			}
			draftTerm.setExamples(joiner.toString());
		}

		if (term.getExclusions() != null && term.getExclusions().size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (String exclusion : term.getExclusions()) {
				joiner.add(exclusion);
			}
			draftTerm.setExclusions(joiner.toString());
		}

		DraftTerm res = this.draftTermRepository.save(draftTerm);
		if (res == null)
			return null;

		Integer termType = term.getMainTerm().getType().getType();

		// Cambio de tipo
		if (!termType.equals(foundTerm.getType().getType())) {
			switch (foundTerm.getType().getType()) {
			case 2:
				this.draftDimensionRepository.delete(idDraft);
				break;
			case 3:
				this.draftIndicatorRepository.delete(idDraft);
				break;
			case 4:
				this.draftObjectiveRepository.delete(idDraft);
				break;
			case 5:
				this.draftInformRepository.delete(idDraft);
				break;
			}
		}

		switch (termType) {
		case 2: // DIMENSIÓN
			DraftDimension draftDimension = new DraftDimension(idDraft);
			if (term.getOrigin() != null) {
				draftDimension.setOrigin(term.getOrigin());
				draftDimension.setOriginCreation(term.getOriginCreation());
				draftDimension.setOriginUpdate(term.getOriginUpdate());
				draftDimension.setPeriod(term.getPeriod());
			}

			if (term.getApplicative() != null) {
				draftDimension.setApplicative(term.getApplicative());
				draftDimension.setApplicative_denomination(term.getApplicativeDenomination());
			}
			draftDimension.setHierarchy(term.getHierarchy());
			DraftDimension dimRes = this.draftDimensionRepository.save(draftDimension);
			if (dimRes != null)
				return dimRes.getId();
			break;

		case 3: // INDICADOR
			DraftIndicator draftIndicator = new DraftIndicator(idDraft);
			if (term.getOrigin() != null) {
				draftIndicator.setOrigin(term.getOrigin());
				draftIndicator.setOriginCreation(term.getOriginCreation());
				draftIndicator.setOriginUpdate(term.getOriginUpdate());
				draftIndicator.setPeriod(term.getPeriod());
			}

			if (term.getApplicative() != null) {
				draftIndicator.setApplicative(term.getApplicative());
				draftIndicator.setApplicative_denomination(term.getApplicativeDenomination());
			}

			draftIndicator.setFunctionalInterpretation(term.getFunctionalInterpretation());
			draftIndicator.setFormula(term.getFormula());
			draftIndicator.setGranularity(term.getGranularity());

			if (term.getIndicatorsDimensions() != null && term.getIndicatorsDimensions().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (Integer dimension : term.getIndicatorsDimensions()) {
					joiner.add(dimension.toString());
				}
				draftIndicator.setDimensions(joiner.toString());
			}

			DraftIndicator indRes = this.draftIndicatorRepository.save(draftIndicator);
			if (indRes != null)
				return indRes.getId();
			break;

		case 4: // OBJETIVO
			DraftObjective draftObjective = new DraftObjective(idDraft);
			if (term.getOrigin() != null) {
				draftObjective.setOrigin(term.getOrigin());
				draftObjective.setOriginCreation(term.getOriginCreation());
				draftObjective.setOriginUpdate(term.getOriginUpdate());
				draftObjective.setPeriod(term.getPeriod());
			}

			if (term.getApplicative() != null) {
				draftObjective.setApplicative(term.getApplicative());
				draftObjective.setApplicative_denomination(term.getApplicativeDenomination());
			}

			draftObjective.setFunctionalInterpretation(term.getFunctionalInterpretation());
			draftObjective.setFormula(term.getFormula());
			draftObjective.setGranularity(term.getGranularity());

			if (term.getObjectivesDimensions() != null && term.getObjectivesDimensions().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (SimpleObjectivesDimension dimension : term.getObjectivesDimensions()) {
					joiner.add(dimension.getDimension().toString());
				}
				draftObjective.setDimensions(joiner.toString());
			}

			if (term.getAsociatedIndicators() != null && term.getAsociatedIndicators().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (Integer indicator : term.getAsociatedIndicators()) {
					joiner.add(indicator.toString());
				}
				draftObjective.setIndicators(joiner.toString());
			}

			DraftObjective objRes = this.draftObjectiveRepository.save(draftObjective);
			if (objRes != null)
				return objRes.getId();
			break;

		case 5: // INFORME
			DraftInform draftInform = new DraftInform(idDraft);
			draftInform.setLink(term.getLink());
			if (term.getInformIndicators() != null && term.getInformIndicators().size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (Integer indicator : term.getInformIndicators()) {
					joiner.add(indicator.toString());
				}
				draftInform.setIndicators(joiner.toString());
			}

			DraftInform infRes = this.draftInformRepository.save(draftInform);
			if (infRes != null)
				return infRes.getId();
			break;
		case 1:
		case 6:
			return idDraft;
		default:
			throw new InternalServerException();
		}

		return null;
	}

	@Override
	public void removeDraftScope(Integer idDraft, Integer idUser) {
		DraftScope res = this.draftScopeRepository.findOne(idDraft);
		if (res != null && res.getOwner().equals(idUser)) {
			this.draftScopeRepository.delete(idDraft);
		}
	}

	@Override
	public void removeDraftCategory(Integer idDraft, Integer idUser) {
		DraftCategory res = this.draftCategoryRepository.findOne(idDraft);
		if (res != null && res.getOwner().equals(idUser)) {
			this.draftCategoryRepository.delete(idDraft);
		}
	}

	@Override
	public void removeDraftTerm(Integer idDraft, Integer idUser) {
		DraftTerm res = this.draftTermRepository.findOne(idDraft);
		if (res != null && res.getOwner().equals(idUser)) {
			this.draftTermRepository.delete(idDraft);
		}
	}
}
