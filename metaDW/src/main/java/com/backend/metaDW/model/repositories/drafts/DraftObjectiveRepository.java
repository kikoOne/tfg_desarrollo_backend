package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftObjective;

public interface DraftObjectiveRepository extends PagingAndSortingRepository<DraftObjective, Integer>{

}
