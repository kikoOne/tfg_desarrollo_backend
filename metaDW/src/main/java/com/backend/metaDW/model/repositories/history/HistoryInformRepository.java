package com.backend.metaDW.model.repositories.history;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryInform;

public interface HistoryInformRepository extends PagingAndSortingRepository<HistoryInform, Integer>{

}
