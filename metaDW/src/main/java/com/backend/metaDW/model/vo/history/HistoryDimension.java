package com.backend.metaDW.model.vo.history;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "historydimensions", schema = "public")
public class HistoryDimension {

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "origin")
	private Integer origin;

	@Column(name = "origin_creation")
	private Timestamp originCreation;

	@Column(name = "origin_update")
	private Timestamp originUpdate;

	@Column(name = "period")
	private int period;

	@Column(name = "applicative")
	private Integer applicative;

	@Column(name = "applicative_denomination")
	private String applicative_denomination;

	@Column(name = "hierarchy")
	private String hierarchy;

	/**
	 * void constructor
	 */
	public HistoryDimension() {
	}

	/**
	 * @param id
	 */
	public HistoryDimension(Integer id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @param origin
	 * @param originCreation
	 * @param originUpdate
	 * @param period
	 * @param applicative
	 * @param applicative_denomination
	 * @param hierarchy
	 */
	public HistoryDimension(Integer id, Integer origin, Timestamp originCreation, Timestamp originUpdate, int period,
			Integer applicative, String applicative_denomination, String hierarchy) {
		this.id = id;
		this.origin = origin;
		this.originCreation = originCreation;
		this.originUpdate = originUpdate;
		this.period = period;
		this.applicative = applicative;
		this.applicative_denomination = applicative_denomination;
		this.hierarchy = hierarchy;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrigin() {
		return origin;
	}

	public void setOrigin(Integer origin) {
		this.origin = origin;
	}

	public Timestamp getOriginCreation() {
		return originCreation;
	}

	public void setOriginCreation(Timestamp originCreation) {
		this.originCreation = originCreation;
	}

	public Timestamp getOriginUpdate() {
		return originUpdate;
	}

	public void setOriginUpdate(Timestamp originUpdate) {
		this.originUpdate = originUpdate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public Integer getApplicative() {
		return applicative;
	}

	public void setApplicative(Integer applicative) {
		this.applicative = applicative;
	}

	public String getApplicative_denomination() {
		return applicative_denomination;
	}

	public void setApplicative_denomination(String applicative_denomination) {
		this.applicative_denomination = applicative_denomination;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicative == null) ? 0 : applicative.hashCode());
		result = prime * result + ((applicative_denomination == null) ? 0 : applicative_denomination.hashCode());
		result = prime * result + ((hierarchy == null) ? 0 : hierarchy.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((originCreation == null) ? 0 : originCreation.hashCode());
		result = prime * result + ((originUpdate == null) ? 0 : originUpdate.hashCode());
		result = prime * result + period;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryDimension other = (HistoryDimension) obj;
		if (applicative == null) {
			if (other.applicative != null)
				return false;
		} else if (!applicative.equals(other.applicative))
			return false;
		if (applicative_denomination == null) {
			if (other.applicative_denomination != null)
				return false;
		} else if (!applicative_denomination.equals(other.applicative_denomination))
			return false;
		if (hierarchy == null) {
			if (other.hierarchy != null)
				return false;
		} else if (!hierarchy.equals(other.hierarchy))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (originCreation == null) {
			if (other.originCreation != null)
				return false;
		} else if (!originCreation.equals(other.originCreation))
			return false;
		if (originUpdate == null) {
			if (other.originUpdate != null)
				return false;
		} else if (!originUpdate.equals(other.originUpdate))
			return false;
		if (period != other.period)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Dimension [idTerm=" + id + ", origin=" + origin + ", originCreation=" + originCreation
				+ ", originUpdate=" + originUpdate + ", period=" + period + ", applicative=" + applicative
				+ ", applicative_denomination=" + applicative_denomination + ", hierarchy=" + hierarchy + "]";
	}
}
