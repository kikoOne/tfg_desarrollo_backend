package com.backend.metaDW.model.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.IIncidenceDAO;
import com.backend.metaDW.model.repositories.terms.FullIncidenceRepository;
import com.backend.metaDW.model.repositories.terms.IncidenceRepository;
import com.backend.metaDW.model.vo.term.FullIncidence;
import com.backend.metaDW.model.vo.term.Incidence;

@Service
public class IncidenceDAO implements IIncidenceDAO {

	@Autowired
	private IncidenceRepository incidenceRepository;
	@Autowired
	private FullIncidenceRepository fullIncidenceRepository;

	@Override
	@Transactional
	public Integer saveIncidence(Incidence incidence) {
		Incidence result = this.incidenceRepository.save(incidence);
		if (result != null)
			return result.getIdIncidence();
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Incidence> getIncidences(Pageable page) {
		return this.incidenceRepository.findAll(page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Incidence> getAllIncidences() {
		return (List<Incidence>) this.incidenceRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Incidence> getIncidencesFromTerm(Integer idTerm) {
		return this.incidenceRepository.findAllByIdTerm(idTerm);
	}

	@Override
	@Transactional
	public Integer updateIncidence(Incidence newIncidence) {
		System.out.println(newIncidence.toString()); 
		Incidence incidence = this.incidenceRepository.findOne(newIncidence.getIdIncidence());
		System.out.println(incidence.toString());
		if (incidence == null || !incidence.getIdIncidence().equals(newIncidence.getIdIncidence()))
			return null;
		Incidence result = this.incidenceRepository.save(newIncidence);
		if (result != null)
			return result.getIdIncidence();
		return null;
	}

	@Override
	@Transactional
	public void removeIncidenceFromTerm(Integer idIncidence, Integer idTerm) {
		Incidence incidence = this.incidenceRepository.findOneByIdIncidenceAndIdTerm(idIncidence, idTerm);
		if (incidence != null)
			this.incidenceRepository.delete(incidence);
	}

	@Override
	public Page<FullIncidence> getFullIncidences(Pageable page) {
		return this.fullIncidenceRepository.findAll(page);
	}
}
