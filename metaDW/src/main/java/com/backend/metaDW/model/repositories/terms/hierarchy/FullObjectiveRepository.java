package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.FullObjective;

public interface FullObjectiveRepository extends PagingAndSortingRepository<FullObjective, Integer> {

	public FullObjective findOneByIdTerm(Integer idTerm);
}
