package com.backend.metaDW.model.repositories.terms;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.backend.metaDW.model.vo.term.TermExample;

public interface TermExampleRepository extends PagingAndSortingRepository<TermExample, Integer>{
	
	public Page<?> findAllByIdTerm(Integer id_term, Pageable page);
	
	public List<TermExample> findAllByIdTerm(Integer id_term);
}
