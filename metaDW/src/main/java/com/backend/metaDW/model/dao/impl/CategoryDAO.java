package com.backend.metaDW.model.dao.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.model.dao.ICategoryDAO;
import com.backend.metaDW.model.dao.IHistoryDAO;
import com.backend.metaDW.model.repositories.categories.CategoryRepository;
import com.backend.metaDW.model.repositories.categories.FullCategoryRepository;
import com.backend.metaDW.model.repositories.categories.FullSubcategoryRepository;
import com.backend.metaDW.model.repositories.categories.SimpleSubcategoryRepository;
import com.backend.metaDW.model.repositories.categories.SubcategoryRepository;
import com.backend.metaDW.model.repositories.categories.TermsByCategoryRepository;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.category.FullCategory;
import com.backend.metaDW.model.vo.category.SimpleSubcategory;

@Service
public class CategoryDAO implements ICategoryDAO {

	private CategoryRepository categoryRepository;
	private FullCategoryRepository fullCategoryRepository;
	private SimpleSubcategoryRepository simpleSubcategoryRepository;
	private SubcategoryRepository subcategoryRepository;
	private TermsByCategoryRepository termsByCategoryRepository;
	private FullSubcategoryRepository fullSubcategoryRepository;
	private IHistoryDAO historyDAO;

	/**
	 * 
	 * @param categoryRepository
	 * @param fullCategoryRepository
	 * @param simpleSubcategoryRepository
	 * @param subcategoryRepository
	 * @param termsByCategoryRepository
	 * @param fullSubcategoryRepository
	 * @param historyDAO
	 */
	@Autowired
	public CategoryDAO(CategoryRepository categoryRepository, FullCategoryRepository fullCategoryRepository,
			SimpleSubcategoryRepository simpleSubcategoryRepository, SubcategoryRepository subcategoryRepository,
			TermsByCategoryRepository termsByCategoryRepository, FullSubcategoryRepository fullSubcategoryRepository,
			IHistoryDAO historyDAO) {
		this.categoryRepository = categoryRepository;
		this.fullCategoryRepository = fullCategoryRepository;
		this.simpleSubcategoryRepository = simpleSubcategoryRepository;
		this.subcategoryRepository = subcategoryRepository;
		this.termsByCategoryRepository = termsByCategoryRepository;
		this.fullSubcategoryRepository = fullSubcategoryRepository;
		this.historyDAO = historyDAO;
	}

	@Override
	@Transactional
	public Integer createCategoryTransaction(CreationCategory category) {

		Category savedCategory = this.categoryRepository
				.save(new Category(null, category.getName(), category.getDefinition(), category.getState(),
						category.getVersion(), category.getOwner(), null, category.getType()));
		Integer idCategory = savedCategory.getIdCategory();

		if (category.getIdParent() != null) {
			this.simpleSubcategoryRepository.save(new SimpleSubcategory(category.getIdParent(), idCategory));

			Category parent = this.categoryRepository.findOne(category.getIdParent());
			if (parent != null && parent.getType() == 0) { // Si no contiene
															// categorías
				parent.setType(1); // Cambio a tipo 1
				Category updatedParent = this.categoryRepository.save(parent);
				if (updatedParent == null)
					return null;
			}
		}

		return idCategory;
	}

	@Override
	@Transactional
	public void createCategoryTransaction(List<CreationCategory> categories) {
		for (CreationCategory category : categories) {
			this.createCategoryTransaction(category);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public FullCategory findCategoryById(Integer idCategory) {
		return this.fullCategoryRepository.findOne(idCategory);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullCategory> findCategoriesByType(Integer type, Pageable page) {
		return this.fullCategoryRepository.findByType(type, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findSubcategoriesFromCategory(Integer idCategory, Pageable page) {
		return this.subcategoryRepository.findSubcategoriesFromParent(idCategory, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findTermsFromCategory(Integer idCategory, Pageable page) {
		return this.termsByCategoryRepository.findTermsFromCategory(idCategory, page);
	}

	@Override
	@Transactional
	public boolean updateCategory(CreationCategory newCategory) {

		Category category = this.categoryRepository.findOne(newCategory.getIdCategory());
		if (category == null)
			return false;

		Integer idVersion = this.historyDAO.addCategoryHistoryVersion(category);
		if (idVersion == null)
			throw new InternalServerException();

		if (category.getVersion().equals(newCategory.getVersion())) {
			String version = newCategory.getVersion();
			String[] temp = (version.split("."));
			Integer next = Integer.parseInt(temp[1]) + 1;
			newCategory.setVersion(temp[0] + "." + next.toString());
		}

		category.setName(newCategory.getName());
		category.setDefinition(newCategory.getDefinition());
		category.setOwner(newCategory.getOwner());
		category.setState(newCategory.getState());
		category.setLastUpdate(new Timestamp((new Date()).getTime()));

		// Recuperar la categoría padre guardada
		List<?> oldParentList = this.fullSubcategoryRepository.findParentCategory(category.getIdCategory());

		if (newCategory.getIdParent() == null) { // Si nueva categoría sin padre
			category.setType(-1); // Actualizar estado
			System.out.println("Category parent null");
			if (oldParentList != null && oldParentList.size() > 0) { // Si tenia categoria padre
				Category oldParent = (Category) oldParentList.get(0);
				// Eliminar relación con categoría padre
				this.simpleSubcategoryRepository
						.delete(new SimpleSubcategory(oldParent.getIdCategory(), category.getIdCategory()));
				// Recuperar hijos de la categoría padre
				List<?> childs = this.subcategoryRepository.findAllSubcategoriesFromParent(oldParent.getIdCategory());
				if (childs.size() == 0) { // Si vacío se actualiza el tipo
					oldParent.setType(0);
					this.categoryRepository.save(oldParent);
				}
			}

		} else {
			// Inserta o actualiza relación con categoría padre
			System.out.println(newCategory.getIdParent());
			this.simpleSubcategoryRepository
					.save(new SimpleSubcategory(newCategory.getIdParent(), category.getIdCategory()));

			Category parent = this.categoryRepository.findOne(newCategory.getIdParent());
			if (parent != null && parent.getType() == 0) {
				parent.setType(1);
				Category updatedParent = this.categoryRepository.save(parent);
				if (updatedParent == null)
					throw new InternalServerException();
			}
		}
		System.out.println("next");

		Category updatedCategory = this.categoryRepository.save(category);
		if (updatedCategory == null)
			throw new InternalServerException();
		return true;
	}

	@Override
	@Transactional
	public void removeCategory(Integer idCategory) {
		this.categoryRepository.delete(idCategory);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Category> findAllCategories() {
		return (List<Category>) this.categoryRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Category findParentCategory(Integer idChild) {
		if (this.fullSubcategoryRepository.findParentCategory(idChild) != null
				&& this.fullSubcategoryRepository.findParentCategory(idChild).size() > 0)
			return (Category) this.fullSubcategoryRepository.findParentCategory(idChild).get(0);
		return null;
	}

}
