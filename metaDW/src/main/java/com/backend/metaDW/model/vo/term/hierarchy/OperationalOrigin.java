package com.backend.metaDW.model.vo.term.hierarchy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "operationalorigins", schema = "public")
public class OperationalOrigin {

	@Id
	@SequenceGenerator(name = "operationalorigins_id_origin_seq", sequenceName = "operationalorigins_id_origin_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operationalorigins_id_origin_seq")
	@Column(name = "id_origin", updatable = false)
	private Integer idOrigin;
	
	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	/**
	 * void constructor
	 */
	public OperationalOrigin() {
	}

	/**
	 * @param idOrigin
	 */
	public OperationalOrigin(Integer idOrigin) {
		this.idOrigin = idOrigin;
	}

	/**
	 * @param idOrigin
	 * @param description
	 */
	public OperationalOrigin(Integer idOrigin, String name, String description) {
		this.idOrigin = idOrigin;
		this.name = name;
		this.description = description;
	}

	public Integer getIdOrigin() {
		return idOrigin;
	}

	public void setIdOrigin(Integer idOrigin) {
		this.idOrigin = idOrigin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((idOrigin == null) ? 0 : idOrigin.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationalOrigin other = (OperationalOrigin) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idOrigin == null) {
			if (other.idOrigin != null)
				return false;
		} else if (!idOrigin.equals(other.idOrigin))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OperationalOrigin [idOrigin=" + idOrigin + ", name=" + name + ", description=" + description + "]";
	}
}
