package com.backend.metaDW.model.vo.user;

import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.*;

import com.backend.metaDW.model.vo.user.security.Role;

@Entity
@Table(name = "users", schema = "public")
public class User {
	@Id
	@SequenceGenerator(name = "users_id_user_seq", sequenceName = "users_id_user_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_user_seq")
	@Column(name = "id_user", updatable = false)
	private Integer idUser;

	@Basic(optional = false)
	@Column(name = "email")
	private String email;

	@Basic(optional = false)
	@Column(name = "user_name")
	private String userName;

	@Basic(optional = false)
	@Column(name = "name")
	private String name;

	@Basic(optional = false)
	@Column(name = "surnames")
	private String surnames;

	@Column(name = "state")
	private int state = 1;

	@Column(name = "register", insertable = false)
	private Timestamp register;

	@Column(name = "password")
	private String password;
	
	@ManyToMany
	@JoinTable(name = "userroles", joinColumns = @JoinColumn(name = "id_user", referencedColumnName = "id_user"), inverseJoinColumns = @JoinColumn(name = "id_role", referencedColumnName = "id_role"))
	private Collection<Role> roles;

	public User() {
	}

	public User(Integer idUser) {
		this.idUser = idUser;
	}

	/**
	 * @param idUser
	 * @param email
	 * @param userName
	 * @param name
	 * @param surnames
	 * @param state
	 * @param register
	 * @param password
	 * @param roles
	 */
	public User(Integer idUser, String email, String userName, String name, String surnames, int state,
			Timestamp register, String password, Collection<Role> roles) {
		this.idUser = idUser;
		this.email = email;
		this.userName = userName;
		this.name = name;
		this.surnames = surnames;
		this.state = state;
		this.register = register;
		this.password = password;
		this.roles = roles;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnames() {
		return surnames;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Timestamp getRegister() {
		return register;
	}

	public void setRegister(Timestamp register) {
		this.register = register;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
}
