package com.backend.metaDW.model.repositories.user.security;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.metaDW.model.vo.user.security.FullInternalUser;

public interface FullInternalUserRepository extends JpaRepository<FullInternalUser, Long> {

	public FullInternalUser findByIdUser(Integer idUser);

	public FullInternalUser findByUserNameAndPassword(String userName, String password);
}
