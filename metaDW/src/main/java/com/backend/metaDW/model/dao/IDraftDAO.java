package com.backend.metaDW.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.drafts.DraftCategory;
import com.backend.metaDW.model.vo.drafts.DraftDimension;
import com.backend.metaDW.model.vo.drafts.DraftIndicator;
import com.backend.metaDW.model.vo.drafts.DraftInform;
import com.backend.metaDW.model.vo.drafts.DraftObjective;
import com.backend.metaDW.model.vo.drafts.DraftScope;
import com.backend.metaDW.model.vo.drafts.DraftTerm;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.CreationTerm;

public interface IDraftDAO {

	public Integer addDraftScope(Scope scope);

	public Page<DraftScope> getDraftScopesFromUser(Integer idUser, Pageable page);

	public DraftScope getDraftScope(Integer idDraft);

	public Integer addDraftCategory(CreationCategory category);

	public Page<DraftCategory> getDraftCategoriesFromUser(Integer idUser, Pageable page);

	public DraftCategory getDraftCategory(Integer idDraft);

	public Integer addDraftTerm(CreationTerm term);

	public Page<DraftTerm> getDraftTermsFromUser(Integer idUser, Pageable page);

	public DraftTerm getDraftTerm(Integer idDraft);

	public DraftDimension getDraftDimension(Integer idDraft);

	public DraftIndicator getDraftIndicator(Integer idDraft);

	public DraftObjective getDraftObjective(Integer idDraft);

	public DraftInform getDraftInform(Integer idDraft);

	public Integer updateDraftScope(Integer idDraft, Scope scope);

	public Integer updateDraftCategory(Integer idDraft, CreationCategory category);

	public Integer updateDraftTerm(Integer idDraft, CreationTerm term);

	public void removeDraftScope(Integer idDraft, Integer idUser);

	public void removeDraftCategory(Integer idDraft, Integer idUser);

	public void removeDraftTerm(Integer idDraft, Integer idUser);
}
