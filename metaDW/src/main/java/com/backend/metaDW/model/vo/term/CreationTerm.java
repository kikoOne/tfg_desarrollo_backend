package com.backend.metaDW.model.vo.term;

import java.sql.Timestamp;
import java.util.List;

import org.jvnet.hk2.annotations.Optional;

import com.backend.metaDW.model.vo.term.hierarchy.SimpleObjectivesDimension;

public class CreationTerm {

	// Características comunes a todos los tipos de términos
	private Term mainTerm;

	@Optional
	//Relaciones con categorías y ámbitos
	private Integer idScope;
	private List<Integer> categories;
	
	// Características adicionales
	private List<Denomination> denominations;
	private List<String> examples;
	private List<String> exclusions;

	// Características comunes de los de tipo Dimensión, Indicador y Objectivo
	private Integer origin;
	private Timestamp originCreation;
	private Timestamp originUpdate;
	private int period;
	private Integer applicative;
	private String applicativeDenomination;

	// Dimensiones
	private String hierarchy;

	// Indicadores y objetivos
	private String functionalInterpretation;
	private String formula;
	private String granularity;
	// Indicadores
	private List<Integer> indicatorsDimensions;
	// Objetivos
	private List<SimpleObjectivesDimension> objectivesDimensions;
	private List<Integer> asociatedIndicators;

	// Informes
	private String link;
	private List<Integer> informIndicators;

	public CreationTerm() {

	}

	/**
	 * @param mainTerm
	 */
	public CreationTerm(Term mainTerm) {
		this.mainTerm = mainTerm;
	}

	

	/**
	 * @param mainTerm
	 * @param idScope
	 * @param categories
	 * @param denominations
	 * @param examples
	 * @param exclusions
	 * @param origin
	 * @param originCreation
	 * @param originUpdate
	 * @param period
	 * @param applicative
	 * @param applicativeDenomination
	 * @param hierarchy
	 * @param functionalInterpretation
	 * @param formula
	 * @param granularity
	 * @param indicatorsDimensions
	 * @param objectivesDimensions
	 * @param asociatedIndicators
	 * @param link
	 * @param informIndicators
	 */
	public CreationTerm(Term mainTerm, Integer idScope, List<Integer> categories, List<Denomination> denominations,
			List<String> examples, List<String> exclusions, Integer origin, Timestamp originCreation,
			Timestamp originUpdate, int period, Integer applicative, String applicativeDenomination, String hierarchy,
			String functionalInterpretation, String formula, String granularity, List<Integer> indicatorsDimensions,
			List<SimpleObjectivesDimension> objectivesDimensions, List<Integer> asociatedIndicators, String link,
			List<Integer> informIndicators) {
		this.mainTerm = mainTerm;
		this.idScope = idScope;
		this.categories = categories;
		this.denominations = denominations;
		this.examples = examples;
		this.exclusions = exclusions;
		this.origin = origin;
		this.originCreation = originCreation;
		this.originUpdate = originUpdate;
		this.period = period;
		this.applicative = applicative;
		this.applicativeDenomination = applicativeDenomination;
		this.hierarchy = hierarchy;
		this.functionalInterpretation = functionalInterpretation;
		this.formula = formula;
		this.granularity = granularity;
		this.indicatorsDimensions = indicatorsDimensions;
		this.objectivesDimensions = objectivesDimensions;
		this.asociatedIndicators = asociatedIndicators;
		this.link = link;
		this.informIndicators = informIndicators;
	}

	public Term getMainTerm() {
		return mainTerm;
	}

	public void setMainTerm(Term mainTerm) {
		this.mainTerm = mainTerm;
	}

	public Integer getIdScope() {
		return idScope;
	}

	public void setIdScope(Integer idScope) {
		this.idScope = idScope;
	}

	public List<Integer> getCategories() {
		return categories;
	}

	public void setCategories(List<Integer> categories) {
		this.categories = categories;
	}

	public List<Denomination> getDenominations() {
		return denominations;
	}

	public void setDenominations(List<Denomination> denominations) {
		this.denominations = denominations;
	}

	public List<String> getExamples() {
		return examples;
	}

	public void setExamples(List<String> examples) {
		this.examples = examples;
	}

	public List<String> getExclusions() {
		return exclusions;
	}

	public void setExclusions(List<String> exclusions) {
		this.exclusions = exclusions;
	}

	public Integer getOrigin() {
		return origin;
	}

	public void setOrigin(Integer origin) {
		this.origin = origin;
	}

	public Timestamp getOriginCreation() {
		return originCreation;
	}

	public void setOriginCreation(Timestamp originCreation) {
		this.originCreation = originCreation;
	}

	public Timestamp getOriginUpdate() {
		return originUpdate;
	}

	public void setOriginUpdate(Timestamp originUpdate) {
		this.originUpdate = originUpdate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public Integer getApplicative() {
		return applicative;
	}

	public void setApplicative(Integer applicative) {
		this.applicative = applicative;
	}

	public String getApplicativeDenomination() {
		return applicativeDenomination;
	}

	public void setApplicativeDenomination(String applicativeDenomination) {
		this.applicativeDenomination = applicativeDenomination;
	}

	public String getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(String hierarchy) {
		this.hierarchy = hierarchy;
	}

	public String getFunctionalInterpretation() {
		return functionalInterpretation;
	}

	public void setFunctionalInterpretation(String functionalInterpretation) {
		this.functionalInterpretation = functionalInterpretation;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getGranularity() {
		return granularity;
	}

	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}

	public List<Integer> getIndicatorsDimensions() {
		return indicatorsDimensions;
	}

	public void setIndicatorsDimensions(List<Integer> indicatorsDimensions) {
		this.indicatorsDimensions = indicatorsDimensions;
	}

	public List<SimpleObjectivesDimension> getObjectivesDimensions() {
		return objectivesDimensions;
	}

	public void setObjectivesDimensions(List<SimpleObjectivesDimension> objectivesDimensions) {
		this.objectivesDimensions = objectivesDimensions;
	}

	public List<Integer> getAsociatedIndicators() {
		return asociatedIndicators;
	}

	public void setAsociatedIndicators(List<Integer> asociatedIndicators) {
		this.asociatedIndicators = asociatedIndicators;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<Integer> getInformIndicators() {
		return informIndicators;
	}

	public void setInformIndicators(List<Integer> informIndicators) {
		this.informIndicators = informIndicators;
	}

	@Override
	public String toString() {
		return "CreationTerm [mainTerm=" + mainTerm + ", idScope=" + idScope + ", categories=" + categories
				+ ", denominations=" + denominations + ", examples=" + examples + ", exclusions=" + exclusions
				+ ", origin=" + origin + ", originCreation=" + originCreation + ", originUpdate=" + originUpdate
				+ ", period=" + period + ", applicative=" + applicative + ", applicativeDenomination="
				+ applicativeDenomination + ", hierarchy=" + hierarchy + ", functionalInterpretation="
				+ functionalInterpretation + ", formula=" + formula + ", granularity=" + granularity
				+ ", indicatorsDimensions=" + indicatorsDimensions + ", objectivesDimensions=" + objectivesDimensions
				+ ", asociatedIndicators=" + asociatedIndicators + ", link=" + link + ", informIndicators="
				+ informIndicators + "]";
	}
}
