package com.backend.metaDW.model.vo.scope;

import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "scopes", schema = "public")
public class Scope {

	@Id
	@SequenceGenerator(name = "scopes_id_scope_seq", sequenceName = "scopes_id_scope_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scopes_id_scope_seq")
	@Column(name = "id_scope", updatable = false)
	private Integer idScope;

	@Column(name = "name")
	private String name;

	@Column(name = "definition")
	private String definition;

	@Column(name = "state")
	private int state;

	@Column(name = "version")
	private String version;

	@Column(name = "owner")
	private Integer owner;

	@Column(name = "creation", insertable = false)
	private Timestamp creation;

	@Column(name = "last_update", insertable = false)
	private Timestamp lastUpdate;

	public Scope() {
	}

	public Scope(Integer idScope) {
		this.idScope = idScope;
	}

	public Scope(Integer idScope, String name, String definition, int state, String version, Integer owner,
			Timestamp creation) {
		this.idScope = idScope;
		this.name = name;
		this.definition = definition;
		this.state = state;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
	}

	public Integer getIdScope() {
		return idScope;
	}

	public void setIdScope(Integer idScope) {
		this.idScope = idScope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((idScope == null) ? 0 : idScope.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + state;
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Scope other = (Scope) obj;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (idScope == null) {
			if (other.idScope != null)
				return false;
		} else if (!idScope.equals(other.idScope))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (state != other.state)
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Scope [idScope=" + idScope + ", name=" + name + ", definition=" + definition + ", state=" + state
				+ ", version=" + version + ", owner=" + owner + ", creation=" + creation + "]";
	}
}
