package com.backend.metaDW.model.repositories.categories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.category.SimpleTermsByCategory;

public interface SimpleTermsByCategoryRepository extends PagingAndSortingRepository<SimpleTermsByCategory, Integer>{

}
