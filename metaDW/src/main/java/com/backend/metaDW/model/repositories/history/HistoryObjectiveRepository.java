package com.backend.metaDW.model.repositories.history;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryObjective;

public interface HistoryObjectiveRepository extends PagingAndSortingRepository<HistoryObjective, Integer>{

}
