package com.backend.metaDW.model.repositories.history;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryCategory;

public interface HistoryCategoryRepository extends PagingAndSortingRepository<HistoryCategory, Integer>{

	public Page<HistoryCategory> findAllByIdCategory(Integer idCategory, Pageable page);
	
	public HistoryCategory findByIdCategoryAndId(Integer idCategory, Integer idVersion);
}
