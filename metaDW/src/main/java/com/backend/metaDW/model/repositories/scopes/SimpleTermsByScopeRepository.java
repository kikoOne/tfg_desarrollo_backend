package com.backend.metaDW.model.repositories.scopes;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.scope.SimpleTermsByScope;

public interface SimpleTermsByScopeRepository extends PagingAndSortingRepository<SimpleTermsByScope, Integer> {

}
