package com.backend.metaDW.model.vo.scope;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(SimpleTermsByCategoryPK.class)
@Table(name = "termsbyscope", schema = "public")
public class SimpleTermsByScope {

	@Id
	@Column(name = "id_scope")
	private Integer idScope;
	
	@Id
	@Column(name = "id_term")
	private Integer idTerm;

	/**
	 * 
	 */
	public SimpleTermsByScope() {
	}

	/**
	 * @param idScope
	 * @param idTerm
	 */
	public SimpleTermsByScope(Integer idScope, Integer idTerm) {
		this.idScope = idScope;
		this.idTerm = idTerm;
	}

	public Integer getIdScope() {
		return idScope;
	}

	public void setIdScope(Integer idScope) {
		this.idScope = idScope;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idScope == null) ? 0 : idScope.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleTermsByScope other = (SimpleTermsByScope) obj;
		if (idScope == null) {
			if (other.idScope != null)
				return false;
		} else if (!idScope.equals(other.idScope))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleTermsByScope [idScope=" + idScope + ", idTerm=" + idTerm + "]";
	}
}

@SuppressWarnings("serial")
class SimpleTermsByCategoryPK implements Serializable {

	private Integer idScope;
	private Integer idTerm;

	public Integer getIdScope() {
		return idScope;
	}

	public void setIdScope(Integer idScope) {
		this.idScope = idScope;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}
}
