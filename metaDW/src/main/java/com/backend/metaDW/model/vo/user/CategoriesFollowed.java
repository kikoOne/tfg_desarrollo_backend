package com.backend.metaDW.model.vo.user;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.backend.metaDW.model.vo.category.Category;

@Entity
@IdClass(CategoriesFollowedPK.class)
@Table(name = "categoriesfollowed", schema = "public")
public class CategoriesFollowed {

	@Id
	@Column(name = "id_user")
	private Integer idUser;

	@Id
	@ManyToOne
	@JoinColumn(name = "id_category")
	private Category category;

	/**
	 * 
	 */
	public CategoriesFollowed() {
	}

	/**
	 * @param idUser
	 * @param category
	 */
	public CategoriesFollowed(Integer idUser, Category category) {
		this.idUser = idUser;
		this.category = category;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategoriesFollowed other = (CategoriesFollowed) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermsFollowed [idUser=" + idUser + ", idTerm=" + category + "]";
	}

}

@SuppressWarnings("serial")
class CategoriesFollowedPK implements Serializable {

	private Integer idUser;
	private String category;

	/**
	 * @param idUser
	 * @param category
	 */
	public CategoriesFollowedPK(Integer idUser, String category) {
		this.idUser = idUser;
		this.category = category;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
