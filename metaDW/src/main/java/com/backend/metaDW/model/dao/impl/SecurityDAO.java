package com.backend.metaDW.model.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.ISecurityDAO;
import com.backend.metaDW.model.repositories.user.UserRepository;
import com.backend.metaDW.model.repositories.user.security.FullInternalUserRepository;
import com.backend.metaDW.model.repositories.user.security.RoleRepository;
import com.backend.metaDW.model.repositories.user.security.UserTokenRepository;
import com.backend.metaDW.model.vo.user.User;
import com.backend.metaDW.model.vo.user.security.FullInternalUser;
import com.backend.metaDW.model.vo.user.security.Role;
import com.backend.metaDW.model.vo.user.security.UserToken;
import com.backend.metaDW.security.jwt.JWT;

@Service
public class SecurityDAO implements ISecurityDAO {

	private UserRepository userRepository;
	private FullInternalUserRepository fullInternalUserRepository;
	private UserTokenRepository userTokenRepository;
	private RoleRepository roleRepository;

	/**
	 * @param userRepository
	 * @param fullInternalUserRepository
	 * @param userTokenRepository
	 */
	@Autowired
	public SecurityDAO(UserRepository userRepository, FullInternalUserRepository fullInternalUserRepository,
			UserTokenRepository userTokenRepository, RoleRepository roleRepository) {
		this.userRepository = userRepository;
		this.fullInternalUserRepository = fullInternalUserRepository;
		this.userTokenRepository = userTokenRepository;
		this.roleRepository = roleRepository;
	}

	@Override
	@Transactional
	public FullInternalUser authenticateTransaction(String userName, String password) {

		User checkUser = this.userRepository.findByUserName(userName);

		if (checkUser != null && (new BCryptPasswordEncoder()).matches(password, checkUser.getPassword())
				&& checkUser.getState() == 1)
			return this.fullInternalUserRepository.findByIdUser(checkUser.getIdUser());
		else
			return null;
	}

	@Override
	@Transactional
	public void saveToken(Integer idUser, String token, String device) {
		this.userTokenRepository.save(new UserToken(idUser, token, device));
	}

	@Override
	@Transactional(readOnly = true)
	public boolean isTokenRecorded(String token) {
		
		UserToken user = this.userTokenRepository.findByToken(token);
		if (user != null) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public boolean removeToken(JWT tokenR) {
		UserToken userToken = this.userTokenRepository.findByIdUserAndToken(Integer.parseInt(tokenR.getTokenId()),
				tokenR.getToken());
		if (userToken != null) {
			this.userTokenRepository.delete(userToken);
			return true;
		}
		return false;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> getAllRoles() {
		return (List<Role>) this.roleRepository.findAll();
	}
}
