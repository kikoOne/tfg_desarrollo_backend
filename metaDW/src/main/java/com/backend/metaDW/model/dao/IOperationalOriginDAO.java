package com.backend.metaDW.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;

public interface IOperationalOriginDAO {

	public Integer saveOperationalOrigin(OperationalOrigin operationalOrigin);

	public Page<?> getOperationalOrigins(Pageable page);

	public OperationalOrigin getOperationalOrigin(Integer id);

	public Integer updateOperationalOrigin(OperationalOrigin operationalOrigin);

	public void removeOperationalOrigin(Integer idOperationalOrigin);
}
