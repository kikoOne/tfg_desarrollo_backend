package com.backend.metaDW.model.vo.drafts;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "draftobjectives", schema = "public")
public class DraftObjective {

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "origin")
	private Integer origin;

	@Column(name = "origin_creation")
	private Timestamp originCreation;

	@Column(name = "origin_update")
	private Timestamp originUpdate;

	@Column(name = "period")
	private int period;

	@Column(name = "applicative")
	private Integer applicative;

	@Column(name = "applicative_denomination")
	private String applicative_denomination;

	@Column(name = "functional_interpretation")
	private String functionalInterpretation;

	@Column(name = "formula")
	private String formula;
	
	@Column(name = "granularity")
	private String granularity;

	@Column(name = "dimensions")
	private String dimensions;
	
	@Column(name = "indicators")
	private String indicators;
	
	/**
	 * 
	 */
	public DraftObjective() {
	}

	/**
	 * @param id
	 */
	public DraftObjective(Integer id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @param origin
	 * @param originCreation
	 * @param originUpdate
	 * @param period
	 * @param applicative
	 * @param applicative_denomination
	 * @param functionalInterpretation
	 * @param formula
	 * @param granularity
	 * @param dimensions
	 * @param indicators
	 */
	public DraftObjective(Integer id, Integer origin, Timestamp originCreation, Timestamp originUpdate, int period,
			Integer applicative, String applicative_denomination, String functionalInterpretation, String formula,
			String granularity, String dimensions, String indicators) {
		this.id = id;
		this.origin = origin;
		this.originCreation = originCreation;
		this.originUpdate = originUpdate;
		this.period = period;
		this.applicative = applicative;
		this.applicative_denomination = applicative_denomination;
		this.functionalInterpretation = functionalInterpretation;
		this.formula = formula;
		this.granularity = granularity;
		this.dimensions = dimensions;
		this.indicators = indicators;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrigin() {
		return origin;
	}

	public void setOrigin(Integer origin) {
		this.origin = origin;
	}

	public Timestamp getOriginCreation() {
		return originCreation;
	}

	public void setOriginCreation(Timestamp originCreation) {
		this.originCreation = originCreation;
	}

	public Timestamp getOriginUpdate() {
		return originUpdate;
	}

	public void setOriginUpdate(Timestamp originUpdate) {
		this.originUpdate = originUpdate;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public Integer getApplicative() {
		return applicative;
	}

	public void setApplicative(Integer applicative) {
		this.applicative = applicative;
	}

	public String getApplicative_denomination() {
		return applicative_denomination;
	}

	public void setApplicative_denomination(String applicative_denomination) {
		this.applicative_denomination = applicative_denomination;
	}

	public String getFunctionalInterpretation() {
		return functionalInterpretation;
	}

	public void setFunctionalInterpretation(String functionalInterpretation) {
		this.functionalInterpretation = functionalInterpretation;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getGranularity() {
		return granularity;
	}

	public void setGranularity(String granularity) {
		this.granularity = granularity;
	}

	public String getDimensions() {
		return dimensions;
	}

	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	public String getIndicators() {
		return indicators;
	}

	public void setIndicators(String indicators) {
		this.indicators = indicators;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((applicative == null) ? 0 : applicative.hashCode());
		result = prime * result + ((applicative_denomination == null) ? 0 : applicative_denomination.hashCode());
		result = prime * result + ((dimensions == null) ? 0 : dimensions.hashCode());
		result = prime * result + ((formula == null) ? 0 : formula.hashCode());
		result = prime * result + ((functionalInterpretation == null) ? 0 : functionalInterpretation.hashCode());
		result = prime * result + ((granularity == null) ? 0 : granularity.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((indicators == null) ? 0 : indicators.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((originCreation == null) ? 0 : originCreation.hashCode());
		result = prime * result + ((originUpdate == null) ? 0 : originUpdate.hashCode());
		result = prime * result + period;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DraftObjective other = (DraftObjective) obj;
		if (applicative == null) {
			if (other.applicative != null)
				return false;
		} else if (!applicative.equals(other.applicative))
			return false;
		if (applicative_denomination == null) {
			if (other.applicative_denomination != null)
				return false;
		} else if (!applicative_denomination.equals(other.applicative_denomination))
			return false;
		if (dimensions == null) {
			if (other.dimensions != null)
				return false;
		} else if (!dimensions.equals(other.dimensions))
			return false;
		if (formula == null) {
			if (other.formula != null)
				return false;
		} else if (!formula.equals(other.formula))
			return false;
		if (functionalInterpretation == null) {
			if (other.functionalInterpretation != null)
				return false;
		} else if (!functionalInterpretation.equals(other.functionalInterpretation))
			return false;
		if (granularity == null) {
			if (other.granularity != null)
				return false;
		} else if (!granularity.equals(other.granularity))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (indicators == null) {
			if (other.indicators != null)
				return false;
		} else if (!indicators.equals(other.indicators))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (originCreation == null) {
			if (other.originCreation != null)
				return false;
		} else if (!originCreation.equals(other.originCreation))
			return false;
		if (originUpdate == null) {
			if (other.originUpdate != null)
				return false;
		} else if (!originUpdate.equals(other.originUpdate))
			return false;
		if (period != other.period)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoryObjective [id=" + id + ", origin=" + origin + ", originCreation=" + originCreation
				+ ", originUpdate=" + originUpdate + ", period=" + period + ", applicative=" + applicative
				+ ", applicative_denomination=" + applicative_denomination + ", functionalInterpretation="
				+ functionalInterpretation + ", formula=" + formula + ", granularity=" + granularity + ", dimensions="
				+ dimensions + ", indicators=" + indicators + "]";
	}
}
