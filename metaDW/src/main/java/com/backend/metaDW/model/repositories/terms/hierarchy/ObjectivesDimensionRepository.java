package com.backend.metaDW.model.repositories.terms.hierarchy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.ObjectivesDimension;

public interface ObjectivesDimensionRepository extends PagingAndSortingRepository<ObjectivesDimension, Integer>{

	public Page<ObjectivesDimension> findByObjective(Integer idObjective, Pageable page);
	
	public List<ObjectivesDimension> findAllByObjective(Integer idObjective);
}
