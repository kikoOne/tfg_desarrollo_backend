package com.backend.metaDW.model.vo.category;

import org.jvnet.hk2.annotations.Optional;

public class CreationCategory {
	
	private Integer idCategory;
	private String name;
	private int state;
	private String version;
	private Integer owner;
	private int type;
	@Optional
	private String definition;
	private Integer idParent;
	
	
	public CreationCategory(){
	}

	/**
	 * @param name
	 * @param state
	 * @param version
	 * @param owner
	 * @param type
	 * @param definition
	 * @param idParent
	 */
	public CreationCategory(String name, int state, String version, Integer owner, int type, String definition,
			Integer idParent) {
		this.name = name;
		this.state = state;
		this.version = version;
		this.owner = owner;
		this.type = type;
		this.definition = definition;
		this.idParent = idParent;
	}

	public Integer getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Integer getIdParent() {
		return idParent;
	}

	public void setIdParent(Integer idParent) {
		this.idParent = idParent;
	}

	@Override
	public String toString() {
		return "CreationCategory [name=" + name + ", state=" + state + ", version=" + version + ", owner=" + owner
				+ ", type=" + type + ", definition=" + definition + ", idParent=" + idParent + "]";
	}
	
}
