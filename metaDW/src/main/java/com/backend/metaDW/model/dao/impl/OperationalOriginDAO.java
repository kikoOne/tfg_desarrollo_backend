package com.backend.metaDW.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.IOperationalOriginDAO;
import com.backend.metaDW.model.repositories.terms.hierarchy.OperationalOriginRepository;
import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;

@Service
public class OperationalOriginDAO implements IOperationalOriginDAO{

	@Autowired
	private OperationalOriginRepository operationalOriginRepository;
	
	@Override
	@Transactional
	public Integer saveOperationalOrigin(OperationalOrigin operationalOrigin) {
		OperationalOrigin result = this.operationalOriginRepository.save(operationalOrigin);
		if (result != null)
			return result.getIdOrigin();
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> getOperationalOrigins(Pageable page) {
		return this.operationalOriginRepository.findAll(page);
	}

	@Override
	@Transactional(readOnly = true)
	public OperationalOrigin getOperationalOrigin(Integer id) {
		OperationalOrigin result = this.operationalOriginRepository.findOne(id);
		if (result != null)
			return result;
		return null; 
	}

	@Override
	@Transactional
	public Integer updateOperationalOrigin(OperationalOrigin operationalOrigin) {
		OperationalOrigin result = this.operationalOriginRepository.save(operationalOrigin);
		if (result != null)
			return result.getIdOrigin();
		return null;
	}

	@Override
	@Transactional
	public void removeOperationalOrigin(Integer idOperationalOrigin) {
		this.operationalOriginRepository.delete(idOperationalOrigin);		
	}

	
}
