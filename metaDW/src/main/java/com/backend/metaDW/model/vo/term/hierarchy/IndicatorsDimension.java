package com.backend.metaDW.model.vo.term.hierarchy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.term.Term;

@Entity
@IdClass(IndicatorsDimensionPK.class)
@Table(name = "indicatorsdimensions", schema = "public")
public class IndicatorsDimension {

	@Id
	@Column(name = "indicator")
	private Integer indicator;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "dimension")
	private Term dimension;
	
	@Column(name = "additivity")
	private boolean additivity;

	/**
	 * 
	 */
	public IndicatorsDimension() {
	}

	/**
	 * @param indicator
	 * @param dimension
	 */
	public IndicatorsDimension(Integer indicator, Term dimension) {
		this.indicator = indicator;
		this.dimension = dimension;
	}

	/**
	 * @param indicator
	 * @param dimension
	 * @param additivity
	 */
	public IndicatorsDimension(Integer indicator, Term dimension, boolean additivity) {
		this.indicator = indicator;
		this.dimension = dimension;
		this.additivity = additivity;
	}

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	public Term getDimension() {
		return dimension;
	}

	public void setDimension(Term dimension) {
		this.dimension = dimension;
	}

	public boolean isAdditivity() {
		return additivity;
	}

	public void setAdditivity(boolean additivity) {
		this.additivity = additivity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (additivity ? 1231 : 1237);
		result = prime * result + ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + ((indicator == null) ? 0 : indicator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndicatorsDimension other = (IndicatorsDimension) obj;
		if (additivity != other.additivity)
			return false;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (indicator == null) {
			if (other.indicator != null)
				return false;
		} else if (!indicator.equals(other.indicator))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IndicatorsDimension [indicator=" + indicator + ", dimension=" + dimension + ", additivity=" + additivity
				+ "]";
	}
}

@SuppressWarnings("serial")
class IndicatorsDimensionPK implements Serializable {

	private Integer indicator;
	private Integer dimension;

	/**
	 * 
	 */
	public IndicatorsDimensionPK() {
	}

	/**
	 * @param indicator
	 * @param dimension
	 */
	public IndicatorsDimensionPK(Integer indicator, Integer dimension) {
		this.indicator = indicator;
		this.dimension = dimension;
	}

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

}
