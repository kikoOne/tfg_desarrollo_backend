package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.FullDimension;

public interface FullDimensionRepository extends PagingAndSortingRepository<FullDimension, Integer> {
	
	public FullDimension findOneByIdTerm(Integer idTerm);
	
//	@Query("SELECT fd FROM FullDimension fd WHERE fd.idTerm.name LIKE %:name%")
//	public Page<FullDimension> findFullTermDimensionsByName(@Param("name") String name, Pageable page);
}

