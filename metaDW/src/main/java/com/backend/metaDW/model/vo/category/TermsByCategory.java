package com.backend.metaDW.model.vo.category;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.term.FullTerm;

@Entity
@IdClass(TermsByCategoryPK.class)
@Table(name = "termsbycategory", schema = "public")
public class TermsByCategory {

	@Id
	@ManyToOne
	@JoinColumn(name = "id_category")
	private Category category;

	@Id
	@ManyToOne
	@JoinColumn(name = "id_term")
	private FullTerm term;

	/**
	 * void
	 */
	public TermsByCategory() {
	}

	/**
	 * @param category
	 * @param term
	 */
	public TermsByCategory(Category category, FullTerm term) {
		this.category = category;
		this.term = term;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public FullTerm getTerm() {
		return term;
	}

	public void setTerm(FullTerm term) {
		this.term = term;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermsByCategory other = (TermsByCategory) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.equals(other.term))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermsByCategory [idCategory=" + category + ", term=" + term + "]";
	}
}

@SuppressWarnings("serial")
class TermsByCategoryPK implements Serializable {

	private Integer category;
	private Integer term;

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}
}
