package com.backend.metaDW.model.repositories.terms;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.Incidence;

public interface IncidenceRepository extends PagingAndSortingRepository<Incidence, Integer> {

	public Page<Incidence> findByIdTerm(Integer idTerm, Pageable page);

	public List<Incidence> findAllByIdTerm(Integer idTerm);

	public Page<Incidence> findAll(Pageable page);

	public Incidence findOneByIdIncidence(Integer idIncidence);

	public Incidence findOneByIdIncidenceAndIdTerm(Integer idIncidence, Integer idTerm);
}
