package com.backend.metaDW.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.scope.FullScope;
import com.backend.metaDW.model.vo.scope.Scope;

public interface IScopeDAO {

	public Integer saveScope(Scope scope);

	public void saveScope(List<Scope> scope);

	public Page<FullScope> findAllScopes(Pageable page);

	public FullScope findScopeById(Integer idScope);

	public Page<?> findTermsFromScope(Integer idScope, Pageable page);

	public boolean updateScopeData(Scope newScope);

	public void removeScope(Integer idScope);
	
	public List<Scope> findAllScopes();
}
