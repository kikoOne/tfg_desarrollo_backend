package com.backend.metaDW.model.vo.user;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.backend.metaDW.model.vo.term.Term;

@Entity
@IdClass(TermsFollowedPK.class)
@Table(name = "termsfollowed", schema = "public")
public class TermFollowed {

	@Id
	@Column(name = "id_user")
	private Integer idUser;

	@Id
	@ManyToOne
	@JoinColumn(name = "id_term")
	private Term term;

	/**
	 * 
	 */
	public TermFollowed() {
	}

	/**
	 * @param idUser
	 * @param term
	 */
	public TermFollowed(Integer idUser, Term term) {
		this.idUser = idUser;
		this.term = term;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermFollowed other = (TermFollowed) obj;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.equals(other.term))
			return false;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermsFollowed [idUser=" + idUser + ", idTerm=" + term + "]";
	}

}

@SuppressWarnings("serial")
class TermsFollowedPK implements Serializable {

	private Integer idUser;
	private Integer term;	
	
	/**
	 * 
	 */
	public TermsFollowedPK() {
	}

	/**
	 * @param idUser
	 * @param term
	 */
	public TermsFollowedPK(Integer idUser, Integer term) {
		this.idUser = idUser;
		this.term = term;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}

}
