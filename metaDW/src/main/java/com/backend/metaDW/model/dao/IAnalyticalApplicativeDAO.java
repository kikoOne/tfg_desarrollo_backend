package com.backend.metaDW.model.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;

public interface IAnalyticalApplicativeDAO {

	public Integer saveAnalyticalApplicative(AnalyticalApplicative analyticalApplicative);

	public Page<?> getAnalyticalApplicatives(Pageable page);
	
	public AnalyticalApplicative getAnalyticalApplicative (Integer id);

	public Integer updateAnalyticalApplicative(AnalyticalApplicative analyticalApplicative);

	public void removeAnalyticalApplicative(Integer idAnalyticalApplicative);
}
