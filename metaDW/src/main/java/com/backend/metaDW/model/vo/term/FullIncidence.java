package com.backend.metaDW.model.vo.term;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "incidences", schema = "public")
public class FullIncidence {

	@Id
	@SequenceGenerator(name = "incidences_id_incidence_seq", sequenceName = "incidences_id_incidence_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "incidences_id_incidence_seq")
	@Column(name = "id_incidence", updatable = false)
	private Integer idIncidence;

	@ManyToOne
	@JoinColumn(name = "id_term")
	private Term idTerm;

	@Column(name = "creation")
	private Timestamp creation;

	@Column(name = "description")
	private String description;

	/**
	 * 
	 */
	public FullIncidence() {
	}

	/**
	 * @param idIncidence
	 * @param idTerm
	 */
	public FullIncidence(Integer idIncidence, Term idTerm) {
		this.idIncidence = idIncidence;
		this.idTerm = idTerm;
	}

	/**
	 * @param idIncidence
	 * @param idTerm
	 * @param description
	 */
	public FullIncidence(Integer idIncidence, Term idTerm, String description) {
		this.idIncidence = idIncidence;
		this.idTerm = idTerm;
		this.description = description;
	}

	public Integer getIdIncidence() {
		return idIncidence;
	}

	public void setIdIncidence(Integer idIncidence) {
		this.idIncidence = idIncidence;
	}

	public Term getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Term idTerm) {
		this.idTerm = idTerm;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((idIncidence == null) ? 0 : idIncidence.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FullIncidence other = (FullIncidence) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idIncidence == null) {
			if (other.idIncidence != null)
				return false;
		} else if (!idIncidence.equals(other.idIncidence))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Incidence [idIncidence=" + idIncidence + ", idTerm=" + idTerm + ", description=" + description + "]";
	}
}
