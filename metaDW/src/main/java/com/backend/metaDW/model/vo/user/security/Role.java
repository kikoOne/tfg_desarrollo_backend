package com.backend.metaDW.model.vo.user.security;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "roles", schema = "public")
public class Role implements GrantedAuthority {

	private static final long serialVersionUID = -125258681634297314L;

	@Id
	@SequenceGenerator(name = "roles_id_role_seq", sequenceName = "roles_id_role_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_id_role_seq")
	@Column(name = "id_role", updatable = false)
	private Integer idRole;

	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;

	@ManyToMany(mappedBy = "roles")
	private Collection<FullInternalUser> users;

	/**
	 * 
	 */
	public Role() {
	}

	/**
	 * @param idRole
	 */
	public Role(Integer idRole) {
		this.idRole = idRole;
	}

	/**
	 * @param name
	 */
	public Role(String name) {
		this.name = name;
	}

	/**
	 * @param idRole
	 * @param name
	 */
	public Role(Integer idRole, String name) {
		this.idRole = idRole;
		this.name = name;
	}

	public Integer getIdRole() {
		return idRole;
	}

	public void setIdRole(Integer idRole) {
		this.idRole = idRole;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRole == null) ? 0 : idRole.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (idRole == null) {
			if (other.idRole != null)
				return false;
		} else if (!idRole.equals(other.idRole))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [idRole=" + idRole + ", name=" + name + "]";
	}

	@Override
	public String getAuthority() {
		return this.name;
	}
}
