package com.backend.metaDW.model.repositories.scopes;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.scope.Scope;

public interface ScopeRepository extends PagingAndSortingRepository<Scope, Integer>{
	
	public Page<Scope> findByNameContainingIgnoreCase(String name, Pageable page);
	
	public Page<Scope> findByNameContainingIgnoreCaseAndOwner(String name, Integer owner, Pageable page);
}
