package com.backend.metaDW.model.repositories.terms.hierarchy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.AsociatedIndicator;

public interface AsociatedIndicatorRepository extends PagingAndSortingRepository<AsociatedIndicator, Integer>{
	
	public Page<AsociatedIndicator> findByObjective(Integer idObjective, Pageable page);
	
	public List<AsociatedIndicator> findAllByObjective(Integer idObjective); 
}
