package com.backend.metaDW.model.vo.history;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.user.User;

@Entity
@Table(name = "historycategories", schema = "public")
public class HistoryCategory {

	@Id
	@SequenceGenerator(name = "historycategories_id_seq", sequenceName = "historycategories_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "historycategories_id_seq")
	@Column(name = "id", updatable = false)
	private Integer id;

	@Column(name = "id_category")
	private Integer idCategory;

	@Column(name = "name")
	private String name;

	@Column(name = "definition")
	private String definition;

	@Column(name = "version")
	private String version;

	@ManyToOne
	@JoinColumn(name = "owner")
	private User owner;

	@Column(name = "creation")
	private Timestamp creation;

	@Column(name = "last_update")
	private Timestamp lastUpdate;

	@Column(name = "type")
	private int type;

	@Column(name = "parent")
	private Integer parent;

	public HistoryCategory() {
	}

	public HistoryCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	/**
	 * @param id
	 * @param idCategory
	 * @param name
	 * @param definition
	 * @param version
	 * @param owner
	 * @param creation
	 * @param type
	 * @param parent
	 */
	public HistoryCategory(Integer id, Integer idCategory, String name, String definition, String version,
			User owner, Timestamp creation, int type, Integer parent) {
		this.id = id;
		this.idCategory = idCategory;
		this.name = name;
		this.definition = definition;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
		this.type = type;
		this.parent = parent;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idCategory == null) ? 0 : idCategory.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + type;
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryCategory other = (HistoryCategory) obj;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idCategory == null) {
			if (other.idCategory != null)
				return false;
		} else if (!idCategory.equals(other.idCategory))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (type != other.type)
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoryCategory [id=" + id + ", idCategory=" + idCategory + ", name=" + name + ", definition="
				+ definition + ", version=" + version + ", owner=" + owner + ", creation=" + creation + ", type=" + type
				+ ", parent=" + parent + "]";
	}
}
