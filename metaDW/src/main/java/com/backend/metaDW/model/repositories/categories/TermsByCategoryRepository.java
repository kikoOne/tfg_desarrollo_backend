package com.backend.metaDW.model.repositories.categories;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.backend.metaDW.model.vo.category.TermsByCategory;
//import com.backend.metaDW.model.vo.term.FullTerm;

public interface TermsByCategoryRepository extends PagingAndSortingRepository<TermsByCategory, Integer> {

	@Query("SELECT tbc.term FROM TermsByCategory tbc WHERE tbc.category.idCategory =:idCategory")
	public Page<?> findTermsFromCategory(@Param("idCategory") Integer idCategory, Pageable page);
	
	@Query("SELECT tbc FROM TermsByCategory tbc WHERE tbc.category.idCategory =:idCategory")
	public List<TermsByCategory> findAllTermsFromCategory(@Param("idCategory") Integer idCategory);

	@Query("SELECT tbc.term FROM TermsByCategory tbc WHERE tbc.category.idCategory = :idCategory AND LOWER(tbc.term.name) LIKE CONCAT('%',LOWER(:name),'%') AND tbc.term.type.type IN (:types)")
	public Page<?> findTermsByNameAndTypeFromCategory(@Param("idCategory") Integer idCategory,
			@Param("name") String name, @Param("types") List<Integer> types, Pageable page);

	@Query("SELECT tbc.term FROM TermsByCategory tbc WHERE tbc.category.idCategory = :idCategory AND LOWER(tbc.term.name) LIKE CONCAT('%',LOWER(:name),'%')")
	public Page<?> findTermsByNameFromCategory(@Param("idCategory") Integer idCategory,
			@Param("name") String name, Pageable page);

	@Query("SELECT tbc.term FROM TermsByCategory tbc WHERE tbc.category.idCategory = :idCategory AND tbc.term.type.type IN (:types)")
	public Page<?> findTermsByTypeFromCategory(@Param("idCategory") Integer idCategory,
			@Param("types") List<Integer> types, Pageable page);

	@Query("SELECT tbc FROM TermsByCategory tbc WHERE id_term =:idTerm")
	public List<TermsByCategory> findCategoriesFromTerm(@Param("idTerm") Integer idTerm);
}
