package com.backend.metaDW.model.vo.scope;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.backend.metaDW.model.vo.term.FullTerm;

@Entity
@IdClass(TermsByCategoryPK.class)
@Table(name = "termsbyscope", schema = "public")
public class TermsByScope {

	@Id
	@OneToOne
	@JoinColumn(name = "id_scope")
	private Scope scope;

	@Id
	@ManyToOne
	@JoinColumn(name = "id_term")
	private FullTerm term;

	/**
	 * void
	 */
	public TermsByScope() {
	}

	/**
	 * @param scope
	 * @param term
	 */
	public TermsByScope(Scope scope, FullTerm term) {
		this.scope = scope;
		this.term = term;
	}

	public Scope getScope() {
		return scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public FullTerm getTerm() {
		return term;
	}

	public void setTerm(FullTerm term) {
		this.term = term;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((scope == null) ? 0 : scope.hashCode());
		result = prime * result + ((term == null) ? 0 : term.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermsByScope other = (TermsByScope) obj;
		if (scope == null) {
			if (other.scope != null)
				return false;
		} else if (!scope.equals(other.scope))
			return false;
		if (term == null) {
			if (other.term != null)
				return false;
		} else if (!term.equals(other.term))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermsByCategory [idCategory=" + scope + ", term=" + term + "]";
	}
}

@SuppressWarnings("serial")
class TermsByCategoryPK implements Serializable {

	private Integer scope;
	private Integer term;

	public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

	public Integer getTerm() {
		return term;
	}

	public void setTerm(Integer term) {
		this.term = term;
	}
}
