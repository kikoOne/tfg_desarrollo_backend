package com.backend.metaDW.model.dao.impl;

import org.springframework.stereotype.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.exceptions.NotFoundException;
import com.backend.metaDW.model.dao.IUserDAO;
import com.backend.metaDW.model.repositories.categories.CategoryRepository;
import com.backend.metaDW.model.repositories.scopes.ScopeRepository;
import com.backend.metaDW.model.repositories.terms.TermRepository;
import com.backend.metaDW.model.repositories.user.FullExternalUserRepository;
import com.backend.metaDW.model.repositories.user.TermFollowedRepository;
import com.backend.metaDW.model.repositories.user.UserRepository;
import com.backend.metaDW.model.repositories.user.security.RoleRepository;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.user.FullExternalUser;
import com.backend.metaDW.model.vo.user.TermFollowed;
import com.backend.metaDW.model.vo.user.User;
import com.backend.metaDW.model.vo.user.security.Role;

@Service
public class UserDAO implements IUserDAO {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FullExternalUserRepository fullExternalUserRepository;
	@Autowired
	private TermRepository termRepository;
	@Autowired
	private ScopeRepository scopeRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private TermFollowedRepository termFollowedRepository;

	@Override
	@Transactional
	public void saveUser(User user) {
		this.userRepository.save(user);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullExternalUser> findAllUsers(Pageable page) {
		return this.fullExternalUserRepository.findAll(page);
	}

	@Override
	@Transactional(readOnly = true)
	public FullExternalUser findUserById(Integer idUser) {
		return this.fullExternalUserRepository.findOne(idUser);
	}

	@Override
	@Transactional
	public boolean updateUserData(FullExternalUser user) {
		if (this.fullExternalUserRepository.exists(user.getIdUser())) {
			this.fullExternalUserRepository.save(user);
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public boolean updatePassword(Integer idUser, String password, String newPassword) {
		User user = this.userRepository.findOne(idUser);		
		if (user == null)
			return false;
		
		if (!(new BCryptPasswordEncoder()).matches(password, user.getPassword()))
			return false;
		
		user.setPassword((new BCryptPasswordEncoder()).encode(newPassword));
		User updated = this.userRepository.save(user);		
		if (updated == null)
			return false;
		
		return true;

	}

	@Override
	@Transactional
	public boolean desactivateUser(Integer idUser) {
		User user = this.userRepository.findOne(idUser);
		if (user != null) {
			user.setState(0);
			this.userRepository.save(user);
			return true;
		}
		return false;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Term> findTermsFromOwnerByNameAndType(Integer owner, String query, List<Integer> types, Pageable page) {
		return this.termRepository.findByNameContainingIgnoreCaseAndTypeTypeInAndOwner(query, types, owner, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Scope> findScopesFromOwnerByName(Integer owner, String query, Pageable page) {
		return this.scopeRepository.findByNameContainingIgnoreCaseAndOwner(query, owner, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Category> findCategoriesFromOwnerByName(Integer owner, String query, Pageable page) {
		return this.categoryRepository.findByNameContainingIgnoreCaseAndOwner(query, owner, page);
	}

	@Override
	@Transactional
	public Role saveRole(Role role) {
		return this.roleRepository.save(role);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> getAllRoles() {
		return (List<Role>) this.roleRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public User findUserByUserName(String userName) {
		return this.userRepository.findByUserName(userName);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullExternalUser> findAllActivatedUsers(Pageable page) {
		return this.fullExternalUserRepository.findByState(1, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullExternalUser> findAllDesactivatedUsers(Pageable page) {
		return this.fullExternalUserRepository.findByState(0, page);
	}

	@Override
	@Transactional
	public Integer activateUser(Integer idUser) {
		FullExternalUser user = this.fullExternalUserRepository.findOne(idUser);
		if (user == null)
			return null;
		user.setState(1);
		user = this.fullExternalUserRepository.save(user);
		return user.getIdUser();
	}

	@Override
	@Transactional
	public boolean addTermBookmark(Integer idUser, Integer idTerm) {

		TermFollowed bookmark = this.termFollowedRepository.findOneByIdUserAndTermIdTerm(idUser, idTerm);
		if (bookmark != null)
			return false;

		Term term = this.termRepository.findOne(idTerm);
		if (term == null)
			return false;

		TermFollowed res = this.termFollowedRepository.save(new TermFollowed(idUser, term));
		if (res == null)
			return false;
		return true;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<TermFollowed> getFollowedTerms(Integer idUser, Pageable page) {
		return this.termFollowedRepository.findByIdUser(idUser, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TermFollowed> getAllFollowedTerms(Integer idUser) {
		return this.termFollowedRepository.findAllByIdUserOrderByTermName(idUser);
	}

	@Override
	@Transactional
	public void removeFollowedTerm(Integer idUser, Integer idTerm) {
		TermFollowed termFollowed = this.termFollowedRepository.findOneByIdUserAndTermIdTerm(idUser, idTerm);
		if (termFollowed != null) {
			this.termFollowedRepository.deleteByIdUserAndTermIdTerm(idUser, idTerm);
		} else {
			throw new NotFoundException();
		}
	}

	@Override
	public TermFollowed getFollowedTerm(Integer idUser, Integer idTerm) {
		return this.termFollowedRepository.findOneByIdUserAndTermIdTerm(idUser, idTerm);
	}
}
