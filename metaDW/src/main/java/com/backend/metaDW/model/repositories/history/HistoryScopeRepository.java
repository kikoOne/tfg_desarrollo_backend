package com.backend.metaDW.model.repositories.history;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryScope;

public interface HistoryScopeRepository extends PagingAndSortingRepository<HistoryScope, Integer>{

	public Page<HistoryScope> findAllByIdScope(Integer idScope, Pageable page);
	
	public HistoryScope findByIdScopeAndId(Integer idScope, Integer id);
}
