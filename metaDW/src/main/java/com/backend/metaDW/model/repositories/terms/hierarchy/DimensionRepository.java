package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.Dimension;

public interface DimensionRepository extends PagingAndSortingRepository<Dimension, Integer>{
	
}
