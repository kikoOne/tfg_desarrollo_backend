package com.backend.metaDW.model.repositories.history;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryDimension;

public interface HistoryDimensionRepository extends PagingAndSortingRepository<HistoryDimension, Integer>{

}
