package com.backend.metaDW.model.repositories.categories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.category.SimpleSubcategory;

public interface SimpleSubcategoryRepository extends PagingAndSortingRepository<SimpleSubcategory, Integer>{

}
