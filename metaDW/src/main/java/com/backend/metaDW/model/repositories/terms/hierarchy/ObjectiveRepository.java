package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.Objective;

public interface ObjectiveRepository extends PagingAndSortingRepository<Objective, Integer>{

}
