package com.backend.metaDW.model.repositories.user;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.user.TermFollowed;

public interface TermFollowedRepository extends PagingAndSortingRepository<TermFollowed, Integer>{

	public TermFollowed findOneByIdUserAndTermIdTerm(Integer idUser, Integer term);
	
	public List<TermFollowed> findAllByIdUserOrderByTermName(Integer idUser);
	
	public Page<TermFollowed> findByIdUser(Integer idUser, Pageable page);
	
	public void deleteByIdUserAndTermIdTerm(Integer idUser, Integer term);
}
