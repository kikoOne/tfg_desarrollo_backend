package com.backend.metaDW.model.repositories.categories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.category.FullCategory;

public interface FullCategoryRepository extends PagingAndSortingRepository<FullCategory, Integer>{

	Page<FullCategory> findByType(int type, Pageable page);
	
	Page<FullCategory> findByTypeAndNameContainingIgnoreCase(Integer type, String name, Pageable page);
}
