package com.backend.metaDW.model.vo.term.hierarchy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "analyticalapplicatives", schema = "public")
public class AnalyticalApplicative {

	@Id
	@SequenceGenerator(name = "analyticalapplicatives_id_applicative_seq", sequenceName = "analyticalapplicatives_id_applicative_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "analyticalapplicatives_id_applicative_seq")
	@Column(name = "id_applicative", updatable = false)
	private Integer idApplicative;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	/**
	 * void constructor
	 */
	public AnalyticalApplicative() {
	}

	/**
	 * @param idApplicative
	 */
	public AnalyticalApplicative(Integer idApplicative) {
		this.idApplicative = idApplicative;
	}

	/**
	 * @param idApplicative
	 * @param name
	 * @param description
	 */
	public AnalyticalApplicative(Integer idApplicative, String name, String description) {
		this.idApplicative = idApplicative;
		this.name = name;
		this.description = description;
	}

	public Integer getIdApplicative() {
		return idApplicative;
	}

	public void setIdApplicative(Integer idApplicative) {
		this.idApplicative = idApplicative;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((idApplicative == null) ? 0 : idApplicative.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnalyticalApplicative other = (AnalyticalApplicative) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (idApplicative == null) {
			if (other.idApplicative != null)
				return false;
		} else if (!idApplicative.equals(other.idApplicative))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AnalyticalApplicative [idApplicative=" + idApplicative + ", name=" + name + ", description="
				+ description + "]";
	}
}
