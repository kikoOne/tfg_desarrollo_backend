package com.backend.metaDW.model.vo.user.security;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.backend.metaDW.security.jwt.JWT;

public class CustomAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = -1076206340202713857L;

	private JWT jwt;

	public CustomAuthenticationToken(Object pricipal, Object credentials, JWT jwt) {
		super(pricipal, credentials);
		this.jwt = jwt;
	}

	public CustomAuthenticationToken(Object principal, Object credentials, JWT jwt,
			GrantedAuthority[] authorities) {
		super(principal, credentials, Arrays.asList(authorities));
		this.jwt = jwt;
	}

	public CustomAuthenticationToken(Object principal, Object credentials, JWT jwt,
			Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
		this.jwt = jwt;
	}

	public JWT getToken() {
		return jwt;
	}

	public void setToken(JWT jwt) {
		this.jwt = jwt;
	}

}
