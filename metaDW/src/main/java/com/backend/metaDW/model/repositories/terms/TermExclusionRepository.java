package com.backend.metaDW.model.repositories.terms;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.TermExclusion;

public interface TermExclusionRepository extends PagingAndSortingRepository<TermExclusion, Integer>{

	public Page<TermExclusion> findByIdTerm(Integer idTerm, Pageable page);
	
	public List<TermExclusion> findAllByIdTerm(Integer idTerm);
}
