package com.backend.metaDW.model.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.IAnalyticalApplicativeDAO;
import com.backend.metaDW.model.repositories.terms.hierarchy.AnalyticalApplicativeRepository;
import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;

@Service
public class AnalyticalApplicativeDAO implements IAnalyticalApplicativeDAO {

	@Autowired
	private AnalyticalApplicativeRepository analyticalApplicativeRepository;
	
	@Override
	@Transactional
	public Integer saveAnalyticalApplicative(AnalyticalApplicative analyticalApplicative) {
		AnalyticalApplicative result = this.analyticalApplicativeRepository.save(analyticalApplicative);
		if (result != null)
			return result.getIdApplicative();
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> getAnalyticalApplicatives(Pageable page) {
		return this.analyticalApplicativeRepository.findAll(page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public AnalyticalApplicative getAnalyticalApplicative(Integer id) {
		return this.analyticalApplicativeRepository.findOne(id);
	}

	@Override
	@Transactional
	public Integer updateAnalyticalApplicative(AnalyticalApplicative analyticalApplicative) {
		AnalyticalApplicative result = this.analyticalApplicativeRepository.save(analyticalApplicative);
		if (result != null)
			return result.getIdApplicative();
		return null;
	}

	@Override
	@Transactional
	public void removeAnalyticalApplicative(Integer idAnalyticalApplicative) {
		this.analyticalApplicativeRepository.delete(idAnalyticalApplicative);		
	}

	
}
