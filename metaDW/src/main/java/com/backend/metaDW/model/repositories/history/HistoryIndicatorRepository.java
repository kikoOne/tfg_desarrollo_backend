package com.backend.metaDW.model.repositories.history;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryIndicator;

public interface HistoryIndicatorRepository extends PagingAndSortingRepository<HistoryIndicator, Integer>{

}
