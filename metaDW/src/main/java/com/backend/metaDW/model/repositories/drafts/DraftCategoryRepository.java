package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftCategory;

public interface DraftCategoryRepository extends PagingAndSortingRepository<DraftCategory, Integer>{

	public Page<DraftCategory> findAllByOwner(Integer owner, Pageable page);
}
