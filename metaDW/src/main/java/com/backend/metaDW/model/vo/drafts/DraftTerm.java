package com.backend.metaDW.model.vo.drafts;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.term.TermType;

@Entity
@Table(name = "draftterms", schema = "public")
public class DraftTerm {

	@Id
	@SequenceGenerator(name = "draftterms_id_seq", sequenceName = "draftterms_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "draftterms_id_seq")
	@Column(name = "id", updatable = false)
	private Integer id;
	
	@Column(name = "id_term")
	private Integer idTerm;

	@Column(name = "name")
	private String name;

	@Column(name = "short_name")
	private String shortName;

	@Column(name = "definition")
	private String definition;

	@Column(name = "extended_definition")
	private String extendedDefinition;

	@Column(name = "version")
	private String version;

	@Column(name = "owner")
	private Integer owner;

	@Column(name = "creation", insertable = false)
	private Timestamp creation;

	@ManyToOne
	@JoinColumn(name = "type")
	private TermType type;
	
	@Column(name = "scope")
	private Integer scope;

	@Column(name = "categories")
	private String categories;
	
	@Column(name = "denominations")
	private String denominations;
	
	@Column(name = "examples")
	private String examples;
	
	@Column(name = "exclusions")
	private String exclusions;

	/**
	 * void
	 */
	public DraftTerm() {
	}

	/**
	 * @param idTerm
	 */
	public DraftTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	/**
	 * @param id
	 * @param idTerm
	 * @param name
	 * @param shortName
	 * @param definition
	 * @param extendedDefinition
	 * @param version
	 * @param owner
	 * @param creation
	 * @param type
	 * @param scope
	 */
	public DraftTerm(Integer id, Integer idTerm, String name, String shortName, String definition,
			String extendedDefinition, String version, Integer owner, Timestamp creation, TermType type,
			Integer scope) {
		this.id = id;
		this.idTerm = idTerm;
		this.name = name;
		this.shortName = shortName;
		this.definition = definition;
		this.extendedDefinition = extendedDefinition;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
		this.type = type;
		this.scope = scope;
	}

	/**
	 * @param id
	 * @param idTerm
	 * @param name
	 * @param shortName
	 * @param definition
	 * @param extendedDefinition
	 * @param version
	 * @param owner
	 * @param creation
	 * @param type
	 * @param scope
	 * @param categories
	 * @param denominations
	 * @param examples
	 * @param exclusions
	 */
	public DraftTerm(Integer id, Integer idTerm, String name, String shortName, String definition,
			String extendedDefinition, String version, Integer owner, Timestamp creation, TermType type, Integer scope,
			String categories, String denominations, String examples, String exclusions) {
		this.id = id;
		this.idTerm = idTerm;
		this.name = name;
		this.shortName = shortName;
		this.definition = definition;
		this.extendedDefinition = extendedDefinition;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
		this.type = type;
		this.scope = scope;
		this.categories = categories;
		this.denominations = denominations;
		this.examples = examples;
		this.exclusions = exclusions;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getExtendedDefinition() {
		return extendedDefinition;
	}

	public void setExtendedDefinition(String extendedDefinition) {
		this.extendedDefinition = extendedDefinition;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	public TermType getType() {
		return type;
	}

	public void setType(TermType type) {
		this.type = type;
	}

	public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

	public String getCategories() {
		return categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getDenominations() {
		return denominations;
	}

	public void setDenominations(String denominations) {
		this.denominations = denominations;
	}

	public String getExamples() {
		return examples;
	}

	public void setExamples(String examples) {
		this.examples = examples;
	}

	public String getExclusions() {
		return exclusions;
	}

	public void setExclusions(String exclusions) {
		this.exclusions = exclusions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categories == null) ? 0 : categories.hashCode());
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((denominations == null) ? 0 : denominations.hashCode());
		result = prime * result + ((examples == null) ? 0 : examples.hashCode());
		result = prime * result + ((exclusions == null) ? 0 : exclusions.hashCode());
		result = prime * result + ((extendedDefinition == null) ? 0 : extendedDefinition.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((scope == null) ? 0 : scope.hashCode());
		result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DraftTerm other = (DraftTerm) obj;
		if (categories == null) {
			if (other.categories != null)
				return false;
		} else if (!categories.equals(other.categories))
			return false;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (denominations == null) {
			if (other.denominations != null)
				return false;
		} else if (!denominations.equals(other.denominations))
			return false;
		if (examples == null) {
			if (other.examples != null)
				return false;
		} else if (!examples.equals(other.examples))
			return false;
		if (exclusions == null) {
			if (other.exclusions != null)
				return false;
		} else if (!exclusions.equals(other.exclusions))
			return false;
		if (extendedDefinition == null) {
			if (other.extendedDefinition != null)
				return false;
		} else if (!extendedDefinition.equals(other.extendedDefinition))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (scope == null) {
			if (other.scope != null)
				return false;
		} else if (!scope.equals(other.scope))
			return false;
		if (shortName == null) {
			if (other.shortName != null)
				return false;
		} else if (!shortName.equals(other.shortName))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoryTerm [id=" + id + ", idTerm=" + idTerm + ", name=" + name + ", shortName=" + shortName
				+ ", definition=" + definition + ", extendedDefinition=" + extendedDefinition + ", version=" + version
				+ ", owner=" + owner + ", creation=" + creation + ", type=" + type + ", scope=" + scope
				+ ", categories=" + categories + ", denominations=" + denominations + ", examples=" + examples
				+ ", exclusions=" + exclusions + "]";
	}
}
