package com.backend.metaDW.model.repositories.terms.hierarchy;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.FullIndicator;

public interface FullIndicatorRepository extends PagingAndSortingRepository<FullIndicator, Integer> {
	
	public FullIndicator findOneByIdTerm(Integer idTerm);
}
