package com.backend.metaDW.model.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.exceptions.InternalServerException;
import com.backend.metaDW.model.dao.IHistoryDAO;
import com.backend.metaDW.model.dao.ITermDAO;
import com.backend.metaDW.model.repositories.categories.CategoryRepository;
import com.backend.metaDW.model.repositories.categories.SimpleTermsByCategoryRepository;
import com.backend.metaDW.model.repositories.categories.TermsByCategoryRepository;
import com.backend.metaDW.model.repositories.scopes.SimpleTermsByScopeRepository;
import com.backend.metaDW.model.repositories.scopes.TermsByScopeRepository;
import com.backend.metaDW.model.repositories.terms.DenominationRepository;
import com.backend.metaDW.model.repositories.terms.FullTermRepository;
import com.backend.metaDW.model.repositories.terms.IncidenceRepository;
import com.backend.metaDW.model.repositories.terms.TermExampleRepository;
import com.backend.metaDW.model.repositories.terms.TermExclusionRepository;
import com.backend.metaDW.model.repositories.terms.TermRepository;
import com.backend.metaDW.model.repositories.terms.TermTypeRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.AsociatedIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.DimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.FullDimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.FullIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.FullObjectiveRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.IndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.IndicatorsDimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.InformIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.InformRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.ObjectiveRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.ObjectivesDimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.SimpleAsociatedIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.SimpleIndicatorDimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.SimpleInformIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.SimpleObjectivesDimensionRepository;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.SimpleTermsByCategory;
import com.backend.metaDW.model.vo.category.TermsByCategory;
import com.backend.metaDW.model.vo.scope.SimpleTermsByScope;
import com.backend.metaDW.model.vo.scope.TermsByScope;
import com.backend.metaDW.model.vo.term.CreationTerm;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.FullTerm;
import com.backend.metaDW.model.vo.term.Incidence;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.TermExample;
import com.backend.metaDW.model.vo.term.TermExclusion;
import com.backend.metaDW.model.vo.term.TermType;
import com.backend.metaDW.model.vo.term.hierarchy.AsociatedIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.Dimension;
import com.backend.metaDW.model.vo.term.hierarchy.FullDimension;
import com.backend.metaDW.model.vo.term.hierarchy.FullIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.FullObjective;
import com.backend.metaDW.model.vo.term.hierarchy.Indicator;
import com.backend.metaDW.model.vo.term.hierarchy.IndicatorsDimension;
import com.backend.metaDW.model.vo.term.hierarchy.Inform;
import com.backend.metaDW.model.vo.term.hierarchy.InformIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.Objective;
import com.backend.metaDW.model.vo.term.hierarchy.ObjectivesDimension;
import com.backend.metaDW.model.vo.term.hierarchy.SimpleAsociatedIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.SimpleIndicatorDimension;
import com.backend.metaDW.model.vo.term.hierarchy.SimpleInformIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.SimpleObjectivesDimension;

@Service
public class TermDAO implements ITermDAO {

	private TermRepository termRepository;
	private FullTermRepository fullTermRepository;
	private DenominationRepository denominationRepository;
	private TermExampleRepository termExampleRepository;
	private TermExclusionRepository termExclusionRepository;
	private InformRepository informRepository;
	private AsociatedIndicatorRepository asociatedIndicatorRepository;
	private InformIndicatorRepository informIndicatorRepository;
	private IndicatorsDimensionRepository indicatorsDimensionRepository;
	private ObjectivesDimensionRepository objectivesDimensionRepository;
	private DimensionRepository dimensionRepository;
	private IndicatorRepository indicatorRepository;
	private ObjectiveRepository objectiveRepository;
	private SimpleTermsByScopeRepository simpleTermsByScopeRepository;
	private SimpleTermsByCategoryRepository simpleTermsByCategoryRepository;
	private SimpleIndicatorDimensionRepository simpleIndicatorDimensionRepository;
	private SimpleObjectivesDimensionRepository simpleObjectivesDimensionRepository;
	private SimpleAsociatedIndicatorRepository simpleAsociatedIndicatorRepository;
	private SimpleInformIndicatorRepository simpleInformIndicatorRepository;
	private TermTypeRepository termTypeRepository;
	private TermsByScopeRepository termsByScopeRepository;
	private TermsByCategoryRepository termsByCategoryRepository;
	private FullDimensionRepository fullDimensionRepository;
	private FullIndicatorRepository fullIndicatorRepository;
	private FullObjectiveRepository fullObjectiveRepository;
	private IncidenceRepository incidenceRepository;
	private IHistoryDAO historyDAO;
	private CategoryRepository categoryRepository;

	/**
	 * 
	 * @param termRepository
	 * @param fullTermRepository
	 * @param denominationRepository
	 * @param termExampleRepository
	 * @param termExclusionRepository
	 * @param informRepository
	 * @param asociatedIndicatorRepository
	 * @param informIndicatorRepository
	 * @param indicatorsDimensionRepository
	 * @param objectivesDimensionRepository
	 * @param dimensionRepository
	 * @param indicatorRepository
	 * @param objectiveRepository
	 * @param simpleTermsByScopeRepository
	 * @param simpleTermsByCategoryRepository
	 * @param simpleIndicatorDimensionRepository
	 * @param simpleObjectivesDimensionRepository
	 * @param simpleAsociatedIndicatorRepository
	 * @param simpleInformIndicatorRepository
	 * @param termTypeRepository
	 * @param termsByScopeRepository
	 * @param termsByCategoryRepository
	 * @param fullDimensionRepository
	 * @param fullIndicatorRepository
	 * @param fullObjectiveRepository
	 * @param incidenceRepository
	 * @param historyDAO
	 */
	@Autowired
	public TermDAO(TermRepository termRepository, FullTermRepository fullTermRepository,
			DenominationRepository denominationRepository, TermExampleRepository termExampleRepository,
			TermExclusionRepository termExclusionRepository, InformRepository informRepository,
			AsociatedIndicatorRepository asociatedIndicatorRepository,
			InformIndicatorRepository informIndicatorRepository,
			IndicatorsDimensionRepository indicatorsDimensionRepository,
			ObjectivesDimensionRepository objectivesDimensionRepository, DimensionRepository dimensionRepository,
			IndicatorRepository indicatorRepository, ObjectiveRepository objectiveRepository,
			SimpleTermsByScopeRepository simpleTermsByScopeRepository,
			SimpleTermsByCategoryRepository simpleTermsByCategoryRepository,
			SimpleIndicatorDimensionRepository simpleIndicatorDimensionRepository,
			SimpleObjectivesDimensionRepository simpleObjectivesDimensionRepository,
			SimpleAsociatedIndicatorRepository simpleAsociatedIndicatorRepository,
			SimpleInformIndicatorRepository simpleInformIndicatorRepository, TermTypeRepository termTypeRepository,
			TermsByScopeRepository termsByScopeRepository, TermsByCategoryRepository termsByCategoryRepository,
			FullDimensionRepository fullDimensionRepository, FullIndicatorRepository fullIndicatorRepository,
			FullObjectiveRepository fullObjectiveRepository, IncidenceRepository incidenceRepository,
			IHistoryDAO historyDAO, CategoryRepository categoryRepository) {
		this.termRepository = termRepository;
		this.fullTermRepository = fullTermRepository;
		this.denominationRepository = denominationRepository;
		this.termExampleRepository = termExampleRepository;
		this.termExclusionRepository = termExclusionRepository;
		this.informRepository = informRepository;
		this.asociatedIndicatorRepository = asociatedIndicatorRepository;
		this.informIndicatorRepository = informIndicatorRepository;
		this.indicatorsDimensionRepository = indicatorsDimensionRepository;
		this.objectivesDimensionRepository = objectivesDimensionRepository;
		this.dimensionRepository = dimensionRepository;
		this.indicatorRepository = indicatorRepository;
		this.objectiveRepository = objectiveRepository;
		this.simpleTermsByScopeRepository = simpleTermsByScopeRepository;
		this.simpleTermsByCategoryRepository = simpleTermsByCategoryRepository;
		this.simpleIndicatorDimensionRepository = simpleIndicatorDimensionRepository;
		this.simpleObjectivesDimensionRepository = simpleObjectivesDimensionRepository;
		this.simpleAsociatedIndicatorRepository = simpleAsociatedIndicatorRepository;
		this.simpleInformIndicatorRepository = simpleInformIndicatorRepository;
		this.termTypeRepository = termTypeRepository;
		this.termsByScopeRepository = termsByScopeRepository;
		this.termsByCategoryRepository = termsByCategoryRepository;
		this.fullDimensionRepository = fullDimensionRepository;
		this.fullIndicatorRepository = fullIndicatorRepository;
		this.fullObjectiveRepository = fullObjectiveRepository;
		this.incidenceRepository = incidenceRepository;
		this.historyDAO = historyDAO;
		this.categoryRepository = categoryRepository;
	}

	@Override
	@Transactional
	public Integer createTermTransaction(CreationTerm ct) {

		// Se guarda la parte común del término
		ct.getMainTerm().setLastUpdate(new Timestamp((new Date()).getTime()));
		Term savedTerm = this.termRepository.save(ct.getMainTerm());
		Integer idTerm = savedTerm.getIdTerm();

		// Relaciones con categorías y ámbitos
		if (ct.getIdScope() != null)
			this.simpleTermsByScopeRepository.save(new SimpleTermsByScope(ct.getIdScope(), idTerm)); // ámbito

		if (ct.getCategories() != null && ct.getCategories().size() > 0) {
			ArrayList<SimpleTermsByCategory> stbc = new ArrayList<>();
			for (Integer cat : ct.getCategories()) {
				stbc.add(new SimpleTermsByCategory(idTerm, cat));
				Category category = this.categoryRepository.findOne(cat);
				if (category.getType() == 0) { // Cambio a tipo de categoría 2
					category.setType(2);
					this.categoryRepository.save(category);
				}
			}
			this.simpleTermsByCategoryRepository.save(stbc); // categorías
		}

		// Partes comunes adicionales
		if (ct.getDenominations() != null && ct.getDenominations().size() > 0) {
			for (Denomination denomination : ct.getDenominations()) {
				denomination.setIdTerm(idTerm);
				// TODO: buscar término con el id de la denominación
			}
			this.denominationRepository.save(ct.getDenominations()); // Denominaciones
		}
		/* Ejemplos */
		if (ct.getExamples() != null && ct.getExamples().size() > 0) {
			ArrayList<TermExample> texpm = new ArrayList<>();
			for (String ex : ct.getExamples()) {
				texpm.add(new TermExample(idTerm, null, ex));
			}
			this.termExampleRepository.save(texpm);
		}

		/* Exclusiones */
		if (ct.getExclusions() != null && ct.getExclusions().size() > 0) {
			ArrayList<TermExclusion> texc = new ArrayList<>();
			for (String ex : ct.getExclusions()) {
				texc.add(new TermExclusion(idTerm, null, ex));
			}
			this.termExclusionRepository.save(texc);
		}

		int termType = ct.getMainTerm().getType().getType(); // Tipo de término

		if (termType == 2) { // DIMENSIÓN
			this.dimensionRepository
					.save(new Dimension(idTerm, ct.getOrigin(), ct.getOriginCreation(), ct.getOriginUpdate(),
							ct.getPeriod(), ct.getApplicative(), ct.getApplicativeDenomination(), ct.getHierarchy()));

		} else if (termType == 3) { // INDICADOR
			this.indicatorRepository.save(new Indicator(idTerm, ct.getOrigin(), ct.getOriginCreation(),
					ct.getOriginUpdate(), ct.getPeriod(), ct.getApplicative(), ct.getApplicativeDenomination(),
					ct.getFunctionalInterpretation(), ct.getFormula(), ct.getGranularity()));

			if (ct.getIndicatorsDimensions() != null && ct.getIndicatorsDimensions().size() > 0) {
				ArrayList<SimpleIndicatorDimension> sid = new ArrayList<>();
				for (Integer dimension : ct.getIndicatorsDimensions()) {
					sid.add(new SimpleIndicatorDimension(idTerm, dimension));
				}
				this.simpleIndicatorDimensionRepository.save(sid);
			}

		} else if (termType == 4) { // OBJETIVO
			this.objectiveRepository.save(new Objective(idTerm, ct.getOrigin(), ct.getOriginCreation(),
					ct.getOriginUpdate(), ct.getPeriod(), ct.getApplicative(), ct.getApplicativeDenomination(),
					ct.getFunctionalInterpretation(), ct.getFormula(), ct.getGranularity()));

			if (ct.getAsociatedIndicators() != null && ct.getAsociatedIndicators().size() > 0) {
				ArrayList<SimpleAsociatedIndicator> sai = new ArrayList<>();
				for (Integer indicator : ct.getAsociatedIndicators()) {
					sai.add(new SimpleAsociatedIndicator(idTerm, indicator));
				}
				this.simpleAsociatedIndicatorRepository.save(sai);
			}

			if (ct.getObjectivesDimensions() != null && ct.getObjectivesDimensions().size() > 0) {
				for (SimpleObjectivesDimension dimension : ct.getObjectivesDimensions()) {
					dimension.setObjective(idTerm);
				}
				this.simpleObjectivesDimensionRepository.save(ct.getObjectivesDimensions());
			}

		} else if (termType == 5) { // INFORME
			this.informRepository.save(new Inform(idTerm, ct.getLink()));

			if (ct.getInformIndicators() != null && ct.getInformIndicators().size() > 0) {
				ArrayList<SimpleInformIndicator> sii = new ArrayList<>();
				for (Integer indicator : ct.getInformIndicators()) {
					sii.add(new SimpleInformIndicator(idTerm, indicator));
				}
				this.simpleInformIndicatorRepository.save(sii);
			}

		} else if (termType < 1 || termType > 6) { // NO_RECONOCIDO
			throw new InternalServerException();
		}

		return idTerm;
	}

	@Override
	@Transactional
	public void createTermTransaction(List<CreationTerm> ct) {
		for (CreationTerm term : ct) {
			this.createTermTransaction(term);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public FullTerm findTermById(Integer idTerm) {
		return this.fullTermRepository.findOne(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> getExamplesFromTerm(Integer idTerm, Pageable page) {
		return this.termExampleRepository.findAllByIdTerm(idTerm, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<TermExclusion> getExclusionsFromTerm(Integer idTerm, Pageable page) {
		return this.termExclusionRepository.findByIdTerm(idTerm, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Denomination> getDenominationsFromTerm(Integer idTerm, Pageable page) {
		return this.denominationRepository.findByIdTerm(idTerm, page);
	}

	@Override
	@Transactional
	public boolean updateTerm(CreationTerm term) {

		Integer idTerm = term.getMainTerm().getIdTerm();
		Term result = this.termRepository.findOne(idTerm);
		if (result == null)
			return false;
		
		// Insertar versión actual en histórico
		Integer idVersion = this.historyDAO.addTermHistoryVersion(result);
		if (idVersion == null)
			throw new InternalServerException();
		
		// Actualizar parte común del término
		term.getMainTerm().setLastUpdate(new Timestamp((new Date()).getTime()));
		this.termRepository.save(term.getMainTerm());
		
		// Ámbito
		TermsByScope tbs = this.termsByScopeRepository.findTermScope(idTerm);
		// System.out.println(tbs.getScope().toString());
		// System.out.println(term.getIdScope().toString());
		if (term.getIdScope() == null && tbs != null) {
			this.simpleTermsByScopeRepository.delete(new SimpleTermsByScope(tbs.getScope().getIdScope(), idTerm));
		} else if (term.getIdScope() != null
				&& (tbs == null || (tbs != null && !tbs.getScope().getIdScope().equals(term.getIdScope())))) {
			this.simpleTermsByScopeRepository.save(new SimpleTermsByScope(term.getIdScope(), idTerm));
		}
		
		// Categoría
		List<TermsByCategory> tbcs = this.termsByCategoryRepository.findCategoriesFromTerm(idTerm);
		for (TermsByCategory tbc : tbcs) {
			if (!term.getCategories().contains(tbc.getCategory().getIdCategory())) {
				this.simpleTermsByCategoryRepository
						.delete(new SimpleTermsByCategory(idTerm, tbc.getCategory().getIdCategory()));
				
				List<TermsByCategory> res = this.termsByCategoryRepository.findAllTermsFromCategory(idTerm);
				if (res.size() == 0) { // Si no contiene términos
					Category category = this.categoryRepository.findOne(tbc.getCategory().getIdCategory());
					if (category.getType() == 2) { // Cambio a tipo de categoría 0
						category.setType(0);
						this.categoryRepository.save(category);
					}
				}
			} else {
				term.getCategories().remove(tbc.getCategory().getIdCategory());
			}
		}
		for (Integer cat : term.getCategories()) {
			this.simpleTermsByCategoryRepository.save(new SimpleTermsByCategory(idTerm, cat));
			Category category = this.categoryRepository.findOne(cat);
			if (category.getType() == 0) { // Cambio a tipo de categoría 2
				category.setType(2);
				this.categoryRepository.save(category);
			}
		}
		
		// Denominaciones
		List<Denomination> denominations = this.denominationRepository.findAllByIdTerm(idTerm);
		for (Denomination denomination : denominations) {
			if (!term.getDenominations().contains(denomination)) {
				this.denominationRepository.delete(denomination);
			} else {
				term.getDenominations().remove(denomination);
			}
		}
		
		// TODO: buscar término con el id de la denominación
		this.denominationRepository.save(term.getDenominations());
		// Ejemplos
		List<TermExample> examples = this.termExampleRepository.findAllByIdTerm(idTerm);
		for (TermExample example : examples) {
			if (!term.getExamples().contains(example.getExample())) {
				this.termExampleRepository.delete(example);
			} else {
				term.getExamples().remove(example.getExample());
			}
		}
		for (String ex : term.getExamples()) {
			this.termExampleRepository.save(new TermExample(idTerm, null, ex));
		}
		
		// Exclusiones
		List<TermExclusion> exclusions = this.termExclusionRepository.findAllByIdTerm(idTerm);
		for (TermExclusion exclusion : exclusions) {
			if (!term.getExclusions().contains(exclusion.getExclusion())) {
				this.termExclusionRepository.delete(exclusion);
			} else {
				term.getExclusions().remove(exclusion.getExclusion());
			}
		}
		for (String ex : term.getExclusions()) {
			this.termExclusionRepository.save(new TermExclusion(idTerm, null, ex));
		}
		
		Integer termType = term.getMainTerm().getType().getType();
		/**
		 * Si hay un cambio de tipo de término se eliminan las caraterísticas
		 * del término anterior
		 */
		if (!termType.equals(result.getType().getType())) {
			Integer oldTermType = result.getType().getType();
			if (oldTermType == 2) {
				this.dimensionRepository.delete(idTerm);
			} else if (oldTermType == 3) {
				this.indicatorRepository.delete(idTerm);
			} else if (oldTermType == 4) {
				this.objectiveRepository.delete(idTerm);
			} else if (oldTermType == 5) {
				this.informRepository.delete(idTerm);
			}
		}
		
		/**
		 * Insertar las caraterísticas de los tipos de términos
		 */
		if (termType == 2) { // DIMENSIÓN
			this.dimensionRepository.save(new Dimension(idTerm, term.getOrigin(), term.getOriginCreation(),
					term.getOriginUpdate(), term.getPeriod(), term.getApplicative(), term.getApplicativeDenomination(),
					term.getHierarchy()));

		} else if (termType == 3) { // INDICADOR
			this.indicatorRepository.save(new Indicator(idTerm, term.getOrigin(), term.getOriginCreation(),
					term.getOriginUpdate(), term.getPeriod(), term.getApplicative(), term.getApplicativeDenomination(),
					term.getFunctionalInterpretation(), term.getFormula(), term.getGranularity()));

			// Dimensiones del indicador
			if (term.getIndicatorsDimensions() == null) {
				term.setIndicatorsDimensions(new ArrayList<Integer>());
			}
			List<IndicatorsDimension> ids = this.indicatorsDimensionRepository.findAllByIndicator(idTerm);
			for (IndicatorsDimension id : ids) {
				if (!term.getIndicatorsDimensions().contains(id.getDimension().getIdTerm())) {
					this.indicatorsDimensionRepository.delete(id);
				} else {
					term.getIndicatorsDimensions().remove(id.getDimension().getIdTerm());
				}
			}
			for (Integer dimension : term.getIndicatorsDimensions()) {
				this.simpleIndicatorDimensionRepository.save(new SimpleIndicatorDimension(idTerm, dimension));
			}
			
		} else if (termType == 4) { // OBJETIVO
			this.objectiveRepository.save(new Objective(idTerm, term.getOrigin(), term.getOriginCreation(),
					term.getOriginUpdate(), term.getPeriod(), term.getApplicative(), term.getApplicativeDenomination(),
					term.getFunctionalInterpretation(), term.getFormula(), term.getGranularity()));

			// Indicadores asociados
			if (term.getAsociatedIndicators() == null)
				term.setAsociatedIndicators(new ArrayList<Integer>());
			List<AsociatedIndicator> ais = this.asociatedIndicatorRepository.findAllByObjective(idTerm);
			for (AsociatedIndicator ai : ais) {
				if (!term.getAsociatedIndicators().contains(ai.getIndicator().getIdTerm())) {
					this.asociatedIndicatorRepository.delete(ai);
				} else {
					term.getAsociatedIndicators().remove(ai.getIndicator().getIdTerm());
				}
			}
			for (Integer indicator : term.getAsociatedIndicators()) {
				this.simpleAsociatedIndicatorRepository.save(new SimpleAsociatedIndicator(idTerm, indicator));
			}

			// Dimensiones del objetivo
			if (term.getObjectivesDimensions() == null)
				term.setObjectivesDimensions(new ArrayList<SimpleObjectivesDimension>());
			List<ObjectivesDimension> ods = this.objectivesDimensionRepository.findAllByObjective(idTerm);
			for (ObjectivesDimension od : ods) {
				if (!term.getObjectivesDimensions().contains(new SimpleObjectivesDimension(od.getObjective(),
						od.getDimension().getIdTerm(), od.isAdditivity()))) {
					this.objectivesDimensionRepository.delete(od);
				} else {
					term.getObjectivesDimensions().remove(od);
				}
			}
			this.simpleObjectivesDimensionRepository.save(term.getObjectivesDimensions());

		} else if (termType == 5) { // INFORME
			this.informRepository.save(new Inform(idTerm, term.getLink()));

			// Indicadores del informe
			if (term.getInformIndicators() == null)
				term.setInformIndicators(new ArrayList<Integer>());
			List<InformIndicator> iis = this.informIndicatorRepository.findAllByInform(idTerm);
			for (InformIndicator ii : iis) {
				if (!term.getInformIndicators().contains(ii.getIndicator().getIdTerm())) {
					this.informIndicatorRepository.delete(ii);
				} else {
					term.getInformIndicators().remove(ii.getIndicator().getIdTerm());
				}
			}
			for (Integer indicator : term.getInformIndicators()) {
				this.simpleInformIndicatorRepository.save(new SimpleInformIndicator(idTerm, indicator));
			}

		} else if (termType < 1 || termType > 6) { // NO_RECONOCIDO
			throw new InternalServerException();
		}
		
		return true;
	}

	@Override
	@Transactional
	public void removeTerm(Integer idTerm) {
		this.termRepository.delete(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<TermType> findAllTermTypes() {
		return this.termTypeRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public TermsByScope getScopeFromTerm(Integer idTerm) {
		return this.termsByScopeRepository.findTermScope(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TermsByCategory> getCategoriesFromTerm(Integer idTerm) {
		return this.termsByCategoryRepository.findCategoriesFromTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public FullDimension getDimensionFromTerm(Integer idTerm) {
		return this.fullDimensionRepository.findOneByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public FullIndicator getIndicatorFromTerm(Integer idTerm) {
		return this.fullIndicatorRepository.findOneByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public FullObjective getObjectiveFromTerm(Integer idTerm) {
		return this.fullObjectiveRepository.findOneByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public Inform getInformFromTerm(Integer idTerm) {
		return this.informRepository.findOneByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Incidence> getIncidencesFromTerm(Integer idTerm, Pageable page) {
		return this.incidenceRepository.findByIdTerm(idTerm, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<AsociatedIndicator> getAsociatedIndicatorsFromTerm(Integer idObjective, Pageable page) {
		return this.asociatedIndicatorRepository.findByObjective(idObjective, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<InformIndicator> getInformIndicators(Integer idInform, Pageable page) {
		return this.informIndicatorRepository.findByInform(idInform, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<IndicatorsDimension> getDimensionIndicators(Integer idIndicator, Pageable page) {
		return this.indicatorsDimensionRepository.findByIndicator(idIndicator, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ObjectivesDimension> getObjectiveDimensions(Integer idObjective, Pageable page) {
		return this.objectivesDimensionRepository.findByObjective(idObjective, page);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Term> findAllTermsByType(Integer type) {
		return this.termRepository.findAllByTypeType(type);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Denomination> getAllDenominationsFromTerm(Integer idTerm) {
		return this.denominationRepository.findAllByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TermExample> getAllExamplesFromTerm(Integer idTerm) {
		return this.termExampleRepository.findAllByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TermExclusion> getAllExclusionsFromTerm(Integer idTerm) {
		return this.termExclusionRepository.findAllByIdTerm(idTerm);
	}

	@Override
	@Transactional(readOnly = true)
	public List<AsociatedIndicator> getAllAsociatedIndicatorsFromTerm(Integer idObjective) {
		return this.asociatedIndicatorRepository.findAllByObjective(idObjective);
	}

	@Override
	@Transactional(readOnly = true)
	public List<InformIndicator> getAllInformIndicators(Integer idInform) {
		return this.informIndicatorRepository.findAllByInform(idInform);
	}

	@Override
	@Transactional(readOnly = true)
	public List<IndicatorsDimension> getAllDimensionIndicators(Integer idIndicator) {
		return this.indicatorsDimensionRepository.findAllByIndicator(idIndicator);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ObjectivesDimension> getAllObjectiveDimensions(Integer idObjective) {
		return this.objectivesDimensionRepository.findAllByObjective(idObjective);
	}

	@Override
	@Transactional
	public TermType saveTermType(TermType type) {
		return this.termTypeRepository.save(type);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullTerm> getTerms(Pageable page) {
		return this.fullTermRepository.findAll(page);
	}
}
