package com.backend.metaDW.model.vo.term;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@IdClass(TermExamplePK.class)
@Table(name = "termexamples", schema = "public")
public class TermExample {

	@Id
	@SequenceGenerator(name = "termexamples_id_example_seq", sequenceName = "termexamples_id_example_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "termexamples_id_example_seq")
	@Column(name = "id_example", updatable = false)
	private Integer idExample;

	@Id
	@Column(name = "id_term")
	private Integer idTerm;

	@Column(name = "example")
	private String example;

	/**
	 * void
	 */
	public TermExample() {
	}

	/**
	 * @param idExample
	 * @param idTerm
	 */
	public TermExample(Integer idExample, Integer idTerm) {
		this.idExample = idExample;
		this.idTerm = idTerm;
	}

	/**
	 * @param idTerm
	 * @param example
	 * @param idExample
	 */
	public TermExample(Integer idTerm, Integer idExample, String example) {
		this.idTerm = idTerm;
		this.idExample = idExample;
		this.example = example;
	}

	public Integer getIdExample() {
		return idExample;
	}

	public void setIdExample(Integer idExample) {
		this.idExample = idExample;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idExample == null) ? 0 : idExample.hashCode());
		result = prime * result + ((example == null) ? 0 : example.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermExample other = (TermExample) obj;
		if (idExample == null) {
			if (other.idExample != null)
				return false;
		} else if (!idExample.equals(other.idExample))
			return false;
		if (example == null) {
			if (other.example != null)
				return false;
		} else if (!example.equals(other.example))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermExample [id_term=" + idTerm + ", denomination=" + idExample + ", id_denomination=" + example + "]";
	}
}

@SuppressWarnings("serial")
class TermExamplePK implements Serializable {

	private Integer idExample;
	private Integer idTerm;

	public Integer getIdExample() {
		return idExample;
	}

	public void setIdExample(Integer idExample) {
		this.idExample = idExample;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}
}
