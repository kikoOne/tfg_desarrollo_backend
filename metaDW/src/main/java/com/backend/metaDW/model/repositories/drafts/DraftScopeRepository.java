package com.backend.metaDW.model.repositories.drafts;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.drafts.DraftScope;

public interface DraftScopeRepository extends PagingAndSortingRepository<DraftScope, Integer>{

	public Page<DraftScope> findAllByOwner(Integer owner, Pageable page);	
}
