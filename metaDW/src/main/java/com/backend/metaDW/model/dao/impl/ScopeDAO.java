package com.backend.metaDW.model.dao.impl;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.IHistoryDAO;
import com.backend.metaDW.model.dao.IScopeDAO;
import com.backend.metaDW.model.repositories.scopes.FullScopeRepository;
import com.backend.metaDW.model.repositories.scopes.ScopeRepository;
import com.backend.metaDW.model.repositories.scopes.TermsByScopeRepository;
import com.backend.metaDW.model.vo.scope.FullScope;
import com.backend.metaDW.model.vo.scope.Scope;

@Service
public class ScopeDAO implements IScopeDAO {

	@Autowired
	private FullScopeRepository fullScopeRepository;
	@Autowired
	private ScopeRepository scopeRepository;
	@Autowired
	private TermsByScopeRepository termsByScopeRepository;
	@Autowired
	private IHistoryDAO historyDAO;

	@Override
	@Transactional
	public Integer saveScope(Scope scope) {
		Scope scopeSaved = this.scopeRepository.save(scope);
		if (scopeSaved != null)
			return scopeSaved.getIdScope();
		return 0;
	}

	@Override
	@Transactional
	public void saveScope(List<Scope> scopes) {
		for (Scope scope : scopes) {
			this.saveScope(scope);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullScope> findAllScopes(Pageable page) {
		return this.fullScopeRepository.findAll(page);
	}

	@Override
	@Transactional(readOnly = true)
	public FullScope findScopeById(Integer idScope) {
		return this.fullScopeRepository.findOne(idScope);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findTermsFromScope(Integer idScope, Pageable page) {
		return this.termsByScopeRepository.findTermsFromScope(idScope, page);
	}

	@Override
	@Transactional
	public boolean updateScopeData(Scope newScope) {

		Scope scope = this.scopeRepository.findOne(newScope.getIdScope());
		if (scope == null)
			return false;

		Integer idVersion = this.historyDAO.addScopeHistoryVersion(scope);
		if (idVersion == null)
			return false;

		if (scope.getVersion().equals(newScope.getVersion())) {
			String version = newScope.getVersion();
			String[] temp = (version.split("."));
			Integer next = Integer.parseInt(temp[1]) + 1;
			newScope.setVersion(temp[0] + "." + next.toString());
		}

		newScope.setLastUpdate(new Timestamp((new Date()).getTime()));

		Scope updatedScope = this.scopeRepository.save(newScope);
		if (updatedScope == null)
			return false;
		return true;
	}

	@Override
	@Transactional
	public void removeScope(Integer idScope) {
		this.scopeRepository.delete(idScope);
	}

	@Override
	public List<Scope> findAllScopes() {
		return (List<Scope>) this.scopeRepository.findAll();
	}

}
