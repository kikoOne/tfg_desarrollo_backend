package com.backend.metaDW.model.vo.drafts;

import java.sql.Timestamp;
import javax.persistence.*;

@Entity
@Table(name = "draftscopes", schema = "public")
public class DraftScope {
	
	@Id
	@SequenceGenerator(name = "draftscopes_id_seq", sequenceName = "draftscopes_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "draftscopes_id_seq")
	@Column(name = "id", updatable = false)
	private Integer id;
	
	@Column(name = "id_scope")
	private Integer idScope;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "definition")
	private String definition;
	
	@Column(name = "version")
	private String version;
	
	@Column(name = "owner")
	private Integer owner;
	
	@Column(name = "creation", insertable = false)
	private Timestamp creation;
	
	public DraftScope() {
	}
	
	public DraftScope(Integer idScope) {
		this.idScope = idScope;
	}

	/**
	 * @param id
	 * @param idScope
	 * @param name
	 * @param definition
	 * @param version
	 * @param owner
	 * @param creation
	 */
	public DraftScope(Integer id, Integer idScope, String name, String definition, String version, Integer owner,
			Timestamp creation) {
		this.id = id;
		this.idScope = idScope;
		this.name = name;
		this.definition = definition;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdScope() {
		return idScope;
	}

	public void setIdScope(Integer idScope) {
		this.idScope = idScope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idScope == null) ? 0 : idScope.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DraftScope other = (DraftScope) obj;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idScope == null) {
			if (other.idScope != null)
				return false;
		} else if (!idScope.equals(other.idScope))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoryScope [id=" + id + ", idScope=" + idScope + ", name=" + name + ", definition=" + definition
				+ ", version=" + version + ", owner=" + owner + ", creation=" + creation + "]";
	}
}
