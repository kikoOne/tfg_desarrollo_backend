package com.backend.metaDW.model.repositories.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.user.User;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

	public Page<User> findByNameContainingIgnoreCase(String name, Pageable page);

	public User findByUserNameAndPassword(String userName, String password);
	
	public User findByUserName(String userName);
}
