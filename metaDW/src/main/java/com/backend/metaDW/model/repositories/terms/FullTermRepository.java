package com.backend.metaDW.model.repositories.terms;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.FullTerm;

public interface FullTermRepository extends PagingAndSortingRepository<FullTerm, Integer> {

	Page<FullTerm> findByNameContainingIgnoreCase(String name, Pageable page);

	Page<FullTerm> findByNameContainingIgnoreCaseAndTypeTypeIn(String name, List<Integer> type, Pageable page);

	Page<FullTerm> findByTypeTypeIn(List<Integer> type, Pageable page);	
}
