package com.backend.metaDW.model.vo.user.security;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(UserTokenPK.class)
@Table(name = "usertokens", schema = "public")
public class UserToken {

	@Id
	@Column(name = "id_user")
	private Integer idUser;

	@Id
	@Column(name = "token")
	private String token;

	@Column(name = "device")
	private String device;

	/**
	 * 
	 */
	public UserToken() {
	}

	/**
	 * @param idUser
	 * @param token
	 */
	public UserToken(Integer idUser, String token) {
		this.idUser = idUser;
		this.token = token;
	}

	/**
	 * @param idUser
	 * @param token
	 * @param device
	 */
	public UserToken(Integer idUser, String token, String device) {
		this.idUser = idUser;
		this.token = token;
		this.device = device;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((device == null) ? 0 : device.hashCode());
		result = prime * result + ((idUser == null) ? 0 : idUser.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserToken other = (UserToken) obj;
		if (device == null) {
			if (other.device != null)
				return false;
		} else if (!device.equals(other.device))
			return false;
		if (idUser == null) {
			if (other.idUser != null)
				return false;
		} else if (!idUser.equals(other.idUser))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserToken [idUser=" + idUser + ", token=" + token + ", device=" + device + "]";
	}
}

class UserTokenPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -981094233585136753L;
	
	private Integer idUser;
	private String token;

	/**
	 * 
	 */
	public UserTokenPK() {
	}

	/**
	 * @param idUser
	 * @param token
	 */
	public UserTokenPK(Integer idUser, String token) {
		this.idUser = idUser;
		this.token = token;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
