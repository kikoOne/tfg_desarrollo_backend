package com.backend.metaDW.model.dao;



import java.util.List;

import com.backend.metaDW.model.vo.user.security.FullInternalUser;
import com.backend.metaDW.model.vo.user.security.Role;
import com.backend.metaDW.security.jwt.JWT;

public interface ISecurityDAO {

	public FullInternalUser authenticateTransaction(String userName, String password);

	public void saveToken(Integer idUser, String token, String device);

	public boolean isTokenRecorded(String token);

	public boolean removeToken(JWT tokenR);
	
	public List<Role> getAllRoles();
}
