package com.backend.metaDW.model.vo.term;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.user.User;

@Entity
@Table(name = "Terms", schema = "public")
public class FullTerm {

	@Id
	@SequenceGenerator(name = "terms_id_term_seq", sequenceName = "terms_id_term_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "terms_id_term_seq")
	@Column(name = "id_term", updatable = false)
	private Integer idTerm;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "short_name")
	private String shortName;
	
	@Column(name = "definition")
	private String definition;
	
	@Column(name = "extended_definition")
	private String extendedDefinition;
	
	@Column(name = "state")
	private int state;
	
	@Column(name = "version")
	private String version;
	
	@ManyToOne
    @JoinColumn(name="owner")
	private User owner;
	
	@Column(name = "creation")
	private Timestamp creation;
	
	@Column(name = "last_update")
	private Timestamp lastUpdate;
	
	@ManyToOne
    @JoinColumn(name="type")
	private TermType type;

	/**
	 * 
	 */
	public FullTerm() {
	}

	/**
	 * @param idTerm
	 */
	public FullTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	/**
	 * @param idTerm
	 * @param name
	 * @param shortName
	 * @param definition
	 * @param extendedDefinition
	 * @param state
	 * @param version
	 * @param owner
	 * @param creation
	 * @param lastUpdate
	 * @param type
	 */
	public FullTerm(Integer idTerm, String name, String shortName, String definition, String extendedDefinition, int state,
			String version, User owner, Timestamp creation, Timestamp lastUpdate, TermType type) {
		this.idTerm = idTerm;
		this.name = name;
		this.shortName = shortName;
		this.definition = definition;
		this.extendedDefinition = extendedDefinition;
		this.state = state;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
		this.lastUpdate = lastUpdate;
		this.type = type;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public String getExtendedDefinition() {
		return extendedDefinition;
	}

	public void setExtendedDefinition(String extendedDefinition) {
		this.extendedDefinition = extendedDefinition;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public TermType getType() {
		return type;
	}

	public void setType(TermType type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((extendedDefinition == null) ? 0 : extendedDefinition.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		result = prime * result + ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((shortName == null) ? 0 : shortName.hashCode());
		result = prime * result + state;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FullTerm other = (FullTerm) obj;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (extendedDefinition == null) {
			if (other.extendedDefinition != null)
				return false;
		} else if (!extendedDefinition.equals(other.extendedDefinition))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		if (lastUpdate == null) {
			if (other.lastUpdate != null)
				return false;
		} else if (!lastUpdate.equals(other.lastUpdate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (shortName == null) {
			if (other.shortName != null)
				return false;
		} else if (!shortName.equals(other.shortName))
			return false;
		if (state != other.state)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Term [idTerm=" + idTerm + ", name=" + name + ", shortName=" + shortName + ", definition=" + definition
				+ ", extendedDefinition=" + extendedDefinition + ", state=" + state + ", version=" + version
				+ ", owner=" + owner + ", creation=" + creation + ", lastUpdate=" + lastUpdate + ", type=" + type + "]";
	}	
}
