package com.backend.metaDW.model.repositories.scopes;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.scope.TermsByScope;
import com.backend.metaDW.model.vo.term.FullTerm;

public interface TermsByScopeRepository extends PagingAndSortingRepository<TermsByScope, Integer> {

	@Query("SELECT tbs.term FROM TermsByScope tbs WHERE tbs.scope.idScope = :idScope")
	public Page<?> findTermsFromScope(@Param("idScope") Integer idScope, Pageable page);

	@Query("SELECT tbs FROM TermsByScope tbs WHERE id_term = :idTerm")
	public TermsByScope findTermScope(@Param("idTerm") Integer idTerm);

	@Query("SELECT tbs.term FROM TermsByScope tbs WHERE tbs.scope.idScope = :idScope AND LOWER(tbs.term.name) LIKE CONCAT('%',LOWER(:name),'%') AND tbs.term.type.type IN (:types)")
	public Page<?> findTermsByNameAndTypeFromScope(@Param("idScope") Integer idScope, @Param("name") String name,
			@Param("types") List<Integer> types, Pageable page);

	public Scope findOneByTerm(FullTerm idTerm);
}
