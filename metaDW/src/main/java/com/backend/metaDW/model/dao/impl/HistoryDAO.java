package com.backend.metaDW.model.dao.impl;

import java.util.List;
import java.util.StringJoiner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.IHistoryDAO;
import com.backend.metaDW.model.repositories.categories.FullSubcategoryRepository;
import com.backend.metaDW.model.repositories.categories.TermsByCategoryRepository;
import com.backend.metaDW.model.repositories.history.HistoryCategoryRepository;
import com.backend.metaDW.model.repositories.history.HistoryDimensionRepository;
import com.backend.metaDW.model.repositories.history.HistoryIndicatorRepository;
import com.backend.metaDW.model.repositories.history.HistoryInformRepository;
import com.backend.metaDW.model.repositories.history.HistoryObjectiveRepository;
import com.backend.metaDW.model.repositories.history.HistoryScopeRepository;
import com.backend.metaDW.model.repositories.history.HistoryTermRepository;
import com.backend.metaDW.model.repositories.scopes.TermsByScopeRepository;
import com.backend.metaDW.model.repositories.terms.DenominationRepository;
import com.backend.metaDW.model.repositories.terms.TermExampleRepository;
import com.backend.metaDW.model.repositories.terms.TermExclusionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.AsociatedIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.DimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.IndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.IndicatorsDimensionRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.InformIndicatorRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.InformRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.ObjectiveRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.ObjectivesDimensionRepository;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.TermsByCategory;
import com.backend.metaDW.model.vo.history.HistoryCategory;
import com.backend.metaDW.model.vo.history.HistoryDimension;
import com.backend.metaDW.model.vo.history.HistoryIndicator;
import com.backend.metaDW.model.vo.history.HistoryInform;
import com.backend.metaDW.model.vo.history.HistoryObjective;
import com.backend.metaDW.model.vo.history.HistoryScope;
import com.backend.metaDW.model.vo.history.HistoryTerm;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.scope.TermsByScope;
import com.backend.metaDW.model.vo.term.Denomination;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.TermExample;
import com.backend.metaDW.model.vo.term.TermExclusion;
import com.backend.metaDW.model.vo.term.hierarchy.AsociatedIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.Dimension;
import com.backend.metaDW.model.vo.term.hierarchy.Indicator;
import com.backend.metaDW.model.vo.term.hierarchy.IndicatorsDimension;
import com.backend.metaDW.model.vo.term.hierarchy.Inform;
import com.backend.metaDW.model.vo.term.hierarchy.InformIndicator;
import com.backend.metaDW.model.vo.term.hierarchy.Objective;
import com.backend.metaDW.model.vo.term.hierarchy.ObjectivesDimension;
import com.backend.metaDW.model.vo.user.User;

@Service
public class HistoryDAO implements IHistoryDAO {

	private HistoryScopeRepository historyScopeRepository;
	private HistoryCategoryRepository historyCategoryRepository;
	private HistoryTermRepository historyTermRepository;
	private HistoryDimensionRepository historyDimensionRepository;
	private HistoryIndicatorRepository historyIndicatorRepository;
	private HistoryInformRepository historyInformRepository;
	private HistoryObjectiveRepository historyObjectiveRepository;

	private FullSubcategoryRepository fullSubcategoryRepository;
	private TermsByScopeRepository termsByScopeRepository;
	private TermsByCategoryRepository termsByCategoryRepository;
	private DenominationRepository denominationRepository;
	private TermExampleRepository termExampleRepository;
	private TermExclusionRepository termExclusionRepository;
	private DimensionRepository dimensionRepository;
	private IndicatorRepository indicatorRepository;
	private IndicatorsDimensionRepository indicatorsDimensionRepository;
	private ObjectiveRepository objectiveRepository;
	private AsociatedIndicatorRepository asociatedIndicatorRepository;
	private ObjectivesDimensionRepository objectivesDimensionRepository;
	private InformRepository informRepository;
	private InformIndicatorRepository informIndicatorRepository;

	/**
	 * @param historyScopeRepository
	 * @param historyCategoryRepository
	 * @param historyTermRepository
	 * @param historyDimensionRepository
	 * @param historyIndicatorRepository
	 * @param historyInformRepository
	 * @param historyObjectiveRepository
	 * @param fullSubcategoryRepository
	 * @param termsByScopeRepository
	 * @param termsByCategoryRepository
	 * @param denominationRepository
	 * @param termExampleRepository
	 * @param termExclusionRepository
	 * @param dimensionRepository
	 * @param indicatorRepository
	 * @param indicatorsDimensionRepository
	 * @param objectiveRepository
	 * @param asociatedIndicatorRepository
	 * @param objectivesDimensionRepository
	 * @param informRepository
	 * @param informIndicatorRepository
	 */
	@Autowired
	public HistoryDAO(HistoryScopeRepository historyScopeRepository,
			HistoryCategoryRepository historyCategoryRepository, HistoryTermRepository historyTermRepository,
			HistoryDimensionRepository historyDimensionRepository,
			HistoryIndicatorRepository historyIndicatorRepository, HistoryInformRepository historyInformRepository,
			HistoryObjectiveRepository historyObjectiveRepository, FullSubcategoryRepository fullSubcategoryRepository,
			TermsByScopeRepository termsByScopeRepository, TermsByCategoryRepository termsByCategoryRepository,
			DenominationRepository denominationRepository, TermExampleRepository termExampleRepository,
			TermExclusionRepository termExclusionRepository, DimensionRepository dimensionRepository,
			IndicatorRepository indicatorRepository, IndicatorsDimensionRepository indicatorsDimensionRepository,
			ObjectiveRepository objectiveRepository, AsociatedIndicatorRepository asociatedIndicatorRepository,
			ObjectivesDimensionRepository objectivesDimensionRepository, InformRepository informRepository,
			InformIndicatorRepository informIndicatorRepository) {
		this.historyScopeRepository = historyScopeRepository;
		this.historyCategoryRepository = historyCategoryRepository;
		this.historyTermRepository = historyTermRepository;
		this.historyDimensionRepository = historyDimensionRepository;
		this.historyIndicatorRepository = historyIndicatorRepository;
		this.historyInformRepository = historyInformRepository;
		this.historyObjectiveRepository = historyObjectiveRepository;
		this.fullSubcategoryRepository = fullSubcategoryRepository;
		this.termsByScopeRepository = termsByScopeRepository;
		this.termsByCategoryRepository = termsByCategoryRepository;
		this.denominationRepository = denominationRepository;
		this.termExampleRepository = termExampleRepository;
		this.termExclusionRepository = termExclusionRepository;
		this.dimensionRepository = dimensionRepository;
		this.indicatorRepository = indicatorRepository;
		this.indicatorsDimensionRepository = indicatorsDimensionRepository;
		this.objectiveRepository = objectiveRepository;
		this.asociatedIndicatorRepository = asociatedIndicatorRepository;
		this.objectivesDimensionRepository = objectivesDimensionRepository;
		this.informRepository = informRepository;
		this.informIndicatorRepository = informIndicatorRepository;
	}

	@Override
	@Transactional
	public Integer addScopeHistoryVersion(Scope scope) {
		HistoryScope scopeVersion = new HistoryScope();
		scopeVersion.setIdScope(scope.getIdScope());
		scopeVersion.setName(scope.getName());
		scopeVersion.setCreation(scope.getCreation());
		scopeVersion.setLastUpdate(scope.getLastUpdate());
		scopeVersion.setDefinition(scope.getDefinition());
		scopeVersion.setVersion(scope.getVersion());
		scopeVersion.setOwner(new User(scope.getOwner()));

		HistoryScope result = this.historyScopeRepository.save(scopeVersion);
		if (result != null)
			return result.getId();
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<HistoryScope> getHistoryFromScope(Integer idScope, Pageable page) {
		return this.historyScopeRepository.findAllByIdScope(idScope, page);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryScope getScopeHistoryVersion(Integer idScope, Integer idVersion) {
		return this.historyScopeRepository.findByIdScopeAndId(idScope, idVersion);
	}

	@Override
	@Transactional
	public Integer addCategoryHistoryVersion(Category category) {
		HistoryCategory categoryVersion = new HistoryCategory();
		categoryVersion.setIdCategory(category.getIdCategory());
		categoryVersion.setName(category.getName());
		categoryVersion.setCreation(category.getCreation());
		categoryVersion.setLastUpdate(category.getLastUpdate());
		categoryVersion.setDefinition(category.getDefinition());
		if (this.fullSubcategoryRepository.findParentCategory(category.getIdCategory()) != null
				&& this.fullSubcategoryRepository.findParentCategory(category.getIdCategory()).size() > 0) {
			Category parent = (Category) this.fullSubcategoryRepository.findParentCategory(category.getIdCategory())
					.get(0);
			// Category parent =
			// this.fullSubcategoryRepository.findParentCategory(category.getIdCategory()).getParent();
			categoryVersion.setParent(parent.getIdCategory());
		}
		categoryVersion.setVersion(category.getVersion());
		categoryVersion.setOwner(new User(category.getOwner()));
		categoryVersion.setType(category.getType());

		HistoryCategory result = this.historyCategoryRepository.save(categoryVersion);
		if (result != null)
			return result.getId();
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<HistoryCategory> getHistoryFromCategory(Integer idCategory, Pageable page) {
		return this.historyCategoryRepository.findAllByIdCategory(idCategory, page);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryCategory getCategoryHistoryVersion(Integer idCategory, Integer idVersion) {
		return this.historyCategoryRepository.findByIdCategoryAndId(idCategory, idVersion);
	}

	@Override
	@Transactional
	public Integer addTermHistoryVersion(Term term) {

		Integer idTerm = term.getIdTerm();
		HistoryTerm termVersion = new HistoryTerm();
		termVersion.setIdTerm(term.getIdTerm());
		termVersion.setName(term.getName());
		termVersion.setShortName(term.getShortName());
		termVersion.setDefinition(term.getDefinition());
		termVersion.setExtendedDefinition(term.getExtendedDefinition());
		termVersion.setVersion(term.getVersion());
		termVersion.setOwner(new User(term.getOwner()));
		termVersion.setType(term.getType());
		
		termVersion.setCreation(term.getCreation());
		termVersion.setLastUpdate(term.getLastUpdate());

		// Ámbito
		TermsByScope tbs = this.termsByScopeRepository.findTermScope(idTerm);
		if (tbs != null) {
			termVersion.setScope(tbs.getScope().getIdScope());
		}

		// Categorías
		List<TermsByCategory> tbcs = this.termsByCategoryRepository.findCategoriesFromTerm(idTerm);
		if (tbcs != null && tbcs.size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (TermsByCategory tbc : tbcs) {
				joiner.add(tbc.getCategory().getIdCategory().toString());
			}
			termVersion.setCategories(joiner.toString());
		}

		// Denominaciones
		List<Denomination> denominations = this.denominationRepository.findAllByIdTerm(idTerm);
		if (denominations != null && denominations.size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (Denomination denomination : denominations) {
				joiner.add(denomination.getDenomination());
			}
			termVersion.setDenominations(joiner.toString());
		}

		// Ejemplos
		List<TermExample> examples = this.termExampleRepository.findAllByIdTerm(idTerm);
		if (examples != null && examples.size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (TermExample example : examples) {
				joiner.add(example.getExample());
			}
			termVersion.setExamples(joiner.toString());
		}

		// Exclusiones
		List<TermExclusion> exclusions = this.termExclusionRepository.findAllByIdTerm(idTerm);
		if (exclusions != null && exclusions.size() > 0) {
			StringJoiner joiner = new StringJoiner(",");
			for (TermExclusion exclusion : exclusions) {
				joiner.add(exclusion.getExclusion());
			}
			termVersion.setExclusions(joiner.toString());
		}

		HistoryTerm result = this.historyTermRepository.save(termVersion);
		Integer termType = termVersion.getType().getType();
		Integer id = result.getId();
		System.out.println("1");
		if (termType == 2) { // DIMENSIÓN
			Dimension dimension = this.dimensionRepository.findOne(idTerm);
			if (dimension != null) {
				HistoryDimension dimensionVersion = new HistoryDimension(id);
				dimensionVersion.setOrigin(dimension.getOrigin());
				dimensionVersion.setOriginCreation(dimension.getOriginCreation());
				dimensionVersion.setOriginUpdate(dimension.getOriginUpdate());
				dimensionVersion.setPeriod(dimension.getPeriod());
				dimensionVersion.setApplicative(dimension.getApplicative());
				dimensionVersion.setApplicative_denomination(dimension.getApplicative_denomination());
				dimensionVersion.setHierarchy(dimension.getHierarchy());
				this.historyDimensionRepository.save(dimensionVersion);
			}
		} else if (termType == 3) { // INDICADOR
			Indicator indicator = this.indicatorRepository.findOne(idTerm);
			HistoryIndicator indicatorVersion = new HistoryIndicator(id);
			indicatorVersion.setOrigin(indicator.getOrigin());
			indicatorVersion.setOriginCreation(indicator.getOriginCreation());
			indicatorVersion.setOriginUpdate(indicator.getOriginUpdate());
			indicatorVersion.setPeriod(indicator.getPeriod());
			indicatorVersion.setApplicative(indicator.getApplicative());
			indicatorVersion.setApplicative_denomination(indicator.getApplicative_denomination());
			indicatorVersion.setFunctionalInterpretation(indicator.getFunctionalInterpretation());
			indicatorVersion.setFormula(indicator.getFormula());
			indicatorVersion.setGranularity(indicator.getGranularity());

			// Dimensiones del indicador
			List<IndicatorsDimension> ids = this.indicatorsDimensionRepository.findAllByIndicator(idTerm);
			if (ids != null && ids.size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (IndicatorsDimension indicatorDimension : ids) {
					joiner.add(indicatorDimension.getDimension().getIdTerm().toString());
				}
				indicatorVersion.setDimensions(joiner.toString());
			}
			System.out.println("2");
			this.historyIndicatorRepository.save(indicatorVersion);
		} else if (termType == 4) { // OBJETIVO
			Objective objective = this.objectiveRepository.findOne(idTerm);
			HistoryObjective objectiveVersion = new HistoryObjective(id);
			objectiveVersion.setOrigin(objective.getOrigin());
			objectiveVersion.setOriginCreation(objective.getOriginCreation());
			objectiveVersion.setOriginUpdate(objective.getOriginUpdate());
			objectiveVersion.setPeriod(objective.getPeriod());
			objectiveVersion.setApplicative(objective.getApplicative());
			objectiveVersion.setApplicative_denomination(objective.getApplicative_denomination());
			objectiveVersion.setFunctionalInterpretation(objective.getFunctionalInterpretation());
			objectiveVersion.setFormula(objective.getFormula());
			objectiveVersion.setGranularity(objective.getGranularity());

			// Dimensiones del objectivo
			List<ObjectivesDimension> ods = this.objectivesDimensionRepository.findAllByObjective(idTerm);
			if (ods != null && ods.size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (ObjectivesDimension objectiveDimension : ods) {
					joiner.add(objectiveDimension.getDimension().getIdTerm().toString());
				}
				objectiveVersion.setDimensions(joiner.toString());
			}

			// Indicadores del objetivo
			List<AsociatedIndicator> ais = this.asociatedIndicatorRepository.findAllByObjective(idTerm);
			if (ais != null && ais.size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (AsociatedIndicator asociatedIndicator : ais) {
					joiner.add(asociatedIndicator.getIndicator().getIdTerm().toString());
				}
				objectiveVersion.setIndicators(joiner.toString());
			}

			this.historyObjectiveRepository.save(objectiveVersion);
		} else if (termType == 5) { // INFORME
			Inform inform = this.informRepository.findOne(idTerm);
			HistoryInform informVersion = new HistoryInform(id);
			informVersion.setLink(inform.getLink());

			// Indicadores del informe
			List<InformIndicator> iis = this.informIndicatorRepository.findAllByInform(idTerm);
			if (iis != null && iis.size() > 0) {
				StringJoiner joiner = new StringJoiner(",");
				for (InformIndicator informIndicator : iis) {
					joiner.add(informIndicator.getIndicator().getIdTerm().toString());
				}
				informVersion.setIndicators(joiner.toString());
			}

			this.historyInformRepository.save(informVersion);
		}
		System.out.println("3");
		return id;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<HistoryTerm> getHistoryFromTerm(Integer idTerm, Pageable page) {
		return this.historyTermRepository.findAllByIdTerm(idTerm, page);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryTerm getTermHistoryVersion(Integer idTerm, Integer idVersion) {
		return this.historyTermRepository.findOneByIdTermAndId(idTerm, idVersion);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryDimension getDimensionHistoryVersion(Integer idVersion) {
		return this.historyDimensionRepository.findOne(idVersion);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryIndicator getIndicatorHistoryVersion(Integer idVersion) {
		return this.historyIndicatorRepository.findOne(idVersion);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryInform getInformHistoryVersion(Integer idVersion) {
		return this.historyInformRepository.findOne(idVersion);
	}

	@Override
	@Transactional(readOnly = true)
	public HistoryObjective getObjectiveHistoryVersion(Integer idVersion) {
		return this.historyObjectiveRepository.findOne(idVersion);
	}
}
