package com.backend.metaDW.model.vo.drafts;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "draftinforms", schema = "public")
public class DraftInform {

	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "link")
	private String link;
	
	@Column(name = "indicators")
	private String indicators;

	/**
	 * void
	 */
	public DraftInform() {
	}

	/**
	 * @param id
	 */
	public DraftInform(Integer id) {
		this.id = id;
	}

	/**
	 * @param id
	 * @param link
	 * @param indicators
	 */
	public DraftInform(Integer id, String link, String indicators) {
		this.id = id;
		this.link = link;
		this.indicators = indicators;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIndicators() {
		return indicators;
	}

	public void setIndicators(String indicators) {
		this.indicators = indicators;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((indicators == null) ? 0 : indicators.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DraftInform other = (DraftInform) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (indicators == null) {
			if (other.indicators != null)
				return false;
		} else if (!indicators.equals(other.indicators))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HistoryInform [id=" + id + ", link=" + link + ", indicators=" + indicators + "]";
	}
}
