package com.backend.metaDW.model.vo.category;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(SimpleSubcategoryPK.class)
@Table(name = "Subcategories", schema = "public")
public class SimpleSubcategory {
	@Id
	@Column(name = "id_parent")
	private Integer idParent;

	@Id
	@Column(name="id_child")
	private Integer child;

	/**
	 * 
	 */
	public SimpleSubcategory() {
	}

	/**
	 * @param parent
	 * @param childs
	 */
	public SimpleSubcategory(Integer parent, Integer childs) {
		this.idParent = parent;
		this.child = childs;
	}

	public Integer getParent() {
		return idParent;
	}

	public void setParent(Integer parent) {
		this.idParent = parent;
	}

	public Integer getChilds() {
		return child;
	}

	public void setChilds(Integer childs) {
		this.child = childs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((child == null) ? 0 : child.hashCode());
		result = prime * result + ((idParent == null) ? 0 : idParent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleSubcategory other = (SimpleSubcategory) obj;
		if (child == null) {
			if (other.child != null)
				return false;
		} else if (!child.equals(other.child))
			return false;
		if (idParent == null) {
			if (other.idParent != null)
				return false;
		} else if (!idParent.equals(other.idParent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Subcategory [parent=" + idParent + ", childs=" + child + "]";
	}
}

@SuppressWarnings("serial")
class SimpleSubcategoryPK implements Serializable {
	private Integer idParent;
	private Integer child;

	public Integer getIdParent() {
		return idParent;
	}

	public void setIdParent(Integer idParent) {
		this.idParent = idParent;
	}

	public Integer getId_child() {
		return child;
	}

	public void setId_child(Integer id_child) {
		this.child = id_child;
	}
}

