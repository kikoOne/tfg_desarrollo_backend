package com.backend.metaDW.model.vo.category;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.user.User;

@Entity
@Table(name = "Categories", schema = "public")
public class FullCategory {

	@Id
	@SequenceGenerator(name = "categories_id_category_seq", sequenceName = "categories_id_category_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categories_id_category_seq")
	@Column(name = "id_category", updatable = false)
	private Integer idCategory;

	@Column(name = "name")
	private String name;

	@Column(name = "definition")
	private String definition;

	@Column(name = "state")
	private int state;

	@Column(name = "version")
	private String version;

	@ManyToOne
	@JoinColumn(name = "owner")
	private User owner;

	@Column(name = "creation")
	private Timestamp creation;

	@Column(name = "last_update")
	private Timestamp lastUpdate;

	@Column(name = "type")
	private int type;

	public FullCategory() {
	}

	public FullCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	public FullCategory(Integer idCategory, String name, String definition, int state, String version, User owner,
			Timestamp creation, int type) {
		this.idCategory = idCategory;
		this.name = name;
		this.definition = definition;
		this.state = state;
		this.version = version;
		this.owner = owner;
		this.creation = creation;
		this.type = type;
	}

	public Integer getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(Integer idCategory) {
		this.idCategory = idCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Timestamp getCreation() {
		return creation;
	}

	public void setCreation(Timestamp creation) {
		this.creation = creation;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creation == null) ? 0 : creation.hashCode());
		result = prime * result + ((definition == null) ? 0 : definition.hashCode());
		result = prime * result + ((idCategory == null) ? 0 : idCategory.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + state;
		result = prime * result + type;
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FullCategory other = (FullCategory) obj;
		if (creation == null) {
			if (other.creation != null)
				return false;
		} else if (!creation.equals(other.creation))
			return false;
		if (definition == null) {
			if (other.definition != null)
				return false;
		} else if (!definition.equals(other.definition))
			return false;
		if (idCategory == null) {
			if (other.idCategory != null)
				return false;
		} else if (!idCategory.equals(other.idCategory))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (state != other.state)
			return false;
		if (type != other.type)
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Category [idCategory=" + idCategory + ", name=" + name + ", definition=" + definition + ", state="
				+ state + ", version=" + version + ", owner=" + owner + ", creation=" + creation + ", type=" + type
				+ "]";
	}
}
