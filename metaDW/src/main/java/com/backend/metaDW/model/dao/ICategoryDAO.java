package com.backend.metaDW.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.category.CreationCategory;
import com.backend.metaDW.model.vo.category.FullCategory;

public interface ICategoryDAO {

	public Integer createCategoryTransaction(CreationCategory category);
	
	public void createCategoryTransaction(List<CreationCategory> category);

	public FullCategory findCategoryById(Integer idCategory);

	public Page<FullCategory> findCategoriesByType(Integer type, Pageable page);

	public Page<?> findSubcategoriesFromCategory(Integer idCategory, Pageable page);

	public Page<?> findTermsFromCategory(Integer idCategory, Pageable page);

	public boolean updateCategory(CreationCategory category);

	public void removeCategory(Integer idCategory);
	
	public List<Category> findAllCategories();
	
	public Category findParentCategory(Integer idChild);
}
