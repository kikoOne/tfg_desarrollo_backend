package com.backend.metaDW.model.vo.term.hierarchy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(SimpleObjectivesDimensionPK.class)
@Table(name = "objectivesdimensions", schema = "public")
public class SimpleObjectivesDimension {

	@Id
	@Column(name = "objective")
	private Integer objective;
	
	@Id
	@Column(name = "dimension")
	private Integer dimension;
	
	@Column(name = "additivity")
	private boolean additivity;

	/**
	 * 
	 */
	public SimpleObjectivesDimension() {
	}

	/**
	 * @param objective
	 * @param dimension
	 * @param additiviy
	 */
	public SimpleObjectivesDimension(Integer objective, Integer dimension, boolean additiviy) {
		this.objective = objective;
		this.dimension = dimension;
		this.additivity = additiviy;
	}

	public Integer getObjective() {
		return objective;
	}

	public void setObjective(Integer objective) {
		this.objective = objective;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}

	public boolean isAdditiviy() {
		return additivity;
	}

	public void setAdditiviy(boolean additiviy) {
		this.additivity = additiviy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (additivity ? 1231 : 1237);
		result = prime * result + ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + ((objective == null) ? 0 : objective.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleObjectivesDimension other = (SimpleObjectivesDimension) obj;
		if (additivity != other.additivity)
			return false;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (objective == null) {
			if (other.objective != null)
				return false;
		} else if (!objective.equals(other.objective))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleObjectivesDimension [objective=" + objective + ", dimension=" + dimension + ", additiviy="
				+ additivity + "]";
	}
}


@SuppressWarnings("serial")
class SimpleObjectivesDimensionPK implements Serializable {
	private Integer objective;
	private Integer dimension;

	/**
	 * 
	 */
	public SimpleObjectivesDimensionPK() {
	}

	/**
	 * @param objective
	 * @param dimension
	 */
	public SimpleObjectivesDimensionPK(Integer objective, Integer dimension) {
		this.objective = objective;
		this.dimension = dimension;
	}

	public Integer getObjective() {
		return objective;
	}

	public void setObjective(Integer objective) {
		this.objective = objective;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}
}

