package com.backend.metaDW.model.repositories.terms.hierarchy;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.hierarchy.InformIndicator;

public interface InformIndicatorRepository extends PagingAndSortingRepository<InformIndicator, Integer>{

	public Page<InformIndicator> findByInform(Integer idInform, Pageable page);
	
	public List<InformIndicator> findAllByInform(Integer idInform);
}
