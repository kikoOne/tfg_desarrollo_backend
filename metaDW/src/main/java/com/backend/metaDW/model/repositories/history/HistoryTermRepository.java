package com.backend.metaDW.model.repositories.history;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.history.HistoryTerm;

public interface HistoryTermRepository extends PagingAndSortingRepository<HistoryTerm, Integer> {

	public Page<HistoryTerm> findAllByIdTerm(Integer idTerm, Pageable page);
	
	public HistoryTerm findOneByIdTermAndId(Integer idTerm, Integer id);
}
