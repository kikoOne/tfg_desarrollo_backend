package com.backend.metaDW.model.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backend.metaDW.model.dao.ISearchDAO;
import com.backend.metaDW.model.repositories.categories.CategoryRepository;
import com.backend.metaDW.model.repositories.categories.FullCategoryRepository;
import com.backend.metaDW.model.repositories.categories.SubcategoryRepository;
import com.backend.metaDW.model.repositories.categories.TermsByCategoryRepository;
import com.backend.metaDW.model.repositories.scopes.ScopeRepository;
import com.backend.metaDW.model.repositories.scopes.TermsByScopeRepository;
import com.backend.metaDW.model.repositories.terms.FullTermRepository;
import com.backend.metaDW.model.repositories.terms.TermRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.AnalyticalApplicativeRepository;
import com.backend.metaDW.model.repositories.terms.hierarchy.OperationalOriginRepository;
import com.backend.metaDW.model.repositories.user.FullExternalUserRepository;
import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.FullTerm;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;
import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;

@Service
public class SearchDAO implements ISearchDAO {

	private ScopeRepository scopeRepository;
	private CategoryRepository categoryRepository;
	private OperationalOriginRepository operationalOriginRepository;
	private AnalyticalApplicativeRepository analyticalApplicativeRepository;
	private FullTermRepository fullTermRepository;
	private TermRepository termRepository;
	private TermsByCategoryRepository termsByCategoryRepository;
	private SubcategoryRepository subcategoryRepository;
	private FullCategoryRepository fullCategoryRepository;
	private FullExternalUserRepository fullExternalUserRepository;
	private TermsByScopeRepository termsByScopeRepository;

	@Autowired
	public SearchDAO(ScopeRepository scopeRepository, CategoryRepository categoryRepository,
			OperationalOriginRepository operationalOriginRepository,
			AnalyticalApplicativeRepository analyticalApplicativeRepository, TermRepository termRepository,
			FullTermRepository fullTermRepository, TermsByCategoryRepository termsByCategoryRepository,
			SubcategoryRepository subcategoryRepository, FullCategoryRepository fullCategoryRepository,
			TermsByScopeRepository termsByScopeRepository, FullExternalUserRepository fullExternalUserRepository) {
		this.scopeRepository = scopeRepository;
		this.categoryRepository = categoryRepository;
		this.operationalOriginRepository = operationalOriginRepository;
		this.analyticalApplicativeRepository = analyticalApplicativeRepository;
		this.termRepository = termRepository;
		this.fullTermRepository = fullTermRepository;
		this.termsByCategoryRepository = termsByCategoryRepository;
		this.subcategoryRepository = subcategoryRepository;
		this.fullCategoryRepository = fullCategoryRepository;
		this.termsByScopeRepository = termsByScopeRepository;
		this.fullExternalUserRepository = fullExternalUserRepository;
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Scope> findScopesByName(String name, Pageable page) {
		return this.scopeRepository.findByNameContainingIgnoreCase(name, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Category> findCategoriesByName(String name, Pageable page) {
		return this.categoryRepository.findByNameContainingIgnoreCase(name, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<OperationalOrigin> findOperationalOriginsByName(String name, Pageable page) {
		return this.operationalOriginRepository.findByNameContainingIgnoreCase(name, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<AnalyticalApplicative> findAnalyticalApplicativeByName(String name, Pageable page) {
		return this.analyticalApplicativeRepository.findByNameContainingIgnoreCase(name, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Term> findTermDimensionsByNameAndType(String name, Pageable page) {
		return this.termRepository.findByNameContainingIgnoreCaseAndTypeType(name, 2, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Term> findTermIndicatorsByName(String name, Pageable page) {
		return this.termRepository.findByNameContainingIgnoreCaseAndTypeType(name, 3, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Term> findTermObjectivesByName(String name, Pageable page) {
		return this.termRepository.findByNameContainingIgnoreCaseAndTypeType(name, 4, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullTerm> findTermsByNameAndType(String query, List<Integer> type, Pageable page) {
		return this.fullTermRepository.findByNameContainingIgnoreCaseAndTypeTypeIn(query, type, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullTerm> findTermsByName(String query, Pageable page) {
		return this.fullTermRepository.findByNameContainingIgnoreCase(query, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<FullTerm> findTermsByType(List<Integer> type, Pageable page) {
		return this.fullTermRepository.findByTypeTypeIn(type, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findTermsByNameAndTypeFromCategory(String query, List<Integer> types, Integer idCategory,
			Pageable page) {
		return this.termsByCategoryRepository.findTermsByNameAndTypeFromCategory(idCategory, query, types, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findTermsByNameFromCategory(String query, Integer idCategory, Pageable page) {
		return this.termsByCategoryRepository.findTermsByNameFromCategory(idCategory, query, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findTermsByTypeFromCategory(List<Integer> types, Integer idCategory, Pageable page) {
		return this.termsByCategoryRepository.findTermsByTypeFromCategory(idCategory, types, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findCategoriesByNameFromCategory(String query, Integer idCategory, Pageable page) {
		return this.subcategoryRepository.findSubcategoriesByNameFromParent(query, idCategory, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findCategoriesByNameAndType(String query, Integer type, Pageable page) {
		return this.fullCategoryRepository.findByTypeAndNameContainingIgnoreCase(type, query, page);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Page<?> findCategoriesByNameAndTypes(String query, List<Integer> types, Pageable page) {
		return this.categoryRepository.findByTypeInAndNameContainingIgnoreCase(types, query, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findUsersByName(String query, Pageable page) {
		return this.fullExternalUserRepository.findByNameContainingIgnoreCase(query, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findTermsByNameAndTypeFromScope(String query, List<Integer> types, Integer idScope, Pageable page) {
		return this.termsByScopeRepository.findTermsByNameAndTypeFromScope(idScope, query, types, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findActivatedUsersByName(String query, Pageable page) {
		return this.fullExternalUserRepository.findByNameContainingIgnoreCaseOrSurnamesContainingIgnoreCaseOrUserNameContainingIgnoreCaseAndState(query, query, query, 1, page);
	}

	@Override
	@Transactional(readOnly = true)
	public Page<?> findDesactivatedUsersByName(String query, Pageable page) {
		return this.fullExternalUserRepository.findByNameContainingIgnoreCaseOrSurnamesContainingIgnoreCaseOrUserNameContainingIgnoreCaseAndState(query, query, query, 0, page);
	}
}
