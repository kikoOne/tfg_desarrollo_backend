package com.backend.metaDW.model.repositories.categories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.backend.metaDW.model.vo.category.Subcategory;

public interface FullSubcategoryRepository extends PagingAndSortingRepository<Subcategory, Integer>{
		
	@Query("SELECT s.idParent FROM FullSubcategory s WHERE s.child.idCategory = :idChild")
	public List<?> findParentCategory(@Param ("idChild") Integer idChild);
}
