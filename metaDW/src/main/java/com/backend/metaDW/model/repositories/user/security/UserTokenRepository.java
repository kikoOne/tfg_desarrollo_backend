package com.backend.metaDW.model.repositories.user.security;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.metaDW.model.vo.user.security.UserToken;

public interface UserTokenRepository extends JpaRepository<UserToken, String> {

	public UserToken findByIdUserAndToken(Integer idUser, String token);

	public UserToken findByToken(String token);
}
