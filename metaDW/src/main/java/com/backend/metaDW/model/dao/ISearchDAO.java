package com.backend.metaDW.model.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.backend.metaDW.model.vo.category.Category;
import com.backend.metaDW.model.vo.scope.Scope;
import com.backend.metaDW.model.vo.term.FullTerm;
import com.backend.metaDW.model.vo.term.Term;
import com.backend.metaDW.model.vo.term.hierarchy.AnalyticalApplicative;
import com.backend.metaDW.model.vo.term.hierarchy.OperationalOrigin;

public interface ISearchDAO {

	Page<Scope> findScopesByName(String name, Pageable page);

	Page<Category> findCategoriesByName(String name, Pageable page);

	Page<OperationalOrigin> findOperationalOriginsByName(String name, Pageable page);

	Page<AnalyticalApplicative> findAnalyticalApplicativeByName(String name, Pageable page);

	Page<Term> findTermDimensionsByNameAndType(String name, Pageable page);

	Page<Term> findTermIndicatorsByName(String name, Pageable page);

	Page<Term> findTermObjectivesByName(String name, Pageable page);

	Page<FullTerm> findTermsByName(String query, Pageable page);

	Page<FullTerm> findTermsByType(List<Integer> type, Pageable page);

	Page<FullTerm> findTermsByNameAndType(String query, List<Integer> type, Pageable page);

	Page<?> findTermsByNameAndTypeFromCategory(String query, List<Integer> type, Integer idCategory, Pageable page);

	Page<?> findTermsByNameFromCategory(String query, Integer idCategory, Pageable page);

	Page<?> findTermsByTypeFromCategory(List<Integer> type, Integer idCategory, Pageable page);

	Page<?> findCategoriesByNameFromCategory(String query, Integer idCategory, Pageable page);

	Page<?> findCategoriesByNameAndType(String query, Integer type, Pageable page);
	
	Page<?> findCategoriesByNameAndTypes(String query, List<Integer> types, Pageable page);

	Page<?> findUsersByName(String query, Pageable page);
	
	Page<?> findActivatedUsersByName(String query, Pageable page);
	
	Page<?> findDesactivatedUsersByName(String query, Pageable page);

	Page<?> findTermsByNameAndTypeFromScope(String query, List<Integer> types, Integer idScope, Pageable page);
}
