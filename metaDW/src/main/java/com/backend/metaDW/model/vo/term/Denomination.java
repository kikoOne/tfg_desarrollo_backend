package com.backend.metaDW.model.vo.term;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(DenominationPK.class)
@Table(name = "denominations", schema = "public")
public class Denomination {
	
	@Id
	@Column(name = "id_term")
	private Integer idTerm;
	
	@Id
	@Column(name = "denomination")
	private String denomination;
	
	@Column(name = "id_denomination")
	private Integer idDenomination;
	
	/**
	 * void 
	 */
	public Denomination() {
	}	
	
	/**
	 * @param idTerm
	 * @param denomination
	 */
	public Denomination(Integer idTerm, String denomination) {
		this.idTerm = idTerm;
		this.denomination = denomination;
		this.idDenomination = null;
	}

	/**
	 * @param idTerm
	 * @param denomination
	 * @param idDenomination
	 */
	public Denomination(Integer idTerm, String denomination, Integer idDenomination) {
		this.idTerm = idTerm;
		this.denomination = denomination;
		this.idDenomination = idDenomination;
	}

	public Integer getIdTerm() {
		return idTerm;
	}

	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public Integer getIdDenomination() {
		return idDenomination;
	}

	public void setIdDenomination(Integer idDenomination) {
		this.idDenomination = idDenomination;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDenomination == null) ? 0 : idDenomination.hashCode());
		result = prime * result + ((denomination == null) ? 0 : denomination.hashCode());
		result = prime * result + ((idTerm == null) ? 0 : idTerm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Denomination other = (Denomination) obj;
		if (idDenomination == null) {
			if (other.idDenomination != null)
				return false;
		} else if (!idDenomination.equals(other.idDenomination))
			return false;
		if (denomination == null) {
			if (other.denomination != null)
				return false;
		} else if (!denomination.equals(other.denomination))
			return false;
		if (idTerm == null) {
			if (other.idTerm != null)
				return false;
		} else if (!idTerm.equals(other.idTerm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TermExample [id_term=" + idTerm + ", denomination=" + idDenomination + ", id_denomination="
				+ denomination + "]";
	}
}

@SuppressWarnings("serial")
class DenominationPK implements Serializable{
	
	private Integer idTerm;
	private String denomination;
	
	/**
	 * 
	 */
	public DenominationPK() {
	}
	
	public Integer getIdTerm() {
		return idTerm;
	}
	public void setIdTerm(Integer idTerm) {
		this.idTerm = idTerm;
	}
	public String getDenomination() {
		return denomination;
	}
	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}
}
