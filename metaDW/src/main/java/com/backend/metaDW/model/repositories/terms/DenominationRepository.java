package com.backend.metaDW.model.repositories.terms;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.Denomination;

public interface DenominationRepository extends PagingAndSortingRepository<Denomination, Integer> {
	
	public Page<Denomination> findByIdTerm(Integer idTerm, Pageable page);
	
	public List<Denomination> findAllByIdTerm(Integer idTerm);
}
