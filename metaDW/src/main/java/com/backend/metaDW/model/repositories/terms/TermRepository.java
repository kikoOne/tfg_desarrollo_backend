package com.backend.metaDW.model.repositories.terms;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.Term;

public interface TermRepository extends PagingAndSortingRepository<Term, Integer> {

	public Page<Term> findByNameContainingIgnoreCaseAndTypeType(String name, int type, Pageable page);

	public List<Term> findAllByType(Integer type);
	
	public List<Term> findAllByTypeType(Integer type);
	
	public Page<Term> findByNameContainingIgnoreCaseAndTypeTypeInAndOwner(String name, List<Integer> type, Integer owner, Pageable page);
}
