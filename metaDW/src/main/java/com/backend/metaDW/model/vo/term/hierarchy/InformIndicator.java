package com.backend.metaDW.model.vo.term.hierarchy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.term.Term;

@Entity
@IdClass(InformIndicatorPK.class)
@Table(name = "informindicators", schema = "public")
public class InformIndicator {

	@Id
	@Column(name = "inform")
	private Integer inform;

	@Id
	@ManyToOne
	@JoinColumn(name = "indicator")
	private Term indicator;

	/**
	 * 
	 */
	public InformIndicator() {
	}

	/**
	 * @param inform
	 * @param indicator
	 */
	public InformIndicator(Integer inform, Term indicator) {
		this.inform = inform;
		this.indicator = indicator;
	}

	public Integer getInform() {
		return inform;
	}

	public void setInform(Integer inform) {
		this.inform = inform;
	}

	public Term getIndicator() {
		return indicator;
	}

	public void setIndicator(Term indicator) {
		this.indicator = indicator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indicator == null) ? 0 : indicator.hashCode());
		result = prime * result + ((inform == null) ? 0 : inform.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InformIndicator other = (InformIndicator) obj;
		if (indicator == null) {
			if (other.indicator != null)
				return false;
		} else if (!indicator.equals(other.indicator))
			return false;
		if (inform == null) {
			if (other.inform != null)
				return false;
		} else if (!inform.equals(other.inform))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "InformIndicator [inform=" + inform + ", indicator=" + indicator + "]";
	}
}

@SuppressWarnings("serial")
class InformIndicatorPK implements Serializable {
	
	private Integer inform;
	private Integer indicator;

	/**
	 * 
	 */
	public InformIndicatorPK() {
	}

	/**
	 * @param inform
	 * @param indicator
	 */
	public InformIndicatorPK(Integer inform, Integer indicator) {
		this.inform = inform;
		this.indicator = indicator;
	}

	public Integer getInform() {
		return inform;
	}

	public void setInform(Integer inform) {
		this.inform = inform;
	}

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}
}
