package com.backend.metaDW.model.repositories.terms;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.FullIncidence;

public interface FullIncidenceRepository extends PagingAndSortingRepository<FullIncidence, Integer> {
	
	public Page<FullIncidence> findAll(Pageable page);	
}
