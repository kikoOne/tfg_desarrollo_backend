package com.backend.metaDW.model.repositories.scopes;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.scope.FullScope;

public interface FullScopeRepository extends PagingAndSortingRepository<FullScope, Integer>{
	
}
