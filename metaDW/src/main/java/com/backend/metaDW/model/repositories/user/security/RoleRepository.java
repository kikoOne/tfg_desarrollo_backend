package com.backend.metaDW.model.repositories.user.security;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.user.security.Role;

public interface RoleRepository extends PagingAndSortingRepository<Role, Integer> {

}
