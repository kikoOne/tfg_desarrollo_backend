package com.backend.metaDW.model.vo;

public class ResponseWrapper {
	
	private Object data;

	/**
	 * @param data
	 */
	public ResponseWrapper(String data) {
		this.data = data;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
