package com.backend.metaDW.model.vo.term.hierarchy;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.backend.metaDW.model.vo.term.Term;

@Entity
@IdClass(ObjectivesDimensionPK.class)
@Table(name = "objectivesdimensions", schema = "public")
public class ObjectivesDimension {

	@Id
	@ManyToOne
	@JoinColumn(name = "dimension")
	private Term dimension;
	
	@Id
	@Column(name = "objective")
	private Integer objective;
	
	@Column(name = "additivity")
	private boolean additivity;

	/**
	 * 
	 */
	public ObjectivesDimension() {
	}

	/**
	 * @param objective
	 * @param dimension
	 */
	public ObjectivesDimension(Term dimension, Integer objective) {
		this.objective = objective;
		this.dimension = dimension;
	}

	/**
	 * @param objective
	 * @param dimension
	 * @param additivity
	 */
	public ObjectivesDimension(Term dimension, Integer objective, boolean additivity) {
		this.objective = objective;
		this.dimension = dimension;
		this.additivity = additivity;
	}

	public Integer getObjective() {
		return objective;
	}

	public void setObjective(Integer objective) {
		this.objective = objective;
	}

	public Term getDimension() {
		return dimension;
	}

	public void setDimension(Term dimension) {
		this.dimension = dimension;
	}

	public boolean isAdditivity() {
		return additivity;
	}

	public void setAdditivity(boolean additivity) {
		this.additivity = additivity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (additivity ? 1231 : 1237);
		result = prime * result + ((dimension == null) ? 0 : dimension.hashCode());
		result = prime * result + ((objective == null) ? 0 : objective.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ObjectivesDimension other = (ObjectivesDimension) obj;
		if (additivity != other.additivity)
			return false;
		if (dimension == null) {
			if (other.dimension != null)
				return false;
		} else if (!dimension.equals(other.dimension))
			return false;
		if (objective == null) {
			if (other.objective != null)
				return false;
		} else if (!objective.equals(other.objective))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ObjectivesDimension [objective=" + objective + ", dimension=" + dimension + ", additiviy=" + additivity
				+ "]";
	}
}

@SuppressWarnings("serial")
class ObjectivesDimensionPK implements Serializable {
	private Integer objective;
	private Integer dimension;

	/**
	 * 
	 */
	public ObjectivesDimensionPK() {
	}

	/**
	 * @param objective
	 * @param dimension
	 */
	public ObjectivesDimensionPK(Integer objective, Integer dimension) {
		this.objective = objective;
		this.dimension = dimension;
	}

	public Integer getObjective() {
		return objective;
	}

	public void setObjective(Integer objective) {
		this.objective = objective;
	}

	public Integer getDimension() {
		return dimension;
	}

	public void setDimension(Integer dimension) {
		this.dimension = dimension;
	}
}
