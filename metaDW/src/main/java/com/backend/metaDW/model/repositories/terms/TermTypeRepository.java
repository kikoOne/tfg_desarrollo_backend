package com.backend.metaDW.model.repositories.terms;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.backend.metaDW.model.vo.term.TermType;

public interface TermTypeRepository extends PagingAndSortingRepository<TermType, Integer> {
	
}
