package com.backend.metaDW.config;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.backend.metaDW.security.jwt.JwtAuthenticationEntryPoint;
import com.backend.metaDW.model.dao.ITermDAO;
import com.backend.metaDW.model.dao.IUserDAO;
import com.backend.metaDW.model.vo.term.TermType;
import com.backend.metaDW.model.vo.user.User;
import com.backend.metaDW.model.vo.user.security.Role;
import com.backend.metaDW.security.jwt.FilterRequestToken;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;
	@Autowired
	private IUserDAO userDAO;
	@Autowired
	private ITermDAO termDAO;

	@Bean
	public FilterRequestToken filterRequestTokenBean() throws Exception {
		FilterRequestToken authenticationTokenFilter = new FilterRequestToken();
		return authenticationTokenFilter;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable() // se deshabilita el CSRF token
				.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and().sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Autowired
	public void configureGlobalSecurity() throws Exception {
		
		
		Collection<TermType> termTypes = (Collection<TermType>) this.termDAO.findAllTermTypes();
		if (termTypes == null || termTypes.size() == 0) {
			termTypes = new ArrayList<>();
			this.termDAO.saveTermType(new TermType(1, "Concepto", ""));
			this.termDAO.saveTermType(new TermType(2, "Dimensión", ""));
			this.termDAO.saveTermType(new TermType(3, "Indicador", ""));
			this.termDAO.saveTermType(new TermType(4, "Objectivo", ""));
			this.termDAO.saveTermType(new TermType(5, "Informe", ""));
			this.termDAO.saveTermType(new TermType(6, "Fuente de datos", ""));
		}

		Collection<Role> roles = this.userDAO.getAllRoles();
		if (roles == null || roles.size() == 0) {

			roles = new ArrayList<>();

			roles.add(this.userDAO.saveRole(new Role("ROLE_ADMIN_USERS")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_ADMIN_OPERATIONALORIGIN")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_ADMIN_ANALYTICALAPPLICATIVE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_ADMIN_INCIDENCE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_TERM_CREATION")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_TERM_UPDATE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_TERM_DELETE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_SCOPE_CREATION")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_SCOPE_UPDATE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_SCOPE_DELETE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_CATEGORY_CREATION")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_CATEGORY_UPDATE")));
			roles.add(this.userDAO.saveRole(new Role("ROLE_CATEGORY_DELETE")));
		}

		User user = this.userDAO.findUserByUserName("admin");
		if (user == null) {
			user = new User();
			user.setName("Admin");
			user.setSurnames("Administrador");
			user.setEmail("admin@admin.com");
			user.setState(1);
			user.setUserName("admin");
			user.setPassword((new BCryptPasswordEncoder()).encode(user.getUserName()));
			user.setRoles(roles);
			this.userDAO.saveUser(user);
		}
	}
}
