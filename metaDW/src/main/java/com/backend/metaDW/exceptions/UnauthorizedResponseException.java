package com.backend.metaDW.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class UnauthorizedResponseException extends WebApplicationException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6775121924977356546L;

	public UnauthorizedResponseException() {
        super(Response.Status.UNAUTHORIZED);
    }
}
