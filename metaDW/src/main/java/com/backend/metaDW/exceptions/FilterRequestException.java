package com.backend.metaDW.exceptions;

import org.springframework.security.core.AuthenticationException;

public class FilterRequestException extends AuthenticationException {

	private static final long serialVersionUID = 9191820333821861906L;

	public FilterRequestException(String msg) {
		super(msg);
	}

}
