package com.backend.metaDW.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class InternalServerException extends WebApplicationException{

	private static final long serialVersionUID = -6351573468354295491L;

	public InternalServerException() {
        super(Response.Status.INTERNAL_SERVER_ERROR);
    }
}
