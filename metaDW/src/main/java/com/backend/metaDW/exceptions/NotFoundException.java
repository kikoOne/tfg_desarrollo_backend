package com.backend.metaDW.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class NotFoundException extends WebApplicationException {

	private static final long serialVersionUID = -4657083200949472125L;

	public NotFoundException() {
		super(Response.Status.NOT_FOUND);
	}
}
