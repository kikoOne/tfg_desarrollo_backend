package com.backend.metaDW.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.backend.metaDW.exceptions.FilterRequestException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class FilterRequestToken extends GenericFilterBean {

	@Autowired
	private JwtTokenHelper jwtTokenHelper;
	@Autowired
	JwtAuthenticationEntryPoint entryPoint;
	@Value("x-auth-token")
	private String tokenHeader;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		try {
			if ((httpRequest.getMethod().equals("GET") && (!httpRequest.getRequestURL().toString().contains("/users")
					&& !httpRequest.getRequestURL().toString().contains("/incidences")
					&& !httpRequest.getRequestURL().toString().contains("drafts")))
					|| httpRequest.getMethod().equals("OPTIONS")) {
				chain.doFilter(request, response);
			} else if (httpRequest.getMethod().equals("GET")
					&& httpRequest.getRequestURL().toString().endsWith("/incidences/last")) {
				chain.doFilter(request, response);
			} else if (httpRequest.getMethod().equals("POST")
					&& httpRequest.getRequestURL().toString().endsWith("/auth")) {
				chain.doFilter(request, response);
			} else if (httpRequest.getMethod().equals("POST")
					&& httpRequest.getRequestURL().toString().endsWith("/refresh")) {
				String authToken = httpRequest.getHeader(this.tokenHeader);
				JWT jwt = jwtTokenHelper.validateTokenR(authToken);
				if (jwt != null) {
					JWTToken token = new JWTToken(jwt);
					if (token != null) {
						SecurityContextHolder.getContext().setAuthentication(token);
						chain.doFilter(request, response);
					}
				} else {
					System.out.println("jwt null");
					throw new FilterRequestException("Error in filter");
				}
			} else if ((!httpRequest.getMethod().equals("GET") || (httpRequest.getMethod().equals("GET")
					&& (httpRequest.getRequestURL().toString().contains("users")
							|| httpRequest.getRequestURL().toString().contains("incidences")
							|| httpRequest.getRequestURL().toString().contains("drafts"))))
					&& httpRequest.getHeader(this.tokenHeader) != null) {
				String authToken = httpRequest.getHeader(this.tokenHeader);
				JWT jwt = jwtTokenHelper.validateTokenA(authToken);
				if (jwt != null) {
					JWTToken token = new JWTToken(jwt);
					SecurityContextHolder.getContext().setAuthentication(token);
					chain.doFilter(request, response);
				} else {
					throw new FilterRequestException("Error in filter");
				}
			} else {
				throw new FilterRequestException("Error in filter");
			}
		} catch (FilterRequestException e) {
			SecurityContextHolder.clearContext();
			if (entryPoint != null) {
				entryPoint.commence(httpRequest, httpResponse, e);
			}
		}
	}
}
