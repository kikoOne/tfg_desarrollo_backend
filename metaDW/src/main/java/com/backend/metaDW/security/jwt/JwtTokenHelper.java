package com.backend.metaDW.security.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.backend.metaDW.exceptions.UnauthorizedResponseException;
import com.backend.metaDW.model.dao.ISecurityDAO;
import com.backend.metaDW.model.vo.user.security.Role;
import com.backend.metaDW.security.jwt.JWTToken;

import java.io.Serializable;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

@Component
public class JwtTokenHelper implements Serializable {

	private static final long serialVersionUID = 7401110086208956212L;

	static final String API_KEY = "API_KEY";
	static final String APP_VERSION = "APP_VERSION";
	static final String USER_ROLES = "roles";
	static final long expTime = 150000000;
	private String secret;

	@Autowired
	private ISecurityDAO securityDAO;

	public String getUsernameFromToken(String token) {
		String username;
		try {
			final Claims claims = getClaimsFromToken(token);
			username = claims.getSubject();
		} catch (Exception e) {
			username = null;
		}
		return username;
	}

	public Date getExpirationDateFromToken(String token) {
		Date expiration;
		try {
			final Claims claims = getClaimsFromToken(token);
			expiration = claims.getExpiration();
		} catch (Exception e) {
			expiration = null;
		}
		return expiration;
	}

	private Claims getClaimsFromToken(String token) {
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		} catch (Exception e) {
			claims = null;
		}
		return claims;
	}

	private Date generateExpirationDate() {
		return new Date(System.currentTimeMillis() + expTime * 1000);
	}

	public Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	/**
	 * Genera un token permanente para un usuario y dispositivo
	 * 
	 * @param idUser
	 * @param roles
	 * @param device
	 * @return
	 */
	public String generateTokenR(Integer idUser, Collection<String> roles, String device) {
		try {
			SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

			byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(API_KEY);
			Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

			JwtBuilder builder = Jwts.builder().setId(idUser.toString()).claim("version", APP_VERSION)
					.claim("device", device).claim("type", "R").claim(USER_ROLES, roles)
					.setIssuedAt(new GregorianCalendar().getTime()).signWith(signatureAlgorithm, signingKey);

			String newToken = builder.compact();
			this.securityDAO.saveToken(idUser, newToken, device);
			return newToken;

		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Genera un token de corta duración para el usuario del R-Token actual
	 * 
	 * @param tokenR
	 * @return
	 */
	public String generateTokenA(JWT tokenR) {
		try {
			SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

			byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(API_KEY);
			Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

			JwtBuilder builder = Jwts.builder().setId(tokenR.getTokenId()).setSubject(APP_VERSION).claim("type", "A")
					.claim(USER_ROLES, tokenR.getRoles()).setExpiration(generateExpirationDate())
					.signWith(signatureAlgorithm, signingKey);

			return builder.compact();
		} catch (Exception e) {
			return null;
		}
	}

	public Claims validateToken(String token) throws SecurityException {
		try {
			Claims newClaims = Jwts.parser().setSigningKey(DatatypeConverter.parseBase64Binary(API_KEY))
					.parseClaimsJws(token).getBody();

			return newClaims;
		} catch (SecurityException ex) {
			return null;
		}

	}

	/**
	 * Realiza la validación de un token A
	 * 
	 * @param tokenA
	 * @return
	 * @throws SecurityException
	 */
	public JWT validateTokenA(String tokenA) throws SecurityException {

		Claims claims = validateToken(tokenA);
		String type = claims.get("type", String.class);

		if (!type.equals("A")) {
			throw new SecurityException();
		}
		return new JWT(claims, tokenA);
	}

	/**
	 * Realiza la validación de un token R comprobando sus partes y su inserción
	 * en la BD
	 * 
	 * @param tokenR
	 * @return
	 * @throws SecurityException
	 * @throws UnauthorizedResponseException
	 */
	public JWT validateTokenR(String tokenR) throws SecurityException, UnauthorizedResponseException {

		Claims claims = validateToken(tokenR);
		String type = claims.get("type", String.class);

		if (!type.equals("R")) {
			System.out.println("NO TYPE R");
			throw new SecurityException();
		}

		if (!this.securityDAO.isTokenRecorded(tokenR)) {
			throw new UnauthorizedResponseException();
		}

		return new JWT(claims, tokenR);
	}

	public boolean removeTokenR(String tokenR) {
		JWT jwt = validateTokenR(tokenR);
		if (jwt != null)
			return this.securityDAO.removeToken(jwt);
		return false;
	}

	public ArrayList<String> parseRolesToString(Collection<Role> roles) {
		ArrayList<String> parsedRoles = new ArrayList<String>();
		for (Role role : roles) {
			parsedRoles.add(role.getName());
		}
		return parsedRoles;
	}

	public boolean hasRole(String role, JWTToken jwtToken) {
		for (GrantedAuthority auth : jwtToken.getAuthorities()) {
			if (role.equals(auth.getAuthority()))
				return true;
		}
		return false;
	}
}
