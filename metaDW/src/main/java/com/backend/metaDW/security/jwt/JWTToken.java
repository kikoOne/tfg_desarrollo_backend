package com.backend.metaDW.security.jwt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import io.jsonwebtoken.Claims;

public class JWTToken implements Authentication {

	private static final long serialVersionUID = -897113872499320347L;

	private JWT jwt;
	private final Collection<GrantedAuthority> authorities;
	private boolean authenticated;
	private Claims claims;

	public JWTToken(JWT jwt) {
		this.jwt = jwt;
		Collection<String> roles;
		roles = jwt.getRoles();
		List<GrantedAuthority> tmp = new ArrayList<>();
		if (roles != null) {
			for (String role : roles) {
				tmp.add(new SimpleGrantedAuthority(role));
			}
		}
		this.authorities = Collections.unmodifiableList(tmp);
		this.claims = jwt.getClaims();
		authenticated = false;
	}

	@Override
	public String getName() {
		return claims.getSubject();
	}

	public JWT getJwt() {
		return jwt;
	}

	public void setJwt(JWT jwt) {
		this.jwt = jwt;
	}

	public Claims getClaims() {
		return claims;
	}

	public void setClaims(Claims claims) {
		this.claims = claims;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public Object getCredentials() {
		return authorities;
	}

	@Override
	public Object getDetails() {
		return claims;
	}

	@Override
	public Object getPrincipal() {
		return claims.getSubject();
	}

	@Override
	public boolean isAuthenticated() {
		return authenticated;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}
}
