package com.backend.metaDW.security.jwt;

import java.util.Collection;
import io.jsonwebtoken.Claims;

public class JWT {

	private Claims claims;
	private String token;

	public JWT() {
	}

	public JWT(Claims claims, String token) {
		this.claims = claims;
		this.token = token;
	}

	/**
	 * Devuelve el id del usuario autenticado
	 *
	 * @return
	 */
	public String getTokenId() {
		return claims.getId();
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getRoles() {
		try {
			Collection<String> collection = (Collection<String>) claims.get(JwtTokenHelper.USER_ROLES);
			return collection;
		} catch (ClassCastException e) {
			return null;
		}
	}

	public boolean checkTokenIdentity(String id) {

		if (id.equals(claims.getId())) {
			return true;
		}
		return false;
	}

	public Claims getClaims() {
		return claims;
	}

	public void setClaims(Claims claims) {
		this.claims = claims;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
