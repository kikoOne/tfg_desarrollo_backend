package com.backend.metaDW.security.jwt;

import java.io.Serializable;

public class JwtAuthenticationRequest implements Serializable {

	private static final long serialVersionUID = -8445943548965154778L;

	private String username;
	private String password;
	private String device;

	public JwtAuthenticationRequest() {
	}

	public JwtAuthenticationRequest(String username, String password, String device) {
		this.setUsername(username);
		this.setPassword(password);
		this.setDevice(device);
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

}
