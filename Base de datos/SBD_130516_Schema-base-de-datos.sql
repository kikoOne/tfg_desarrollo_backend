
CREATE TABLE Users(
	id_user BIGSERIAL PRIMARY KEY,
	email VARCHAR(255) NOT NULL UNIQUE,
	user_name VARCHAR(255) NOT NULL UNIQUE,
	name VARCHAR(255) NOT NULL,
	surnames VARCHAR(255) NOT NULL,
	level INTEGER NOT NULL DEFAULT 0,
	state INTEGER NOT NULL DEFAULT 1 CHECK ((state = 0) OR (state = 1)), -- 0: usuarios no activos, 1: usuarios activos
	register TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	last_login TIMESTAMP,
	password VARCHAR(255) NOT NULL,
	salt VARCHAR(2047) NOT NULL
);

CREATE TABLE UserTokens(
	id_user INTEGER NOT NULL,
	token VARCHAR(2047) NOT NULL UNIQUE,
	device VARCHAR(2047) NOT NULL,
	CONSTRAINT id_userFK FOREIGN KEY (id_user) REFERENCES Users (id_user) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT UserTokensPK PRIMARY KEY (id_user, token)
);

CREATE TABLE Categories(
	id_category BIGSERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	definition VARCHAR(2047),
	state INTEGER NOT NULL DEFAULT 0 CHECK ((state = 0) OR (state = 1)), -- 0: version temporal, 1: version final
	version VARCHAR(50),
	owner INTEGER NOT NULL,
	creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	type INTEGER NOT NULL DEFAULT -1 CHECK ((type = -1) OR (type = 0) OR (type = 1)), -- -1: categoría raiz, 0 categoría intermedia, 1: categotía hoja
	CONSTRAINT ownerFK FOREIGN KEY (owner) REFERENCES Users (id_user) ON DELETE NO ACTION ON UPDATE CASCADE 
); --En caso de eliminación del propietario se mantendrá igualmente el id y no se eliminará la categoría aunque se ejecutará la accion para permitir la posible ejecución de triggers

CREATE TABLE Subcategories(
	id_parent INTEGER,
	id_child INTEGER,
	CONSTRAINT id_parentFK FOREIGN KEY (id_parent) REFERENCES Categories (id_category) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT id_childFK FOREIGN KEY (id_child) REFERENCES Categories (id_category) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT subcategoriesPK PRIMARY KEY (id_parent, id_child)
);

CREATE TABLE Scopes(
	id_scope BIGSERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	definition VARCHAR(2047),
	state INTEGER NOT NULL DEFAULT 0 CHECK ((state = 0) OR (state = 1)), -- 0: version temporal, 1: version final
	version VARCHAR(50),
	owner INTEGER NOT NULL,
	creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT ownerFK FOREIGN KEY (owner) REFERENCES Users (id_user) ON DELETE NO ACTION ON UPDATE CASCADE 
); --En caso de eliminación del propietario se mantendrá igualmente el id y no se eliminará el ámbito aunque se ejecutará la accion para permitir la posible ejecución de triggers

CREATE TABLE TermTypes(
	type SERIAL PRIMARY KEY, -- 1: CONCEPTO, 2: DIMENSION, 3: INDICADOR, 4: OBJETIVO, 5: INFORME, 6: FUENTE DE DATOS
	name VARCHAR(50) NOT NULL UNIQUE,
	description VARCHAR(255)
);

CREATE TABLE Terms(
	id_term BIGSERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL UNIQUE,
	short_name VARCHAR(100) NOT NULL,
	definition VARCHAR(255) NOT NULL,
	extended_definition VARCHAR(2047),
	state INTEGER DEFAULT 0 CHECK ((state = 0) OR (state = 1)), -- 0: version temporal, 1: version final
	version VARCHAR(255),
	owner INTEGER NOT NULL,
	creation TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	last_update TIMESTAMP,
	type INTEGER DEFAULT 1, -- POR DEFECTO DE TIPO CONCEPTO
	CONSTRAINT ownerFK FOREIGN KEY (owner) REFERENCES Users (id_user) ON DELETE NO ACTION ON UPDATE CASCADE, --En caso de eliminación del propietario se mantendrá igualmente el id y no se eliminará el ámbito aunque se ejecutará la accion para permitir la posible ejecución de triggers
	CONSTRAINT typeFK FOREIGN KEY (type) REFERENCES TermTypes (type) ON DELETE SET NULL ON UPDATE CASCADE --En caso de que se elimine el tipo de término maestro, los términos se fijarán a NULL 
);

CREATE TABLE TermsByCategory(
	id_term INTEGER NOT NULL,
	id_category INTEGER NOT NULL,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT id_categoryFK FOREIGN KEY (id_category) REFERENCES Categories (id_category) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT TermsByCategoryPK PRIMARY KEY (id_term, id_category)
);

CREATE TABLE TermsByScope(
	id_term INTEGER NOT NULL,
	id_scope INTEGER NOT NULL,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT id_scopeFK FOREIGN KEY (id_scope) REFERENCES Scopes (id_scope) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT TermsByScopePK PRIMARY KEY (id_term, id_scope)
);

CREATE TABLE TermsFollowed(
	id_user INTEGER NOT NULL,
	id_term INTEGER NOT NULL,
	CONSTRAINT id_userFK FOREIGN KEY (id_user) REFERENCES Users (id_user) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT TermsFollowedPK PRIMARY KEY (id_user, id_term)
);

CREATE TABLE CategoriesFollowed(
	id_user INTEGER NOT NULL,
	id_category INTEGER NOT NULL,
	CONSTRAINT id_userFK FOREIGN KEY (id_user) REFERENCES Users (id_user), --DELETE UPDATE
	CONSTRAINT id_termFK FOREIGN KEY (id_category) REFERENCES Categories (id_category), --DELETE UPDATE
	CONSTRAINT CategoriesFollowedFK PRIMARY KEY (id_user, id_category)
);

CREATE TABLE ScopesFollowed(
	id_user INTEGER NOT NULL,
	id_scope INTEGER NOT NULL,
	CONSTRAINT id_userFK FOREIGN KEY (id_user) REFERENCES Users (id_user), --DELETE UPDATE
	CONSTRAINT id_scopeFK FOREIGN KEY (id_scope) REFERENCES Scopes (id_scope), --DELETE UPDATE
	CONSTRAINT ScopesFollowedFK PRIMARY KEY (id_user, id_scope)
);

CREATE TABLE Denominations(
	id_term INTEGER NOT NULL,
	denomination VARCHAR(2047) NOT NULL,
	id_denomination INTEGER DEFAULT NULL,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT DenominationsPK PRIMARY KEY (id_term, denomination, id_denomination)
);

CREATE TABLE TermExamples(
	id_example BIGSERIAL NOT NULL,
	id_term INTEGER NOT NULL,
	example VARCHAR(2047) NOT NULL,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT TermExamplesPK PRIMARY KEY (id_example, id_term)
);

CREATE TABLE TermExclusions (
	id_exclusion BIGSERIAL NOT NULL,
	id_term INTEGER NOT NULL,
	exclusion VARCHAR(2047) NOT NULL,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT TermExclusionsPK PRIMARY KEY (id_exclusion, id_term)
);

CREATE TABLE OperationalOrigins(
	id_origin BIGSERIAL PRIMARY KEY,
	description VARCHAR(2047) NOT NULL
);

CREATE TABLE Incidences(
	id_incidence BIGSERIAL NOT NULL,
	id_term INTEGER NOT NULL,
	description VARCHAR(2047) NOT NULL,
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT IncidencesPK PRIMARY KEY (id_incidence, id_term)
);

CREATE TABLE AnalyticalApplicative(
	id_applicative BIGSERIAL PRIMARY KEY,
	name VARCHAR(255) NOT NULL UNIQUE,
	description VARCHAR(2047)
);

CREATE TABLE Dimensions(
	id_term INTEGER PRIMARY KEY,
	origin INTEGER,
	origin_creation TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	origin_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	period  INTEGER,
	applicative INTEGER,
	applicative_denomination VARCHAR(2047),
	hierarchy VARCHAR(2047),
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT originFK FOREIGN KEY (origin) REFERENCES OperationalOrigins (id_origin) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT applicativeFK FOREIGN KEY (applicative) REFERENCES AnalyticalApplicative (id_applicative) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE Indicators(
	id_term INTEGER PRIMARY KEY,
	origin INTEGER,
	origin_creation TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	origin_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	period  INTEGER,
	applicative INTEGER,
	applicative_denomination VARCHAR(2047),
	functional_interpretation VARCHAR(2047),
	formula VARCHAR(2047),
	granularity VARCHAR(255),
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT originFK FOREIGN KEY (origin) REFERENCES OperationalOrigins (id_origin) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT applicativeFK FOREIGN KEY (applicative) REFERENCES AnalyticalApplicative (id_applicative) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE Objectives(
	id_term INTEGER PRIMARY KEY,
	origin INTEGER,
	origin_creation TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	origin_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	period  INTEGER,
	applicative INTEGER,
	applicative_denomination VARCHAR(2047),
	functional_interpretation VARCHAR(2047),
	formula VARCHAR(2047),
	granularity VARCHAR(255),
	CONSTRAINT id_termFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT originFK FOREIGN KEY (origin) REFERENCES OperationalOrigins (id_origin) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT applicativeFK FOREIGN KEY (applicative) REFERENCES AnalyticalApplicative (id_applicative) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE IndicatorsDimensions(
	indicator INTEGER NOT NULL,
	dimension INTEGER NOT NULL,
	additivity BOOLEAN NOT NULL DEFAULT FALSE,
	CONSTRAINT indicatorFK FOREIGN KEY (indicator) REFERENCES Indicators (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT dimensionFK FOREIGN KEY (dimension) REFERENCES Dimensions (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT IndicatorsDimensionsPK PRIMARY KEY (indicator, dimension)
);

CREATE TABLE ObjectivesDimensions(
	objective INTEGER NOT NULL,
	dimension INTEGER NOT NULL,
	additivity BOOLEAN NOT NULL DEFAULT FALSE,
	CONSTRAINT objectiveFK FOREIGN KEY (objective) REFERENCES Objectives (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT dimensionFK FOREIGN KEY (dimension) REFERENCES Dimensions (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT ObjectivesDimensionsPK PRIMARY KEY (objective, dimension)
);

CREATE TABLE AsociatedIndicators (
	indicator INTEGER NOT NULL,
	objective INTEGER NOT NULL,
	CONSTRAINT objectiveFK FOREIGN KEY (objective) REFERENCES Objectives (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT indicatorFK FOREIGN KEY (indicator) REFERENCES Indicators (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT AsociatedIndicatorsPK PRIMARY KEY (objective, indicator)
);

CREATE TABLE Informs(
	id_term INTEGER PRIMARY KEY,
	link VARCHAR(255) NOT NULL,
	CONSTRAINT objectiveFK FOREIGN KEY (id_term) REFERENCES Terms (id_term) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE InformIndicators(
	inform INTEGER NOT NULL,
	indicator INTEGER NOT NULL,
	CONSTRAINT informFK FOREIGN KEY (inform) REFERENCES Informs (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT indicatorFK FOREIGN KEY (indicator) REFERENCES Indicators (id_term) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT InformIndicatorsPK PRIMARY KEY (inform, indicator)
);

