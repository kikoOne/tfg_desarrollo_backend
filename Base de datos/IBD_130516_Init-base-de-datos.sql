
BEGIN;
	INSERT INTO Users (email,user_name,name,surnames,level,password,salt) 
		VALUES ('admin@gmail.com','admin','Admin','Administrador',777,'12341234','');
	
	INSERT INTO TermTypes (type, name, description) 
		VALUES (1,'Concepto','');
	INSERT INTO TermTypes (type, name, description) 
		VALUES (2,'Dimensión','');
	INSERT INTO TermTypes (type, name, description) 
		VALUES (3,'Indicador','');
	INSERT INTO TermTypes (type, name, description) 
		VALUES (4,'Objetivo','');
	INSERT INTO TermTypes (type, name, description) 
		VALUES (5,'Informe','');
	INSERT INTO TermTypes (type, name, description) 
		VALUES (6,'Fuente de datos','');
COMMIT;